from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class CampActManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "copy_config"):
            return
        self.conn = conn
        self.copy_config = None
        self.copy_values = {}
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(CampActManager, "_instance"):
            CampActManager._instance = object.__new__(cls)
        return CampActManager._instance

    def load_config(self):
        self.copy_config = self.conn.getALL('select * from act_camp_copy')
        self.copy_values = {}
        for con in self.copy_config:
            self.copy_values[con.get('level')] = CampCopyEntity(con)
        db_log.info('加载配置成功: act_camp_copy')

    def get_copy_all(self):
        """
        获取所有副本事件的配置
        :return:
        """
        return list(self.copy_values.values())

    def get_copy_by_level(self, level):
        """
        获取副本事件对应难度的配置
        :param level:
        :return:
        """
        value = self.copy_values.get(level)
        if not value:
            raise AssertionError(f"阵营副本事件中未找到对应难度的配置: {level}")
        return value

    def get_next_level_copy(self, level: int):
        """
        获取副本事件当前难度的下一难度配置
        :param level:
        :return:
        """
        value = self.copy_values.get(level + 1)
        if not value:
            raise AssertionError(f"阵营副本事件中未找到对应难度的配置: {level}")
        return value


class CampCopyEntity:
    """act_camp_copy"""

    def __init__(self, value):
        self.id = value.get('id')
        self.note = value.get('note')
        self.stageid = value.get('stageid')
        self.monster = value.get('monster')
        self.ce = value.get('ce')
        self.icon = value.get('icon')
        self.level = value.get('level')
        self.into = value.get('into')
        self.target = value.get('target')
        self.award = value.get('award')
        self.param = value.get('param')
        self.actid = value.get('actid')
        self.scroll = value.get('scroll')

