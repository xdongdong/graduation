from copy import copy
from apis.base_api import BaseApi


class LeaderApi(BaseApi):
    # 英雄
    __unlock_leader_path = "/game/leader/unlock"

    def unlock_leader(self, lid, unlock_type=2):
        """
        开启物品
        :param lid: 要解锁的主角id
        :param unlock_type: 解锁方式  2花钱解锁
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "lid": lid,
            "type": unlock_type,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__unlock_leader_path, data=data)
        return re
