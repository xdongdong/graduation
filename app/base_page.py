import os
from copy import deepcopy
import allure
from airtest.core.android import Android
from poco.drivers.unity3d import UnityPoco
from poco.exceptions import PocoNoSuchNodeException, PocoTargetTimeout
from settings import BASE_DIR
from utils.log import ui_log


class BasePage:
    _LOGIN_BTN = {"name": 'btnlogin'}
    _SWITCH_SERVER_BTN = {"name": "switch", "type": "Node"}
    _ACCOUNT_INPUT = {"name": "input", "type": "InputField", "parent": {
        "name": "loginnode", "type": "Node"
    }}
    # _SERVER = {"text": "测试1服", "name": "des", "parent": {"name": "bgbtn", "parent": {"name": "peritem"}}}
    _SERVER = {"text": "测试1服", "parent": {"name": "peritem", "offspring": {"name": "des"}}}
    _INTO_GAME_BTN = {"name": "btn_enter", "type": "Image"}
    _NOVICE_ALTER = {"text": "是否跳过新手引导？", "name": "content"}

    # 弹窗内的确认按钮
    _SURE_BUTTON = {"text": "确定"}
    # 左上角的返回按钮
    _BACK_BUTTON = {"name": "return_btn"}
    # 右侧信息提示栏
    _RIGHT_TIP = {"name": "text", "parent": {"name": "tip"}}

    # 用户名
    _USERNAME = {"name": "name"}

    # 弹窗确认
    _ALTER_SURE = {"name": "confirm_btn"}

    # 网络异常
    _NET_WORK_ERROR = {"name": "content", "text": "连接出现异常_wprotocolerror"}
    _RE_CONNECT = {"name": "right_btn"}

    def __init__(self, driver: Android = None, poco: UnityPoco = None):
        self.driver = driver
        self.poco = poco
        self.timeout = 10

    def find(self, locator, timeout=None):
        if timeout is None:
            timeout = self.timeout
        try:
            # 拷贝一个 以防影响到之前的
            locator = deepcopy(locator)
            # 是否需要父级查找
            ui_log.info(f"查找元素: {locator}")
            el = None
            if locator.get("parent"):
                el = self.find(locator.pop("parent"))

            # 是否有子集查找
            offspring = locator.pop("offspring") if locator.get("offspring") else None
            # 是否有所有子属性查找
            children = locator.pop("children") if locator.get("children") else None

            return_el = None
            # 如果只有一个元素且有查找父元素,则直接查找父元素对应的child
            if el and len(el) == 1:
                """
                例: {"text": "content", "parent": {"name": "listnode"}}
                查找listnode节点下的text为content的那一个节点
                """
                return_el = el.child(**locator).wait(timeout)
            # 如果查找过父元素且查找出的不止一个元素, 则遍历所有元素查找匹配的那一个
            elif el:
                """
                例: {"text": "content", "parent": {"name": "listnode", "offspring": {"name": "linenode"}}}
                查找listnode节点下的所有linenode节点,在linenode节点中查找text为content的那一个
                """
                # @todo 如果有特殊的条件如 index: 1这种 则直接取第一个元素
                for ele in el:
                    matched = True
                    for key, value in locator.items():
                        if ele.attr(key) != value:
                            matched = False
                    if matched:
                        return_el = ele
                        break
                if not return_el:
                    raise PocoNoSuchNodeException("未在所有元素中找到匹配的元素")
            # 如果没有查询过父元素，则直接查找对应元素即可
            else:
                return_el = self.poco(**locator).wait(timeout)

            if offspring:
                r = return_el.offspring(**offspring)
                return r
            elif children:
                r = return_el.children()
                return r
            return return_el
        except (PocoNoSuchNodeException, PocoTargetTimeout) as e:
            # 未找到元素时,查验是否网络异常如果异常的话 重新连接 再进行查找
            if self.is_exits(self._NET_WORK_ERROR, 0):
                self.find_and_click(self._RE_CONNECT, 3)
                self.find(locator, timeout)
            else:
                raise PocoNoSuchNodeException(e)

    def find_and_click(self, locator, timeout=None):
        element = self.find(locator, timeout)
        element.click(sleep_interval=0.5)

    def is_exits(self, locator, timeout=None):
        """判断x秒内是否出现"""
        return self.find(locator, timeout).exists()

    def wait_for_appearance(self, locator, timeout=0):
        """
        等待某个元素出现
        :param locator:  定位
        :param timeout:  超时时间 默认为self.timeout的时间 如果传入则时间为self.timeout + timeout 还会多出个默认的3S 不知道为啥
        :return:
        """
        el = self.find(locator)
        el.wait_for_appearance(timeout)
        return el

    def wait_for_disappearance(self, locator, timeout=0):
        """
        等待某个元素消失
        :param locator:  定位
        :param timeout:  超时时间
        :return:
        """
        return self.find(locator).wait_for_disappearance(timeout)

    def find_and_set_text(self, locator: dict, text: str):
        with allure.step(f"定位元素并输入文本: {locator}, text={text}"):
            element = self.find(locator)
            element.set_text(str(text))

    def fail_picture(self, name):
        with allure.step(f"用例失败截图: {name}"):
            if not os.path.exists(BASE_DIR + r"/error_picture/"):
                os.mkdir(BASE_DIR + r"/error_picture/")
            filename = BASE_DIR + rf'/error_picture/{name}.png'
            self.driver.snapshot(filename)
        allure.attach.file(filename, '失败用例截图:{filename}'.format(filename=name), allure.attachment_type.PNG)

    def login(self, open_id: str):
        """
        登录
        :param open_id:
        :return:
        """
        with allure.step(f"登录账号: {open_id}"):
            self.find_and_set_text(self._ACCOUNT_INPUT, open_id)
            self.find(self._LOGIN_BTN).click()
            self.find_and_click(self._SWITCH_SERVER_BTN)
            self.find_and_click(self._SERVER)
            self.find_and_click(self._INTO_GAME_BTN)
            # 如果self.timeout秒内出现新手引导弹窗, 则点击确定按钮
            if self.is_exits(self._NOVICE_ALTER):
                self.find_and_click(self._SURE_BUTTON)

        from app.lobby_page import LobbyPage
        return LobbyPage(self.driver, self.poco)

    def get_right_tips(self):
        with allure.step(f"获取右侧tips"):
            tips = self.wait_for_appearance(self._RIGHT_TIP)
        return tips

    def close_alter(self):
        """
        关闭弹窗
        :return:
        """
        with allure.step(f"关闭弹窗"):
            self.find_and_click(self._ALTER_SURE)
        return self
