# import time
# from random import choice
# import allure
# import pytest
# from apis.camp_act_api import CampActApi
# from apis.camp_api import CampApi
# from apis.chat_api import ChatApi
# from settings import get_config
# from tables.award import AwardEntity, AwardManager
# from tables.camp import CampManager
# from tables.camp_act import CampActManager
# from tables.function_unlock import FunctionUnlockManager
# from tables.mail import MailManager
# from testcase.constant import Constant
# from testcase.data.camp_act_data import CampActData
# from utils.GetHashCode import GetHashCode
# from utils.comparators import Comparators
# from utils.log import api_log
# from utils.param_encrypt import decrypt
# from utils.time_utlis import TimeUtils
# from utils.util import parse_json, parse_setting, generate_openid
#
#
# class TestCopy:
#     """
#     阵营副本类活动
#     """
#     def setup_class(self):
#         self.setting = get_config()
#         # api类
#         self.api = CampActApi()
#         # 将openid进行随机,每次都是用不重复的openid
#         openid = generate_openid()
#         # 登录
#         user_info, host = self.api.login(openid)
#         # 用户信息
#         self.role_id = user_info.get("roleid")
#         # 数据类
#         self.data = CampActData()
#         # 用户hash值
#         self.user_hash = GetHashCode.getHashCode(self.role_id)
#         # 初始化chat api
#         self.chat_api = ChatApi(self.api.base_data)
#         # 断言类
#         self.compare = Comparators()
#         # 副本事件配置
#         self.camp_copy = CampActManager(self.api.db_utils.sql_conn)
#         # basic_function_unlock
#         self.function_unlock = FunctionUnlockManager(self.api.db_utils.sql_conn)
#         # basic_mail
#         self.mail_manager = MailManager(self.api.db_utils.sql_conn)
#         # camp
#         self.camp_manager = CampManager(self.api.db_utils.sql_conn)
#         # basic_award
#         self.award_manager = AwardManager(self.api.db_utils.sql_conn)
#         # time
#         self.time_util = TimeUtils()
#         # constant
#         self.constant = Constant()
#         # 加入阵营
#         camp_api = CampApi(self.api.base_data)
#         # 先解锁阵营功能
#         unlock_stage = self.function_unlock.get_value_by_id(self.constant.unlock_camp_id)
#         self.api.db_utils.insert_user_stage(self.role_id, unlock_stage.stageid)
#         # 先退出之前的阵营
#         camp_api.leave_camp()
#         re = camp_api.join_camp(self.data.camp)
#         response = decrypt(re.text)
#         self.compare.success_code(response, msg=f"加入阵营失败: {response}")
#
#     @pytest.mark.thisismark
#     def test_lock_info(self):
#         """
#         活动未开启时 拉取接口 挑战等
#         :return:
#         """
#         self.api.reset_camp_event()
#
#         re = self.api.copy_info()
#         response = decrypt(re.text)
#         self.compare.error_code(response, msg="活动未开启时拉取信息code")
#
#         re = self.api.copy_buy(1)
#         response = decrypt(re.text)
#         self.compare.error_code(response, msg="活动未开启时购买门票code")
#
#         re = self.api.copy_start(1)
#         response = decrypt(re.text)
#         self.compare.error_code(response, msg="活动未开启时开始挑战code")
#
#     def test_lock_start(self):
#         """
#         挑战未解锁的副本
#         :return:
#         """
#         self.api.open_act(self.data.act_copy)
#
#         re = self.api.copy_info()
#         response = decrypt(re.text)
#         self.compare.success_code(response, msg="开启活动后拉取信息code")
#
#         with allure.step(f"根据返回的内容查找到当前未解锁的难度"):
#             not_unlock = []
#             info = parse_json("info", response)
#             for stage in info:
#                 if stage.get("unLockstatus") == 0:
#                     not_unlock.append(stage.get("level"))
#
#         start_re = self.api.copy_start(choice(not_unlock))
#         start_response = decrypt(start_re.text)
#         self.compare.error_code(start_response, msg="挑战未解锁的难度code")
#
#     def test_start_less(self):
#         """
#         材料不足时挑战副本
#         :return:
#         """
#         self.api.open_act(self.data.act_copy)
#
#         re = self.api.copy_info()
#         response = decrypt(re.text)
#         self.compare.success_code(response, msg="开启活动后拉取信息code")
#
#         with allure.step(f"修改门票数量为0"):
#             self.api.db_utils.update_db(f"{self.setting.DB}.user_package_{self.user_hash}",
#                                {"num": 0},
#                                {"pid": self.data.copy_ticket, "roleid": self.role_id})
#
#         with allure.step(f"修改今日已使用的免费次数"):
#             free_time = self.global_manager.get_value_by_id(self.data.free_times_global)
#             self.api.db_utils.api_redis.conn.zadd(self.data.free_challenge_key, {
#                 f'"{self.role_id}"': free_time
#             })
#
#         first_setting = self.camp_copy.get_copy_all()[0]
#         start_re = self.api.copy_start(first_setting.level)
#         start_response = decrypt(start_re.text)
#         self.compare.error_code(start_response, msg="材料不足时挑战副本code")
#
#     def test_error_settle(self):
#         """
#         传入错误的参数进行结算
#         :return:
#         """
#         self.api.open_act(self.data.act_copy)
#
#         re = self.api.copy_info()
#         response = decrypt(re.text)
#         # 获取已解锁的难度
#         info = parse_json("info", response)
#         unlock = [i for i in info if i.get("unLockstatus") == 1]
#         self.compare.success_code(response, msg="开启活动后拉取信息code")
#
#         challenge = unlock[-1]
#         start_re = self.api.copy_start(challenge.get("level"))
#         start_response = decrypt(start_re.text)
#         verify = parse_json("verify", start_response)
#         self.compare.success_code(start_response, msg="开始挑战code")
#
#         settle_re = self.api.copy_settle(verify + "t", 1)
#         settle_response = decrypt(settle_re.text)
#         self.compare.error_code(settle_response, msg="传入错误的验证码进行结算")
#
#         with allure.step(f"使用一个验证码重复结算两次"):
#             settle_re1 = self.api.copy_settle(verify, 1)
#             settle_response1 = decrypt(settle_re1.text)
#             self.compare.success_code(settle_response1, msg="首次正常进行结算")
#
#             settle_re2 = self.api.copy_settle(verify, 1)
#             settle_response2 = decrypt(settle_re2.text)
#             self.compare.error_code(settle_response2, msg="第二次使用同一验证码进行结算")
#
#     def test_error_buy(self):
#         """
#         传入错误的参数购买门票
#         :return:
#         """
#         self.api.open_act(self.data.act_copy)
#
#         with allure.step(f"获取购买门票所需消耗的货币"):
#             cost = parse_setting(self.global_manager.get_value_by_id(self.data.buy_times_global))
#             pid, num = list(cost.values())[0], list(cost.keys())[0]
#             self.chat_api.chat_talk(f"gm prop {pid} 10000")
#
#         before_prop = self.api.db_utils.get_user_props(self.role_id)
#         re = self.api.copy_buy(-1)
#         response = decrypt(re.text)
#         self.compare.error_code(response, msg=f"购买次数传入负数购买门票: {response}")
#         changed_prop = self.api.db_utils.get_user_props(self.role_id)
#         self.compare.equal(before_prop, changed_prop, f"购买次数传入负数购买门票,物品应无变动")
#
#         re = self.api.copy_buy(0)
#         response = decrypt(re.text)
#         self.compare.success_code(response, msg=f"购买次数传入0购买门票: {response}")
#         changed_prop = self.api.db_utils.get_user_props(self.role_id)
#         self.compare.equal(before_prop, changed_prop, f"购买次数传入0购买门票,物品应无变动")
#
#     def test_success_buy(self):
#         """
#         传入正确的参数购买门票
#         :return:
#         """
#         self.api.open_act(self.data.act_copy)
#
#         with allure.step(f"获取购买门票所需消耗的货币"):
#             cost = parse_setting(self.global_manager.get_value_by_id(self.data.buy_times_global))
#             pid, num = str(list(cost.values())[0]), int(list(cost.keys())[0])
#             self.chat_api.chat_talk(f"gm prop {pid} 10000")
#
#         buy_counts = [1, 10]
#         for buy_count in buy_counts:
#             setting_drop = AwardEntity(reward=[])
#             before_prop = self.api.db_utils.get_user_props(self.role_id)
#             with allure.step(f"购买次数传入{buy_count}进行购买门票"):
#                 re = self.api.copy_buy(buy_count)
#                 response = decrypt(re.text)
#                 self.compare.success_code(response, msg=f"购买次数传入{buy_count}购买门票: {response}")
#                 changed_prop = self.api.db_utils.get_user_props(self.role_id)
#                 setting_drop.reward.append({
#                     "pid": self.data.copy_ticket,
#                     "num": buy_count
#                 })
#                 setting_drop.reward.append({
#                     "pid": pid,
#                     "num": -(num * buy_count)
#                 })
#                 self.compare.changing(response, before_prop, changed_prop, setting_drop)
#
#     def test_challenge(self):
#         """
#         进行正常的挑战
#         :return:
#         """
#         open_stage = self.api.open_act(self.data.act_copy)
#
#         re = self.api.copy_info()
#         response = decrypt(re.text)
#
#         with allure.step(f"验证首次进入活动拉取信息是否正确"):
#             # 第一个难度的配置
#             first = self.camp_copy.get_copy_all()[0]
#             # 获取已解锁的难度
#             info = parse_json("info", response)
#             unlock = [i.get('level') for i in info if i.get("unLockstatus") == 1]
#             # 免费挑战次数
#             free = parse_json("freecount", response)
#             # 当前进度
#             finish = parse_json("finish", response)
#             user_finish = parse_json("userfinish", response)
#
#             self.compare.success_code(response, msg="开启活动后拉取信息code")
#             self.compare.equal(free, int(self.global_manager.get_value_by_id(self.data.free_times_global)), f"免费挑战次数验证")
#             self.compare.equal(unlock, [first.level], f"首次进入活动默认开启难度验证")
#             self.compare.equal(finish, 0, f"当前进度验证")
#             self.compare.equal(user_finish, 0, f"当前玩家进度验证")
#
#         cur_setting = first
#
#         while True:
#             with allure.step(f"获取难度{cur_setting.level}的下一级配置"):
#                 try:
#                     next_setting = self.camp_copy.get_next_level_copy(cur_setting.level)
#                 except AssertionError:
#                     next_setting = None
#                 setting_drop = AwardEntity(reward=[])
#                 for pid, num in parse_setting(cur_setting.award).items():
#                     setting_drop.reward.append({
#                         "pid": pid,
#                         "num": num
#                     })
#             if free <= 0:
#                 with allure.step(f"免费挑战次数不足, 添加门票"):
#                     self.chat_api.chat_talk(f"gm prop {self.data.copy_ticket} 1")
#                     setting_drop.reward.append({
#                         "pid": self.data.copy_ticket,
#                         "num": -1
#                     })
#
#             start_re = self.api.copy_start(cur_setting.level)
#             start_response = decrypt(start_re.text)
#             verify = parse_json("verify", start_response)
#             self.compare.success_code(start_response, msg=f"开始挑战: 难度={cur_setting.level}")
#
#             before_prop = self.api.db_utils.get_user_props(self.role_id)
#             settle_re = self.api.copy_settle(verify, 1)
#             settle_response = decrypt(settle_re.text)
#             changed_prop = self.api.db_utils.get_user_props(self.role_id)
#             self.compare.success_code(settle_response, msg=f"结算关卡:{cur_setting.level} data={settle_response}")
#             # 达成目标物品不需要检查 不进背包
#             self.compare.changing(settle_response, before_prop, changed_prop, setting_drop,
#                                   blank=[list(parse_setting(cur_setting.target).keys())[0]])
#
#             with allure.step(f"挑战成功后验证info接口返回的数据"):
#                 # 挑战完成后更改数据
#                 if next_setting:
#                     unlock.append(next_setting.level)
#                 free = free - 1 if free > 0 else 0
#                 add_finish = list(parse_setting(cur_setting.target).values())[0]
#                 finish += add_finish
#                 user_finish += add_finish
#                 # 拉取信息
#                 re = self.api.copy_info()
#                 response = decrypt(re.text)
#                 info = parse_json("info", response)
#                 new_unlock = [i.get("level") for i in info if i.get("unLockstatus") == 1]
#                 # 免费挑战次数
#                 new_free = parse_json("freecount", response)
#                 # 当前进度
#                 new_finish = parse_json("finish", response)
#                 new_user_finish = parse_json("userfinish", response)
#                 rank_value = self.api.db_utils.api_redis.conn.zscore(self.data.event_rank_key.format(open_stage),
#                                                             f'"{self.role_id}"')
#
#                 self.compare.equal(new_unlock, unlock, f"验证成功挑战后是否解锁下一难度")
#                 self.compare.equal(new_free, free, f"验证成功挑战后是否扣减次数")
#                 self.compare.equal(new_finish, finish, f"验证成功挑战后是否增加进度值")
#                 self.compare.equal(new_user_finish, user_finish, f"验证成功挑战后是否增加玩家进度值")
#                 self.compare.equal(int(rank_value), user_finish, f"验证排行榜数值存储是否正确")
#             if not next_setting:
#                 break
#             cur_setting = next_setting
#
#     def test_target_finish(self):
#         """
#         验证达成目标后活动是否会提前结束
#         :return:
#         """
#         delay_time = 1
#         self.update_delay_time(delay_time)
#
#         open_stage = self.api.open_act(self.data.act_copy)
#
#         finish_num = self.camp_manager.get_setting_by_stage(open_stage).price
#         self.api.update_event_finish(finish_num - 1)
#
#         first = self.camp_copy.get_copy_all()[0]
#         start_re = self.api.copy_start(first.level)
#         start_response = decrypt(start_re.text)
#         verify = parse_json("verify", start_response)
#         self.compare.success_code(start_response, msg=f"开始挑战: 难度={first.level}")
#
#         settle_re = self.api.copy_settle(verify, 1)
#         settle_response = decrypt(settle_re.text)
#         self.compare.success_code(settle_response, msg=f"结算关卡:{first.level} data={settle_response}")
#         end_time = self.time_util.get_current_timestamp() + delay_time * 60 * 1000
#
#         info_re = self.api.copy_info()
#         info_response = decrypt(info_re.text)
#         self.compare.success_code(info_response, msg="活动处于延迟时间内可以拉取信息")
#
#         start_re = self.api.copy_start(first.level)
#         start_response = decrypt(start_re.text)
#         self.compare.error_code(start_response, msg=f"活动处于延迟时间内无法开始挑战: {start_response}")
#
#         buy_re = self.api.copy_buy(1)
#         buy_response = decrypt(buy_re.text)
#         self.compare.error_code(buy_response, msg="活动处于延迟时间内无法购买次数")
#
#         rank_re = self.api.stage_rank()
#         rank_response = decrypt(rank_re.text)
#         self.compare.success_code(rank_response, msg="活动处于延迟时间内可以查看榜单信息")
#
#         while True:
#             cur_time = self.time_util.get_current_timestamp()
#             if cur_time > end_time:
#                 info_re = self.api.copy_info()
#                 info_response = decrypt(info_re.text)
#                 self.compare.error_code(info_response, msg="活动完全结束后无法拉取到活动信息")
#                 break
#             time.sleep(10)
#
#     def test_over_time(self):
#         """
#         测试活动到达时间自然结束
#         :return:
#         """
#         # 延迟时间 和持续时间
#         delay_time, active_time = 60, 60
#         with allure.step(f"将活动的持续时间设置为1分钟"):
#             act_data = CampActData()
#             act_data.act_copy["field"]["`time`"] = active_time
#
#         self.update_delay_time(int(delay_time / 60))
#
#         with allure.step(f"开启活动"):
#             open_stage = self.api.open_act(act_data.act_copy)
#             first_end_time = self.time_util.get_current_timestamp() + active_time * 1000
#             end_time = self.time_util.get_current_timestamp() + active_time * 1000 + delay_time * 1000
#
#         with allure.step(f"获取配置"):
#             # 重新拉取一下配置
#             self.camp_manager.reload()
#             rank_num = self.camp_manager.get_stage_max_rank(open_stage)
#
#         with allure.step(f"添加排名数据"):
#             # 添加超出最大排名2位的排名数据
#             ranks = self.api.add_event_rank(open_stage, rank_num + 2)
#
#         # 是否已经检查过延迟时间内的接口
#         first_check = False
#         # 每十秒检测一次, 如果活动已结束则退出
#         while True:
#             cur_time = self.time_util.get_current_timestamp()
#             # 活动完全结束
#             if cur_time > end_time:
#                 break
#             # 活动已结束但仍然存在
#             if cur_time > first_end_time and not first_check:
#                 with allure.step(f"活动处于延迟时间内,各接口信息检查"):
#                     re = self.api.copy_info()
#                     response = decrypt(re.text)
#                     self.compare.success_code(response, msg="活动处于延迟时间内依然能拉取信息查看")
#
#                     start_re = self.api.copy_start(self.camp_copy.get_copy_all()[0].level)
#                     start_response = decrypt(start_re.text)
#                     self.compare.error_code(start_response, msg="活动处于延迟时间内无法开始挑战")
#
#                     buy_re = self.api.copy_buy(1)
#                     buy_response = decrypt(buy_re.text)
#                     self.compare.error_code(buy_response, msg="活动处于延迟时间内无法购买次数")
#
#                     rank_re = self.api.stage_rank()
#                     rank_response = decrypt(rank_re.text)
#                     self.compare.success_code(rank_response, msg="活动处于延迟时间内可以查看榜单信息")
#                     first_check = True
#             api_log.info(
#                 f"等待活动结束: cur_time={self.time_util.timestamp_to_date(cur_time)}, end_time={self.time_util.timestamp_to_date(end_time)}")
#             time.sleep(10)
#
#         with allure.step(f"验证邮件排名奖励是否正确"):
#             for rank, user in ranks.items():
#                 reward_id = self.camp_manager.get_stage_rank_reward(open_stage, rank)
#                 role_id = user.get("role_id")
#                 mail = self.api.db_utils.get_last_mail(self.data.mail_event_rank, role_id, end_time)
#                 if rank > rank_num:
#                     self.compare.equal_bool(reward_id, False, f"超出排名的玩家没有奖励")
#                     self.compare.equal_bool(mail, False, f"超出排名的玩家应该没有对应的邮件奖励")
#                     continue
#                 setting_drop = self.award_manager.get_award_drop(reward_id)
#                 self.compare.mail_reward(mail, setting_drop)
#
#         with allure.step(f"验证事件结束奖励是否正确"):
#             mail = self.api.db_utils.get_last_mail(self.data.mail_event_reward, self.role_id, end_time)
#             self.compare.equal_bool(mail, False, f"事件目标未达成应该没有对应的事件奖励")
#
#     def test_many_join(self):
#         """
#         多人同时参与活动
#         :return:
#         """
#         self.api.open_act(self.data.act_copy)
#
#         cur_setting = self.camp_copy.get_copy_all()[0]
#         add_finish = list(parse_setting(cur_setting.target).values())[0]
#
#         with allure.step(f"登录另一个账号"):
#             other_api = CampActApi()
#             openid = generate_openid()
#             user_info, host = other_api.login(openid)
#             with allure.step(f"加入阵营"):
#                 camp_api = CampApi(other_api.base_data)
#                 # 解锁阵营所需的关卡
#                 unlock_stage = self.function_unlock.get_value_by_id(self.constant.unlock_camp_id)
#                 self.api.db_utils.insert_user_stage(camp_api.role_id, unlock_stage.stageid)
#                 re = camp_api.join_camp(self.data.camp)
#                 response = decrypt(re.text)
#                 self.compare.success_code(response, msg=f"加入阵营失败: {response}")
#
#         with allure.step(f"参与活动前拉取第一个玩家的数据"):
#             re = self.api.copy_info()
#             response = decrypt(re.text)
#             # 当前进度
#             finish = parse_json("finish", response)
#             first_user_finish = parse_json("userfinish", response)
#
#             self.compare.success_code(response, msg="开启活动后拉取信息code")
#             self.compare.equal(finish, 0, f"当前进度验证")
#             self.compare.equal(first_user_finish, 0, f"当前玩家进度验证")
#
#         with allure.step(f"参与活动前拉取第二个玩家的数据"):
#             re = other_api.copy_info()
#             response = decrypt(re.text)
#             # 当前进度
#             finish = parse_json("finish", response)
#             second_user_finish = parse_json("userfinish", response)
#
#             self.compare.success_code(response, msg="开启活动后拉取信息code")
#             self.compare.equal(finish, 0, f"当前进度验证")
#             self.compare.equal(first_user_finish, 0, f"当前玩家进度验证")
#
#         with allure.step(f"使用第一个号参与活动"):
#             first_start = self.api.copy_start(cur_setting.level)
#             first_start_response = decrypt(first_start.text)
#             first_verify = parse_json("verify", first_start_response)
#
#             first_settle = self.api.copy_settle(first_verify, 1)
#             first_settle_response = decrypt(first_settle.text)
#             self.compare.success_code(first_settle_response, msg="第一个玩家进行结算")
#
#             finish += add_finish
#             first_user_finish += add_finish
#
#         with allure.step(f"使用第二个号参与活动"):
#             second_start = other_api.copy_start(cur_setting.level)
#             second_start_response = decrypt(second_start.text)
#             second_verify = parse_json("verify", second_start_response)
#
#             second_settle = other_api.copy_settle(second_verify, 1)
#             second_settle_response = decrypt(second_settle.text)
#             self.compare.success_code(second_settle_response, msg="第二个玩家进行结算")
#
#             add_finish = list(parse_setting(cur_setting.target).values())[0]
#             finish += add_finish
#             second_user_finish += add_finish
#
#         with allure.step(f"参与活动后拉取第一个玩家的数据"):
#             re = self.api.copy_info()
#             response = decrypt(re.text)
#             # 当前进度
#             get_finish = parse_json("finish", response)
#             after_first_user_finish = parse_json("userfinish", response)
#
#             self.compare.success_code(response, msg="开启活动后拉取信息code")
#             self.compare.equal(get_finish, finish, f"当前进度验证")
#             self.compare.equal(after_first_user_finish, first_user_finish, f"当前玩家进度验证")
#
#         with allure.step(f"参与活动后拉取第二个玩家的数据"):
#             re = other_api.copy_info()
#             response = decrypt(re.text)
#             # 当前进度
#             get_finish = parse_json("finish", response)
#             after_second_user_finish = parse_json("userfinish", response)
#
#             self.compare.success_code(response, msg="开启活动后拉取信息code")
#             self.compare.equal(get_finish, finish, f"当前进度验证")
#             self.compare.equal(after_second_user_finish, second_user_finish, f"当前玩家进度验证")
#
#     def test_concurrent_settle(self):
#         """
#         并发结算
#         :return:
#         """
#         self.api.open_act(self.data.act_copy)
#         cur_setting = self.camp_copy.get_copy_all()[0]
#         add_finish = list(parse_setting(cur_setting.target).values())[0]
#
#         setting_drop = AwardEntity(reward=[])
#         for pid, num in parse_setting(cur_setting.award).items():
#             setting_drop.reward.append({
#                 "pid": pid,
#                 "num": num
#             })
#
#         # 获取info信息
#         info_re = self.api.copy_info()
#         info_response = decrypt(info_re.text)
#         # 免费挑战次数
#         free = parse_json("freecount", info_response)
#         # 当前进度
#         finish = parse_json("finish", info_response)
#         user_finish = parse_json("userfinish", info_response)
#
#         # 免费次数不足时获取门票
#         if free <= 0:
#             with allure.step(f"免费挑战次数不足, 添加门票"):
#                 self.chat_api.chat_talk(f"gm prop {self.data.copy_ticket} 1")
#                 setting_drop.reward.append({
#                     "pid": self.data.copy_ticket,
#                     "num": -1
#                 })
#
#         start_re = self.api.copy_start(cur_setting.level)
#         start_response = decrypt(start_re.text)
#         verify = parse_json("verify", start_response)
#
#         with allure.step(f"进行并发结算"):
#             before_prop = self.api.db_utils.get_user_props(self.role_id)
#             params = {"verify": verify, "complete_status": 1}
#             results = self.api.concurrent_func(self.api.copy_settle, **params)
#             changed_prop = self.api.db_utils.get_user_props(self.role_id)
#
#             success = False
#             for result in results:
#                 response = decrypt(result.text)
#                 if parse_json("code", response) == "1":
#                     self.compare.equal_bool(success, False, "首次成功")
#                     self.compare.changing(response, before_prop, changed_prop, setting_drop,
#                                           blank=[list(parse_setting(cur_setting.target).keys())[0]])
#                     success = True
#             self.compare.equal_bool(success, True, f"两次均未成功")
#
#         # 获取info信息
#         info_re = self.api.copy_info()
#         info_response = decrypt(info_re.text)
#         # 免费挑战次数
#         now_free = parse_json("freecount", info_response)
#         if free > 0:
#             self.compare.equal(now_free, free - 1, f"并发结算后挑战次数扣除验证")
#         # 当前进度
#         now_finish = parse_json("finish", info_response)
#         now_user_finish = parse_json("userfinish", info_response)
#         self.compare.equal(now_finish, finish + add_finish, f"并发结算后当前完成总进度验证")
#         self.compare.equal(now_user_finish, user_finish + add_finish, f"并发结算后当前玩家完成进度验证")
#
#     def update_delay_time(self, minute):
#         """
#         修改配置的活动延迟时间
#         :param minute: 分钟数
#         :return:
#         """
#         with allure.step(f"将活动结束后保留的时间调整为{minute}分钟"):
#             update_count = self.api.db_utils.update_db("basic_global",
#                                               {"value": minute},
#                                               {"id": self.data.delay_close_time})
#             # 刷新缓存
#             if update_count > 0:
#                 self.api.reload_setting()
#
