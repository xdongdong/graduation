from copy import copy
from apis.base_api import BaseApi


class CampApi(BaseApi):
    __join_camp = "/game/camp/join"

    def join_camp(self, camp_id):
        """
        加入阵营
        :param camp_id: 要加入的阵营id
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "campid": camp_id,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__join_camp, data=data)
        return re

    def leave_camp(self):
        """
        退出阵营
        :return:
        """
        self.db_utils.update_db(f"{self.setting.DB}.user_info",
                       {"camp": 0},
                       {"roleid": self.role_id})
