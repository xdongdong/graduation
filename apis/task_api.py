from copy import copy
from apis.base_api import BaseApi
from apis.hero_api import HeroApi
from apis.sign_api import SignApi
from utils.time_utlis import TimeUtils
from utils.util import get_set_nums


class TaskApi(BaseApi):
    # 日常任务
    __get_day_task_info = "/game/daliytask/gettaskInfo"
    __get_day_task_box = "/game/daliytask/openbox"
    __get_day_task_reward = "/game/daliytask/rewardtask"

    TASK_FAIL = 0
    TASK_COMPLETE = 1

    time_utils = TimeUtils()

    def day_task_info(self, kind):
        """
        获取日常任务信息
        :param kind: 1=日常任务 3=周常任务
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "kind": kind,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__get_day_task_info, data=data)
        return re

    def day_task_box(self, box):
        """
        获取日常/周常宝箱奖励
        :param box: 宝箱id
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "boxid": box,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__get_day_task_box, data=data)
        return re

    def day_task_reward(self, task_id):
        """
        获取日常/周常任务奖励
        :param task_id: 任务id
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "taskid": task_id,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__get_day_task_reward, data=data)
        return re

    def do_task_1000(self, para1, para2):
        """
        任务类型1000  累计签到多少天
        :param para1: 1:累计签到 2:连续签到
        :param para2: 天数
        :return:
        """
        sign_api = SignApi(self.base_data)
        if para1 == 1:
            # 获取不重复的数字 para2 个
            random_day = get_set_nums(para2, 1, 100)
            random_day.sort(reverse=True)
            # 循环签到
            for day in set(random_day):
                # 签到
                sign_api.sign()
                # 修改签到日期
                self.update_today_sign(-day)

        elif para1 == 2:
            for day in range(1, para2 + 1):
                # 签到
                sign_api.sign()
                # 修改签到日期
                self.update_today_sign(-(para2 - day))
        else:
            raise ValueError(f"签到任务类型仅支持1和2")

    def test_task_1000(self, para1, para2):
        """
        任务类型1000  累计签到多少天 测试方法
        yield返回任务进度
        :param para1: 1:累计签到 2:连续签到
        :param para2: 天数
        :return:
        """
        sign_api = SignApi(self.base_data)

        if para1 == 1:

            # 未满任务所需天数时
            random_day = get_set_nums(para2 - 1, 1, 100)
            random_day.sort(reverse=True)
            for day in set(random_day):
                # 签到
                sign_api.sign()
                # 修改签到日期
                self.update_today_sign(-day)
            yield para2 - 1

            # 正常完成任务
            sign_api.clear_user_sign()
            self.do_task_1000(para1, para2)
            yield para2

            # 超出任务所需天数
            sign_api.clear_user_sign()
            random_day = get_set_nums(para2 + 1, 1, 100)
            random_day.sort(reverse=True)
            for day in set(random_day):
                # 签到
                sign_api.sign()
                # 修改签到日期
                self.update_today_sign(-day)
            yield para2 + 1
        elif para1 == 2:
            # 连续签到天数不足
            for day in range(1, para2):
                # 签到
                sign_api.sign()
                # 修改签到日期
                self.update_today_sign(-(para2 - day))
            yield para2 - 1

            # 签到天数足够 但非连续
            sign_api.clear_user_sign()
            for day in range(1, para2):
                # 签到
                sign_api.sign()
                # 修改签到日期
                self.update_today_sign(-(para2 - day - 1))
            sign_api.sign()
            yield 1
            sign_api.clear_user_sign()

            # 正常完成任务
            self.do_task_1000(para1, para2)
            yield para2

            # 超出任务所需连续签到天数
            sign_api.clear_user_sign()
            for day in range(1, para2 + 1):
                # 签到
                sign_api.sign()
                # 修改签到日期
                self.update_today_sign(-(para2 - day))
            yield para2 + 1
        else:
            raise ValueError(f"签到任务类型仅支持1和2")

    def test_task_1063(self, para1, para2):
        """
        提升n个英雄的专属武器至xx级
        :param para1: 等级
        :param para2: 英雄数量
        :return:
        """
        hero_api = HeroApi(self.base_data)
        hero = hero_api.get_random_hero(para2)

    def update_today_sign(self, day):
        """
        将今日的签到改到指定的天数
        :param day: 与今日相差多少天
        :return:
        """
        to_day = self.time_utils.update_date(self.time_utils.get_today_datetime(), day=day)
        self.db_utils.update_db(f"{self.setting.DB}.user_sign",
                       {"signtime": to_day, "daytime": to_day.date()},
                       {"daytime": self.time_utils.get_today_date(), "roleid": self.role_id})
