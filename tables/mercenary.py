from abc import ABC
from tables.base import BaseManager
from utils.log import db_log
from utils.mysql import Mysql


class MercenaryIntensifyManager(BaseManager, ABC):
    def __init__(self, conn: Mysql):
        if hasattr(self, "config"):
            return
        self.config = None
        self.id_setting = {}
        self.type_setting = {}
        self.conn = conn
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(MercenaryIntensifyManager, "_instance"):
            MercenaryIntensifyManager._instance = object.__new__(cls)
        return MercenaryIntensifyManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_mercenary_intensify')
        for con in self.config:
            self.id_setting[con.get('id')] = MercenaryIntensifyEntity(con)

        for con in self.config:
            c_type = con.get("type")
            if c_type in self.type_setting.keys():
                value = self.type_setting.get(c_type)
                value.append(MercenaryIntensifyEntity(con))
            else:
                self.type_setting[c_type] = [MercenaryIntensifyEntity(con)]

        db_log.info('加载配置成功: basic_mercenary_intensify')

    def get_settings_by_type(self, s_type):
        return self.type_setting.get(s_type)

    def get_setting_by_id(self, setting_id):
        return self.id_setting.get(setting_id)

    def get_setting_by_type_args(self, s_type, args):
        for setting in self.get_settings_by_type(s_type):
            if setting.args == args:
                return setting


class MercenaryIntensifyEntity:

    def __init__(self, value):
        self.id = value.get('id')
        self.nextid = value.get('nextid')
        self.preid = value.get('preid')
        self.note = value.get('note')
        self.name = value.get('name')
        self.amount = value.get('amount')
        self.type = value.get('type')
        self.args = value.get('args')
        self.stagemercenary = value.get('stagemercenary')