class BaseManager:

    def load_config(self):
        raise NotImplementedError

    def reload(self):
        self.load_config()
