from abc import ABC
from tables.base import BaseManager
from utils.log import db_log
from utils.mysql import Mysql


class MonsterManager(BaseManager, ABC):
    def __init__(self, conn: Mysql):
        if hasattr(self, "monster_config"):
            return
        self.monster_config = None
        self.monster_value = {}
        self.conn = conn
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(MonsterManager, "_instance"):
            MonsterManager._instance = object.__new__(cls)
        return MonsterManager._instance

    def load_config(self):
        self.monster_config = self.conn.getALL('select * from basic_monster')
        for con in self.monster_config:
            self.monster_value[con.get('monsterid')] = MonsterEntity(con)

        db_log.info('加载配置成功: basic_monster')

    def get_monster_by_id(self, monster_id: int):
        result = self.monster_value.get(monster_id)
        if not result:
            raise AssertionError(f"basic_monster中未找到该怪物: {monster_id}")
        return self.monster_value.get(monster_id)


class MonsterEntity:

    def __init__(self, value):
        self.monsterid = value.get('monsterid')
        self.note = value.get('note')
        self.name = value.get('name')
        self.desc = value.get('desc')
        self.head = value.get('head')
        self.body = value.get('body')
        self.mode = value.get('mode')
        self.state = value.get('state')
        self.skill1 = value.get('skill1')
        self.skill2 = value.get('skill2')
        self.attr = value.get('attr')
        self.gap = value.get('gap')
        self.position = value.get('position')
        self.aiid = value.get('aiid')
        self.race = value.get('race')
        self.energy = value.get('energy')
        self.modescale = value.get('modescale')
        self.monsterstyle = value.get('monsterstyle')
        self.aitype = value.get('aitype')
        self.buffvalue = value.get('buffvalue')
        self.firsttarget = value.get('firsttarget')
        self.monster = value.get('monster')
        self.hpscale = value.get('hpscale')
        self.blood = value.get('blood')
        self.viewfield = value.get('viewfield')
        self._with = value.get('with')
        self.gender = value.get('gender')
        self.frozenscale = value.get('frozenscale')
        self.frozenselect = value.get('frozenselect')
