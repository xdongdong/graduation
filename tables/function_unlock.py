from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class FunctionUnlockManager(BaseManager, ABC):

    def __init__(self, conn):
        if hasattr(self, "event_stage_config"):
            return
        self.conn = conn
        self.function_unlock_config = None
        self.function_unlock_values = {}
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(FunctionUnlockManager, "_instance"):
            FunctionUnlockManager._instance = object.__new__(cls)
        return FunctionUnlockManager._instance

    def load_config(self):
        self.function_unlock_config = self.conn.getALL('select * from basic_functionunlock')
        for con in self.function_unlock_config:
            self.function_unlock_values[con.get('id')] = FunctionUnlockEntity(con)
        db_log.error('加载配置成功: basic_functionunlock')

    def get_value_by_id(self, _id):
        result = self.function_unlock_values.get(_id)
        if not result:
            raise AssertionError(f"未找到对应的功能解锁配置: {_id}")
        return result


class FunctionUnlockEntity:
    """basic_function_unlock"""

    def __init__(self, value):
        self.id = value.get('id')
        self.note = value.get('note')
        self.funcid = value.get('funcid')
        self.stageid = value.get('stageid')
        self.noteunlock = value.get('noteunlock')
        self.show = value.get('show')
        self.icon = value.get('icon')
        self.info = value.get('info')
        self.name = value.get('name')
        self.must = value.get('must')
