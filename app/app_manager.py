from airtest.core.api import connect_device, sleep
from airtest.core.error import AdbShellError
from poco.drivers.unity3d import UnityPoco
from app.base_page import BasePage
from utils.log import ui_log


class App(BasePage):

    package = "com.cyou.sstf.gp"
    device = "android://127.0.0.1:5037/127.0.0.1:7555"

    def start_app(self):
        if not self.driver:
            self.driver = connect_device(self.device)
        try:
            self.driver.shell(f"ps | grep {self.package}")
        except AdbShellError as e:
            ui_log.debug(f"app: {self.package}未运行, 启动此app")
            self.driver.start_app(self.package)
        else:
            ui_log.debug(f"app: {self.package}正在运行中, 重启此app")
            self.driver.stop_app(self.package)
            self.driver.start_app(self.package)

        # 等待app完全启动后再生成poco实例
        sleep(10)
        self.poco = UnityPoco(device=self.driver)
        return self

    def quit(self):
        self.driver.stop_app(self.package)

