from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class TaskManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.day_values = {}
        self.day_config = None
        self.conn = conn
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(TaskManager, "_instance"):
            TaskManager._instance = object.__new__(cls)
        return TaskManager._instance

    def load_config(self):
        self.day_config = self.conn.getALL('select * from basic_daliy_task')

        for con in self.day_config:
            self.day_values[con.get('taskid')] = DayTaskEntity(con)

        db_log.info('加载配置成功: basic_daliy_task')

    def get_day_task(self):
        """
        获取所有日常任务
        :return:
        """
        return [task for task in self.day_values.values() if task.kind == 1]

    def get_day_box(self):
        """
        获取日常宝箱任务
        :return:
        """
        return [task for task in self.day_values.values() if task.kind == 2]

    def get_week_task(self):
        """
        获取所有周常任务
        :return:
        """
        return [task for task in self.day_values.values() if task.kind == 3]

    def get_week_box(self):
        """
        获取周常任务宝箱
        :return:
        """
        return [task for task in self.day_values.values() if task.kind == 4]


class DayTaskEntity:

    def __init__(self, value):
        self.taskid = value.get('taskid')
        self.note = value.get('note')
        self.kind = value.get('kind')
        self.order = value.get('order')
        self.active = value.get('active')
        self.jump = value.get('jump')
        self.unlock = value.get('unlock')
        self.type = value.get('type')
        self.para1 = value.get('para1')
        self.para2 = value.get('para2')
        self.schedule = value.get('schedule')
        self.desc = value.get('desc')
        self.award = value.get('award')
        self.icon = value.get('icon')
        self.name = value.get('name')
        self.info = value.get('info')
        self.unlocktext = value.get('unlocktext')
