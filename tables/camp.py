from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class CampManager(BaseManager, ABC):

    def __init__(self, conn):
        if hasattr(self, "event_stage_config"):
            return
        self.conn = conn
        self.event_stage_config = None
        self.event_stage_values = {}
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(CampManager, "_instance"):
            CampManager._instance = object.__new__(cls)
        return CampManager._instance

    def load_config(self):
        self.event_stage_config = self.conn.getALL('select * from basic_camp_event_stage')
        for con in self.event_stage_config:
            self.event_stage_values[con.get('stage')] = CampEventStageEntity(con)
        db_log.info('加载配置成功: basic_camp_event_stage')

    def get_setting_by_stage(self, stage):
        """
        根据stage获取对应的配置
        :param stage:
        :return:
        """
        value: CampEventStageEntity = self.event_stage_values.get(stage)
        if not value:
            raise AssertionError(f"basic_camp_event_stage中未找到该配置: stage={stage}")
        return value

    def get_stage_rank_reward(self, stage, rank):
        """
        获取stage对应的排名奖励
        :param rank: 排名
        :param stage:
        :return:
        """
        setting = self.get_setting_by_stage(stage)
        if setting.listward == -1:
            raise AssertionError(f"stage={stage}, 没有排行奖励配置")
        for rank_value in setting.listward.split("|"):
            rank_setting, reward = rank_value.split(":")
            min_rank, max_rank = rank_setting.split(";")
            if int(min_rank) <= rank < int(max_rank):
                return int(reward)
        return None

    def get_stage_max_rank(self, stage) -> int:
        """
        获取stage对应的排行奖励共有多少名
        :param stage:
        :return:
        """
        setting = self.get_setting_by_stage(stage)
        if setting.listward == -1:
            raise AssertionError(f"stage={stage}, 没有排行奖励配置")
        last = setting.listward.split("|")[-1]
        last_rank, reward = last.split(":")
        min_rank, max_rank = last_rank.split(";")
        return int(max_rank) - 1


class CampEventStageEntity:
    """basic_camp_event_stage"""

    def __init__(self, value):
        self.stage = value.get('stage')
        self.name = value.get('name')
        self.campnote = value.get('campnote')
        self.type = value.get('type')
        self.price = value.get('price')
        self.succeedcondition = value.get('succeedcondition')
        self.decidestage = value.get('decidestage')
        self.winnote = value.get('winnote')
        self.losenote = value.get('losenote')
        self.choose1event = value.get('choose1event')
        self.choose1other = value.get('choose1other')
        self.choose1other = value.get('choose1other')
        self.choose2event = value.get('choose2event')
        self.choose2other = value.get('choose2other')
        self.successaward1 = value.get('successaward1')
        self.successaward2 = value.get('successaward2')
        self.listward = value.get('listward')
        self._or = value.get('or')
        self.buffevent = value.get('buffevent')
        self.conditionaward1 = value.get('conditionaward1')
        self.conditionaward2 = value.get('conditionaward2')
        self.win = value.get('win')
        self.lose = value.get('lose')
        self.winkill = value.get('winkill')
        self.losekill = value.get('losekill')
        self.time = value.get('time')
        self.son = value.get('son')
        self.maybe = value.get('maybe')
        self.activity = value.get('activity')
