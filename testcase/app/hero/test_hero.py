from tables.basic_global import GlobalManager
from tables.hero import HeroManager
from tables.item import ItemManager
from testcase.constant import Constant
from testcase.data.hero_data import HeroData
from utils.comparators import Comparators
from utils.db_utils import DbUtils
from utils.util import generate_openid


class TestAppHero:

    def setup_class(self):
        self.db_utils = DbUtils()
        self.openid = generate_openid()
        self.data = HeroData()
        self.compare = Comparators()
        self.item_manager = ItemManager(self.db_utils.sql_conn)
        self.hero_manager = HeroManager(self.db_utils.sql_conn)

    def test_hero_init(self, app):
        """
        验证初始化英雄
        :param app:
        :return:
        """
        level = app.login(self.openid). \
            goto_hero_page(). \
            get_hero_level(self.data.init_hero)
        self.compare.equal(level, 1, f"验证初始英雄等级")

    def test_add_hero(self, app):
        """
        添加英雄
        :param app:
        :return:
        """
        get_hero = self.item_manager.get_hero_by_quality(self.data.get_hero, self.data.get_hero_quality)
        hero_page = app.login(self.openid).goto_hero_page()
        # 获取英雄前的英雄列表
        before_hero_list = hero_page.get_hero_list()
        for i in before_hero_list:
            print("before_list", i.get_name())
        after_hero_list = hero_page.back_to_lobby(). \
            goto_chat_page(). \
            send_message(f"gm prop {get_hero.aid} 1"). \
            back_to_lobby(). \
            goto_hero_page(). \
            get_hero_list()
        for i in before_hero_list:
            print("before_list", i.get_name())
        for i in after_hero_list:
            print("after_list", i.get_name())
        self.compare.equal(len(after_hero_list) - 1, len(before_hero_list), f"获取英雄后的英雄列表比之前多出一个")

    def test_hero_level_up_less(self, app):
        """
        升级时材料不足
        :return:
        """
        # 登录获取对应的用户id
        lobby_page = app.login(self.openid)
        username, role_id = lobby_page.get_user_role()
        # 切换页面
        hero_page = lobby_page.goto_hero_page()
        # 清除玩家背包内的物品
        self.db_utils.clear_user_package(role_id)
        # 进行英雄升级
        before_level = hero_page.get_hero_level(self.data.init_hero)
        hero_page.switch_to_hero(self.data.init_hero).click_level_up()
        tips = hero_page.get_right_tips()
        after_level = hero_page.get_hero_level(self.data.init_hero)
        self.compare.equal(after_level, before_level, f"升级未成功的情况下等级不变化")
        self.compare.equal(tips.get_text(), "材料不足", f"升级未成功的情况下文本提示内容")

    def test_hero_level_up(self, app):
        """
        升级英雄成功
        :param app:
        :return:
        """
        # 登录获取对应的用户id
        lobby_page = app.login(self.openid)
        username, role_id = lobby_page.get_user_role()
        # 切换页面
        hero_page = lobby_page.goto_hero_page()
        # 清除玩家背包内的物品
        self.db_utils.clear_user_package(role_id)
        # 获取升级消耗
        cost = hero_page.get_hero_level_cost(self.data.init_hero)
        chat_page = hero_page.back_to_lobby().goto_chat_page()
        for pid, num in cost.items():
            chat_page.send_message(f"gm prop {pid} {num}")
        # 切换到英雄界面
        hero_page = chat_page.back_to_lobby().goto_hero_page().switch_to_hero(self.data.init_hero)
        before_level = hero_page.get_hero_level(self.data.init_hero)
        hero_page.click_level_up()
        after_level = hero_page.get_hero_level(self.data.init_hero)

        self.compare.equal(after_level - 1, before_level, f"升级成功后等级验证")

    def test_hero_quick_level(self, app):
        """
        快速升级功能验证
        :param app:
        :return:
        """
        max_level = self.hero_manager.get_hero_max_level(self.data.init_hero)
        hero_cost = self.hero_manager.get_hero_level_cost(self.data.init_hero, level=max_level.level, state=max_level.state)

        chat_page = app.login(self.openid).goto_chat_page()
        for pid, num in hero_cost.items():
            chat_page.send_message(f"gm prop {pid} {num}")

        # 未解锁快速升级时,不会出现快速升级按钮
        hero_page = chat_page.back_to_lobby().goto_hero_page().click_level_up()
        exits_status = hero_page.quick_level_exits(timeout=0)

        self.compare.equal_bool(exits_status, False, message=f"未解锁快速升级时不会出现快速升级按钮")

        # 获取解锁快速升级功能的等级配置
        global_manager = GlobalManager(self.db_utils.sql_conn)
        constant = Constant()
        unlock_level = global_manager.get_value_by_id(constant.quick_level)

        # 解锁快速升级后进行快速升级
        level = hero_page. \
            back_to_lobby(). \
            goto_chat_page(). \
            send_message(f"gm userlevel {unlock_level}").\
            close_alter(). \
            back_to_lobby(). \
            goto_hero_page().\
            switch_to_hero(self.data.init_hero). \
            click_level_up(). \
            click_quick_level().get_hero_level(self.data.init_hero)

        self.compare.equal(level, max_level.level, f"快速升级后等级达到配置最高等级")
