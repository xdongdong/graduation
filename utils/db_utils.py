from time import time
import allure
from settings import get_config
from tables.basic_global import GlobalManager
from tables.mail import MailManager
from testcase.constant import Constant
from utils.GetHashCode import GetHashCode
from utils.log import db_log
from utils.mysql import Mysql
from utils.read_data import ReadData
from utils.redis_client import RedisClient
from utils.time_utlis import TimeUtils


class DbUtils:
    init_flag = False

    def __init__(self):
        if DbUtils.init_flag:
            return
        self.setting = get_config()
        self.db = self.setting.DB
        r = ReadData()
        sql_config = r.get_db_info(self.setting.DB_KEY, self.setting.DB_NAME)
        self.sql_conn = Mysql(sql_config)
        # 连接redis
        api_redis_config = r.get_redis_info(self.setting.REDIS_KEY, db=self.setting.REDIS_API_DB)
        cross_redis_config = r.get_redis_info(self.setting.REDIS_KEY, db=self.setting.REDIS_CROSS_DB)
        self.api_redis = RedisClient(api_redis_config)
        self.cross_redis = RedisClient(cross_redis_config)
        DbUtils.init_flag = True

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(DbUtils, "_instance"):
            DbUtils._instance = object.__new__(cls)
        return DbUtils._instance

    def clear_user_package(self, role_id):
        role_hash = GetHashCode.getHashCode(role_id)
        with allure.step(f"清除玩家背包物品及货币"):
            # 清除背包
            self.sql_conn.delete(f"delete from {self.db}.user_package_{role_hash} where roleid = {role_id}")
            # 清除资源
            self.sql_conn.update(f"update {self.db}.user_info set "
                                 f"gem = 0, coin=0, jewel=0, can=0, campvitality=0, currency=0 where roleid = {role_id}")
            db_log.debug(f"清除玩家背包物品及货币: {role_id}")

    def unlock_quick_function(self, role_id):
        global_manager = GlobalManager(self.sql_conn)
        constant = Constant()
        unlock_level = global_manager.get_value_by_id(constant.quick_level)
        with allure.step(f"解锁快速升级功能, 解锁等级= {unlock_level}"):
            self.sql_conn.update(
                f"update {self.db}.user_info set `level` = {unlock_level} where roleid = {role_id}")
            db_log.debug(f"解锁玩家快速升级功能, 解锁等级 = {unlock_level}, role={role_id}")

    def clear_user_stage(self, role_id):
        with allure.step(f"清除玩家所有关卡信息， role={role_id}"):
            self.sql_conn.delete(f"delete from {self.db}.user_plot_stage where roleid = {role_id}")
            db_log.debug(f"清除玩家所有关卡信息, role={role_id}")

    def insert_user_stage(self, role_id, stage=-1):
        setting = get_config()
        # 获取所有关卡
        with allure.step(f"使玩家通关至对应关卡: {stage}"):
            if int(stage) > 0:
                stages = self.sql_conn.getALL(f"select * from basic_plot_stage where stageid <= {stage}")
            else:
                stages = self.sql_conn.getALL(f"select * from basic_plot_stage")
            insert_stage = []
            insert_sql = f"insert ignore into {setting.DB}.user_plot_stage values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            for stage in stages:
                insert_stage.append(
                    (role_id, stage.get("stageid"), stage.get("difficulty"), stage.get("wave"), 0, 0,
                     int(time() * 1000), None, None, None))

            self.sql_conn.insertMany(insert_sql, insert_stage)
            db_log.debug(f"添加玩家通关关卡信息: role={role_id}, stage={stage}")

    def get_user_props(self, role_id):
        """
        获取玩家所有物品
        :param role_id:
        :return:
        """
        with allure.step(f"获取玩家所有物品"):
            constant = Constant()

            user_props = {}
            user_hash = GetHashCode.getHashCode(role_id)

            # 处理背包物品
            user_prop = self.sql_conn.getALL(
                f'select * from {self.db}.user_package_{user_hash} where roleid = {role_id}')
            for prop in user_prop:
                user_props[str(prop.get("pid"))] = prop.get("num")

            # 处理玩家资源
            user_resource = self.sql_conn.getRow(f'select * from {self.db}.user_info where roleid = {role_id}')
            user_props[constant.user_resource.get("gem")] = user_resource.get("gem")
            user_props[constant.user_resource.get("coin")] = user_resource.get("coin")
            user_props[constant.user_resource.get("jewel")] = user_resource.get("jewel")
            user_props[constant.user_resource.get("ce") or "ce"] = user_resource.get("ce")
            user_props[constant.user_resource.get("can") or "can"] = user_resource.get("can")
            user_props[constant.user_resource.get("exp") or "exp"] = user_resource.get("exp")
            user_props[constant.user_resource.get("level") or "level"] = user_resource.get("level")
            user_props[constant.user_resource.get("currency1") or "currency1"] = user_resource.get("currency1")
            db_log.debug(f"获取玩家所有物品, role={role_id}, props={user_props}")
            return user_props

    def get_role_by_name(self, name):
        with allure.step(f"根据名称获取玩家role_id"):
            role = self.sql_conn.getOne(f"select roleid from {self.db}.user_info where nickname='{name}'")
            return role

    def get_user_hero(self, role_id):
        """
        获取玩家所有英雄
        :return:
        """
        user_hash = GetHashCode.getHashCode(role_id)
        with allure.step(f"获取玩家所有英雄"):
            hero = self.sql_conn.getALL(f"select  * from {self.db}.user_hero_{user_hash} where roleid = {role_id}")

            return hero

    def delete_user_mail(self, role_id, mid=None):
        """
        清除玩家邮件
        :param role_id: 获取哪个玩家的邮件
        :param mid: 邮件id
        :return:
        """
        user_hash = GetHashCode.getHashCode(str(role_id))
        with allure.step(f"清除玩家邮件: role={role_id}, mid={mid}"):
            if mid:
                mail_manager = MailManager(self.sql_conn)
                mail = mail_manager.get_setting_by_id(mid)
                self.sql_conn.delete(
                    f"delete from {self.db}.user_mail_{user_hash} where sender='{mail.sender}' and title='{mail.name}' "
                    f"and content='{mail.info}' and roleid={role_id}"
                )
            else:
                self.sql_conn.delete(f"delete from {self.db}.user_mail_{user_hash} where roleid={role_id}")

    def get_last_mail(self, mid, role_id, end_time, offset=30):
        """
        获取最后一封mid的邮件
        :param role_id: 获取哪个玩家的邮件
        :param mid: 邮件id
        :param end_time: 结束时间
        :param offset: 结束时间的偏移量
        :return:
        """
        mail_manager = MailManager(self.sql_conn)
        time_util = TimeUtils()

        user_hash = GetHashCode.getHashCode(str(role_id))
        mail = mail_manager.get_setting_by_id(mid)

        last_mail = self.sql_conn.getRow(
            f"select * from {self.db}.user_mail_{user_hash} where sender='{mail.sender}' and title='{mail.name}' "
            f"and content='{mail.info}' and roleid={role_id} order by sendtime desc"
        )
        if not last_mail:
            return None
        # 时间范围
        send_time = last_mail.get("sendtime")
        if abs(send_time - end_time) > offset * 1000:
            db_log.error(
                f"结束时间为: {time_util.timestamp_to_date(end_time)} 最后一封邮件的发送时间为: {time_util.timestamp_to_date(send_time)}")
            return None
        return last_mail

    def clear_db_data(self, tables):
        """
        清除数据库数据
        :return:
        """
        with allure.step(f"清除数据库数据: {tables}"):
            for table in tables:
                self.sql_conn.delete(f"TRUNCATE TABLE {table};")

    def clear_redis_data(self, match_key):
        """
        清除缓存数据
        :param match_key: key的匹配表达式
        :return:
        """
        with allure.step(f"清除匹配到的所有key： {match_key}"):
            pos, keys = self.api_redis.conn.scan(cursor=0, match=match_key, count=99999)
            for key in keys:
                self.api_redis.conn.delete(key)

    def update_db(self, table, update: dict, condition=None) -> int:
        """
        修改数据库
        :param table: 表
        :param update: 要更新的值
        :param condition: 条件
        :return:
        """
        if condition is None:
            condition = {}
        update_list = [f'{key} = "{value}"' for key, value in update.items() if value is not None]
        condition_list = [f'{key} = "{value}"' for key, value in condition.items() if value is not None]

        count = self.sql_conn.update(
            f"update {table} set {','.join(update_list)} where {'and '.join(condition_list)}"
        )
        return count
