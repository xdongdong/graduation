from copy import copy

import allure

from apis.base_api import BaseApi
from testcase.data.train_data import TrainData


class TrainApi(BaseApi):
    """
    模拟训练
    """
    __train_info_path = "/game/train/info"
    __train_start_path = "/game/train/start"
    __train_save_path = "/game/train/savebuff"
    __train_restart_path = "/game/train/restart"
    __train_settle_path = "/game/train/settle"

    def train_info(self):
        """
        拉取副本详情
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__train_info_path, data=data)
        return re

    def train_start(self):
        """
        开始挑战
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__train_start_path, data=data)
        return re

    def train_save(self, verify, choose_buff, hero=None):
        """
        关卡保存波次
        battle 信息示例
        {
            "battle":{
                "battlehero":{
                    "100124410":{
                        "hid":"100124410",
                        "hurt":[
                            200000,
                            0,
                            0
                        ],
                        "eng":129.64,
                        "heroid":1001002,
                        "hp":10000,
                        "site":210028
                    }
                },
                "nowdeathnum":1,
                "wave":1,
                "battlebeast":{
                    "perid":1011001,
                    "site":140025,
                    "hurt":[
                        0,
                        0,
                        0
                    ],
                    "hp":10000,
                    "skillscd":[
                        {
                            "vid":5910121,
                            "cd":0
                        },
                    ]
                },
                "leaderhp":10000,
                "monsterdeath":{
                    "1102208":0,
                }
            },
        }
        :param verify: 验证码
        :param choose_buff: 选择的buff id
        :param hero: 英雄信息 自己测试不需要传
        :return:
        """
        data = copy(self.user_info)
        # 不需要这个信息， 随便放
        battle = {
            "leaderhp": 10000
        }
        encrypt_data = {
            "verify": verify,
            "choosebuff": choose_buff,
            "battle": battle
        }
        data["encryptdata"] = encrypt_data

        re = self.http_post(self.__train_save_path, data=data)
        return re

    def train_settle(self, verify, complete_status):
        """
        关卡结算
        :param verify: 验证码
        :param complete_status: 1 成功  0 失败
        :return:
        """
        data = copy(self.user_info)

        encrypt_data = {
            "verify": verify,
            "completestatus": complete_status,
        }
        data["encryptdata"] = encrypt_data

        re = self.http_post(self.__train_settle_path, data=data)
        return re

    def train_restart(self):
        """
        关卡重开
        :return:
        """
        data = copy(self.user_info)

        re = self.http_post(self.__train_restart_path, data=data)
        return re

    def clear_user_train(self):
        with allure.step("删除玩家模拟训练的数据"):
            self.db_utils.sql_conn.delete(f"delete from {self.setting.DB}.user_train_info where roleid = {self.role_id}")
            self.db_utils.sql_conn.delete(f"delete from {self.setting.DB}.user_train_rank where roleid = {self.role_id}")

    def unlock_train(self):
        """
        解锁模拟训练
        :return:
        """
        data = TrainData()
        # 获取解锁关卡
        unlock_stage = self.db_utils.sql_conn.getOne(data.get_unlock_sql)
        with allure.step(f"解锁模拟训练: {unlock_stage}"):
            self.db_utils.insert_user_stage(self.role_id, unlock_stage)
