from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class LeaderManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.conn = conn
        self.config = None
        self.leader_skill = {}

        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(LeaderManager, "_instance"):
            LeaderManager._instance = object.__new__(cls)
        return LeaderManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_leader_skill')

        for con in self.config:
            leader_id = con.get("leaderid")
            if leader_id in self.leader_skill.keys():
                value = self.leader_skill.get(leader_id)
                value.append(LeaderSkillEntity(con))
            else:
                self.leader_skill[leader_id] = [LeaderSkillEntity(con)]

        db_log.info('加载配置成功: basic_tech')

    def get_skill_by_leader(self, leader_id):
        result = self.leader_skill.get(leader_id)
        if not result:
            raise AssertionError(f"主角技能表中没有该主角信息: {leader_id}")
        return result


class LeaderSkillEntity:

    def __init__(self, value):
        self.leaderid = value.get('leaderid')
        self.id = value.get('id')
        self.groupid = value.get('groupid')
        self.preid = value.get('preid')
        self.tier = value.get('tier')
        self.connect = value.get('connect')
        self.coordinate = value.get('coordinate')
        self.note = value.get('note')
        self.info = value.get('info')
        self.icon = value.get('icon')
        self.type = value.get('type')
        self.talentnum = value.get('talentnum')
        self.paddid = value.get('paddid')
        self.skillvid = value.get('skillvid')
        self.leaderattr = value.get('leaderattr')
        self.name = value.get('name')
        self.leadertype = value.get('leadertype')
