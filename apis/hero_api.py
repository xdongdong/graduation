from copy import copy
from random import choice

import allure

from apis.base_api import BaseApi
from apis.chat_api import ChatApi
from tables.item import ItemManager
from utils.GetHashCode import GetHashCode
from utils.param_encrypt import decrypt
from utils.util import parse_json


class HeroApi(BaseApi):
    # 英雄
    __buy_limit_path = "/game/hero/buyLimit"
    __hero_info_path = "/game/hero/getHeroInfo"
    __lock_hero_path = "/game/hero/lockhero"
    __hero_skill_path = "/game/hero/skillInfo"
    # 升星
    __next_star_path = "/game/hero/getPromoteStarInfo"
    __quick_star_path = "/game/hero/oneKeyStar"
    __star_up_path = "/game/hero/promoteStar"
    # 升级
    __break_level_path = "/game/hero/levelbreak"
    __level_up_path = "/game/hero/levelup"
    __quick_level_path = "/game/hero/heroquicklevelup"
    # 共鸣
    __resonance_info_path = "/game/hero/resonanceInfo"
    __add_resonance_path = "/game/hero/addResonance"
    __remove_master_path = "/game/hero/removeMasterResonance"
    __remove_resonance_path = "/game/hero/removeResonance"
    __clean_cd_path = "/game/hero/clearResonanceCd"
    __resonance_list_path = "/game/hero/resonanceList"
    # 专属
    __exclusive_info_path = "/game/exclusive/exclusiveInfo"
    __exclusive_skin_path = "/game/exclusive/allSkin"
    __exclusive_level_path = "/game/exclusive/exclusiveUplevel"
    __exclusive_quick_level_path = "/game/exclusive/exclusivequicklevelup"
    __switch_skin_path = "/game/exclusive/switchSkin"

    def get_hero_info(self, hid):
        """
        获取英雄信息
        :param hid:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__hero_info_path, data=data)
        return re

    def lock_hero(self, hid, paramtype):
        """
        锁定/解锁英雄
        :param paramtype:  0 解锁 1锁定
        :param hid:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
            "paramtype": paramtype
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__lock_hero_path, data=data)
        return re

    def hero_skill_info(self, hid):
        """
        拉取英雄全部技能
        :param hid:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__hero_skill_path, data=data)
        return re

    def buy_limit(self):
        """
        购买英雄数量上限
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__buy_limit_path, data=data)
        return re

    def level_up(self, hid):
        """
        英雄升级
        :param hid:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__level_up_path, data=data)
        return re

    def quick_level(self, hid, tolevel, tostate):
        """
        快速升级
        :param hid:
        :param tolevel: 升到得等级
        :param tostate: 升到得阶级
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
            "tolevel": tolevel,
            "tostate": tostate
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__quick_level_path, data=data)
        return re

    def break_level(self, hid):
        """
        英雄突破等级
        :param hid:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__break_level_path, data=data)
        return re

    def get_next_star_info(self, hid):
        """
        获取英雄下一次升星属性详情
        :param hid:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__next_star_path, data=data)
        return re

    def quick_star(self, hids, tp):
        """
        英雄一键升星
        :param hids:
        :param tp: 1-一键升星，2-智能升星
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hids": hids,
            "type": tp
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__quick_star_path, data=data)
        return re

    def star_up(self, hid, uhids: list):
        """
        英雄升星
        :param hid:
        :param uhids: 消耗的英雄唯一主键数组
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
            "uhids": uhids
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__star_up_path, data=data)
        return re

    def resonance_list(self):
        """
        获得可以所有被共鸣者并且不是共鸣者的信息列表
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__resonance_list_path, data=data)
        return re

    def get_resonance_info(self):
        """
        获取共鸣详情
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__resonance_info_path, data=data)
        return re

    def add_resonance(self, hid, **kwargs):
        """
        添加共鸣 或给已经有共鸣的英雄添加被共鸣者
        :param kwargs: fhid：添加到被共鸣槽位的英雄 trench: 添加到共鸣的哪个槽位
        :param hid: 需要添加共鸣的英雄唯一主键
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
            **kwargs
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__add_resonance_path, data=data)
        return re

    def remove_master_resonance(self, hid):
        """
        移除主共鸣者
        :param hid: 被移除的hero唯一ID
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__remove_master_path, data=data)
        return re

    def remove_resonance(self, hid, fhid, trench, tp):
        """
        移除被共鸣者
        :param hid:需要卸下共鸣的英雄唯一主键
        :param fhid: 被卸下的英雄唯一主键
        :param trench: 卸下的槽位,最大为6
        :param tp: 是否卸下被共鸣者装备 1是 0不是
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
            "fhid": fhid,
            "trench": trench,
            "type": tp
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__remove_resonance_path, data=data)
        return re

    def clean_resonance_cd(self, hid, trench):
        """
        清除共鸣槽位CD
        :param hid: 主共鸣英雄唯一ID
        :param trench: 槽位ID
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "hid": hid,
            "trench": trench,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__clean_cd_path, data=data)
        return re

    def get_exclusive(self, upid):
        """
        获取专属详情
        :param upid: 专属唯一ID
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            # 写的是upid 服务器实际用hid去查的专属
            "upid": upid,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__exclusive_info_path, data=data)
        return re

    def get_exclusive_allSkin(self):
        """
        获取已拥有的所有专属皮肤
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__exclusive_skin_path, data=data)
        return re

    def exclusive_level(self, upid):
        """
        专属升级
        :param upid:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "upid": upid,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__exclusive_level_path, data=data)
        return re

    def exclusive_quick_level(self, upid, tolevel):
        """
        专属装备快速升级
        :param upid:
        :param tolevel:
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "upid": upid,
            "tolevel": tolevel
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__exclusive_quick_level_path, data=data)
        return re

    def exclusive_switch_skin(self, upid, skinid):
        """
        专属切换皮肤
        :param upid:
        :param skinid: 皮肤ID
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "upid": upid,
            "skinid": skinid
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__switch_skin_path, data=data)
        return re

    def update_hero_num(self, num):
        """
        将玩家的英雄上限修改为无上限
        :return:
        """
        with allure.step(f"将英雄存储上限修改为{num}个"):
            self.db_utils.sql_conn.insertOne(
                f"insert into {self.setting.DB}.user_hero_buy_num values({self.role_id}, {num}, 3)")

    def get_exclusive_db_data(self, upid=None, hid=None):
        """
        获取数据库专属信息
        :param hid:  英雄唯一ID
        :param upid: 专属唯一ID
        :return:
        """
        with allure.step(f"获取数据库专属武器信息: upid={upid}, hid={hid}"):
            if upid:
                exclusive = self.db_utils.sql_conn.getRow(
                    f"select * from {self.setting.DB}.user_hero_exclusive where upid = {upid}")
            elif hid:
                exclusive = self.db_utils.sql_conn.getRow(
                    f"select * from {self.setting.DB}.user_hero_exclusive where hid = {hid}")
            else:
                raise ValueError("upid 和 hid必须传一个")
            return exclusive

    def get_hero_db_data(self, hid):
        """
        获取英雄
        :param hid: 英雄唯一ID
        :return:
        """
        user_hash = GetHashCode.getHashCode(self.role_id)

        with allure.step(f"获取数据库英雄信息: hash={user_hash}, hid={hid}"):
            hero = self.db_utils.sql_conn.getRow(
                f"select * from {self.setting.DB}.user_hero_{user_hash} where hid = {hid}")

        return hero

    def update_hero(self, hid, update: dict):
        """
        修改英雄数据
        :param hid: 英雄唯一ID
        :param update: 要修改的键值
        :return:
        """
        user_hash = GetHashCode.getHashCode(self.role_id)

        with allure.step(f"修改英雄属性: hash={user_hash} hid={hid}, update={update}"):
            self.db_utils.update_db(f"{self.setting.DB}.user_hero_{user_hash}", update, {"hid": hid})
        return self.get_hero_db_data(hid)

    def get_hero(self, hero_id, quality, num=1):
        """
        获取英雄
        :param hero_id:
        :param quality:
        :param num:
        :return:
        """
        chat_api = ChatApi(self.base_data)
        item_manager = ItemManager(self.db_utils.sql_conn)
        hero = item_manager.get_hero_by_quality(hero_id, quality)

        with allure.step(f"获取英雄: hero={hero}, quality={quality}, num={num}"):
            re = chat_api.chat_talk(f"gm prop {hero.aid} {num}")
            response = decrypt(re.text)
            ret_hero = parse_json("changed", response).get("hero")
        if not ret_hero:
            raise AssertionError(f"获取英雄失败: hero={hero}, quality={quality}, num={num}, response={response}")
        return ret_hero

    def get_random_hero(self, num):
        """
        随机获取num个英雄
        :param num:  num
        :return:
        """
        item_manager = ItemManager(self.db_utils.sql_conn)
        # 61类型为英雄
        hero = item_manager.get_item_by_type(61)
        chat_api = ChatApi()
        results = []
        for _ in range(num):
            get_hero = choice(hero)
            with allure.step(f"获取英雄: hero={get_hero}, quality={get_hero.quality}, num={1}"):
                re = chat_api.chat_talk(f"gm prop {get_hero.aid} {num}")
                response = decrypt(re.text)
                ret_hero = parse_json("changed", response).get("hero")
            if not ret_hero:
                raise AssertionError(f"获取英雄失败: hero={hero}, quality={get_hero.quality}, num={num}, response={response}")
            results.append(ret_hero)
        return results

    def get_unlock_skin(self, pid):
        """
        获取玩家当前某个专属已解锁的所有皮肤
        :param pid: 专属pid
        :return:
        """
        with allure.step(f"获取玩家专属={pid}的所有已解锁皮肤"):
            db_skin = self.db_utils.sql_conn.getLine(
                f"select skinid from {self.setting.DB}.user_exclusive_skin where roleid ={self.role_id} and exclusiveid = {pid}"
            )
        return db_skin

    def update_exclusive(self, update, hid=None, upid=None):
        """
        修改专属属性
        :return:
        """
        with allure.step(f"修改数据库专属武器信息: upid={upid}, hid={hid} update={update}"):
            condition = {}
            if upid:
                condition["upid"] = upid
            elif hid:
                condition["hid"] = hid
            else:
                raise ValueError("upid 和 hid必须传一个")

            self.db_utils.update_db(f"{self.setting.DB}.user_hero_exclusive", update, condition)
            return self.get_exclusive_db_data(upid=upid, hid=hid)

    def get_db_resonance(self, hid):
        """
        获取英雄数据共鸣信息
        :param hid:
        :return:
        """
        with allure.step(f"获取英雄数据库共鸣信息: {hid}"):
            result = self.db_utils.sql_conn.getRow(f"select * from user_hero_resonance where hid = {hid}")
        return result

    def clear_exclusive_skin(self):
        """
        清除玩家所有专属皮肤
        :return:
        """
        # 结尾为1的皮肤id为初始皮肤id
        with allure.step(f"清除玩家所有专属皮肤"):
            self.db_utils.sql_conn.delete(
                f"delete from {self.setting.DB}.user_exclusive_skin where roleid={self.role_id} and RIGHT(skinid, 1) != 1")

    def clear_hero(self):
        """
        清除玩家英雄数据
        :return:
        """
        with allure.step(f"清除玩家所有英雄数据"):
            user_hash = GetHashCode.getHashCode(self.role_id)
            self.db_utils.sql_conn.delete(
                f"delete from {self.setting.DB}.user_hero_{user_hash} where roleid = {self.role_id}")
            self.db_utils.sql_conn.update(
                f"update {self.setting.DB}.user_hero_equipment_{user_hash} set hid = '(NULL)' where roleid = {self.role_id}")
            self.db_utils.sql_conn.delete(
                f"delete from {self.setting.DB}.user_hero_exclusive where roleid = {self.role_id}")
            self.db_utils.sql_conn.delete(
                f"delete from {self.setting.DB}.user_hero_resonance where roleid = {self.role_id}")
            self.db_utils.sql_conn.delete(f"delete from {self.setting.DB}.user_hero_skin where roleid = {self.role_id}")
            self.db_utils.sql_conn.delete(
                f"delete from {self.setting.DB}.user_hero_talent where roleid = {self.role_id}")
