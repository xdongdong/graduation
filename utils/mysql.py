import pymysql
from utils.log import db_log


class Mysql:

    def __init__(self, _config):
        """
        :param _config: 连接配置
        """
        self.connection = pymysql.connect(host=_config.get('host'),
                                          port=_config.get('port'),
                                          user=_config.get('username'),
                                          password=_config.get('password'),
                                          db=_config.get('db'),
                                          autocommit=True,  # 自动提交
                                          cursorclass=pymysql.cursors.DictCursor)

    def insertOne(self, sql, *params):
        """
        :param sql:  sql语句
        :param params: 占位符匹配参数
        :return:count 受影响的行数
        """
        db_log.info(f"执行单条插入语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            try:
                count = cursor.execute(sql, params)
            except Exception as e:
                db_log.info("%s 执行失败: %s" % (sql, str(e)))
                return None
            return count

    def insertMany(self, sql, params):
        """
        :param sql:sql语句
        :param params:要插入的记录数据tuple(tuple)/list[list]
        :return: count 受影响的行数
        """
        db_log.info(f"执行批量插入语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            try:
                count = cursor.executemany(sql, params)
            except Exception as e:
                db_log.info("%s 执行失败: %s" % (sql, str(e)))
                return None
            return count

    def update(self, sql, *params):
        """
        :param sql: sql
        :param params: 要更新的值 tuple/list
        :return: count 受影响的行数
        """
        db_log.info(f"执行更新语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            try:
                count = cursor.execute(sql, params)
            except Exception as e:
                db_log.info("%s 执行失败: %s" % (sql, str(e)))
                return None
            return count

    def delete(self, sql, *params):
        """
        :param sql: sql格式及条件，使用(%s,%s)
        :param params: 要删除的条件 值 tuple/list
        :return: count 受影响的行数
        """
        db_log.info(f"执行删除语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            try:
                count = cursor.execute(sql, params)
            except Exception as e:
                db_log.info("%s 执行失败: %s" % (sql, str(e)))
                return None
            return count

    def getRow(self, sql, *params):
        """
        :param sql: sql语句
        :param params: 占位符匹配参数
        :return:获取单行数据
        """
        db_log.info(f"执行获取一行语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            results = cursor.fetchall()
            try:
                results = results[0]
            except Exception as e:
                db_log.info('%s 查询失败: %s' % (sql, str(e)))
                return None
            return results

    def getLine(self, sql, *params):
        """
        :param sql: sql语句
        :param params: 占位符匹配参数
        :return: 获取单列数据
        """
        db_log.info(f"执行获取一列语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            results = cursor.fetchall()
            results_list = [list(result.values())[0] for result in results]
            return results_list

    def getOne(self, sql, *params):
        """
        :param sql: sql语句
        :param params: 占位符匹配参数
        :return: 获取单个数据
        """
        db_log.info(f"执行获取单个数据语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            results = cursor.fetchall()
            result = None
            if results:
                result = list(results[0].values())[0]
            return result

    def getALL(self, sql, *params):
        """
         :param sql: sql语句
         :param params: 占位符匹配参数
         :return: 获取所有数据
         """
        db_log.info(f"执行查询语句: {sql} , {params}")
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            results = cursor.fetchall()
            results = list(results)
            return results

    def dispose(self):
        """
        @summary: 释放资源
        """
        self.connection.close()
