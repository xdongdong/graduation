from copy import copy
from datetime import  date
import allure
from apis.base_api import BaseApi
from tables.monster import MonsterManager
from tables.zombies import ZombiesManager
from testcase.data.zombies_data import ZombiesData
from utils.log import api_log
from utils.time_utlis import TimeUtils


class ZombiesApi(BaseApi):
    __zombies_info = "/game/zombies/info"
    __user_integral = "/game/zombies/userintegral"

    __rank_award = "/game/zombies/rankaward"
    __integral_award = "/game/zombies/integralaward"
    __wave_award = "/game/zombies/waveaward"
    __integral_rank = "/game/zombies/integralrank"

    __zombies_start = "/game/zombies/start"
    __zombies_save = "/game/zombies/savewave"
    __zombies_settle = "/game/zombies/savewave"
    __zombies_abandon = "/game/zombies/abandon"

    __save_plan = "/game/zombies/saveplan"
    __fight_info = "/game/zombies/figthinfo"

    data = ZombiesData()

    def get_zombies_info(self):
        """
        获取丧尸入侵活动详情
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__zombies_info, data=data)
        return re

    def get_user_integral(self):
        """
        获取玩家战队积分详情
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__user_integral, data=data)
        return re

    def get_rank_award(self):
        """
        获取排行奖励信息
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__rank_award, data=data)
        return re

    def get_integral_award(self):
        """
        获取积分奖励信息
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__integral_award, data=data)
        return re

    def get_wave_award(self):
        """
        获取波次奖励信息
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__wave_award, data=data)
        return re

    def get_integral_rank(self, page=1, count=10):
        """
        获取积分排行榜信息
        :param page: 第几页
        :param count: 多少条数据
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "start": page,
            "ends": count
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__integral_rank, data=data)
        return re

    def zombies_start(self):
        """
        开始挑战
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__zombies_start, data=data)
        return re

    def save_plan(self, plan):
        """
        保存上阵阵容信息
        :param plan:  阵容键值对（键：玩家id ，值：选择的hid数组）
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "plan": plan,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__save_plan, data=data)
        return re

    def get_fight_info(self):
        """
        获取战斗数据信息
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__fight_info, data=data)
        return re

    def zombies_save(self, verify, buff, battle):
        """
        保存波次数据
        :param verify: 验证码
        :param buff: buff信息
        :param battle: 战斗相关信息
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "verify": verify,
            "buff": buff,
            "battle": battle,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__zombies_save, data=data)
        return re

    def zombies_settle(self, verify, status):
        """
        结算关卡
        :param verify: 验证码
        :param status: 0失败 1成功
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "verify": verify,
            "status": status,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__zombies_settle, data=data)
        return re

    def zombies_abandon(self):
        """
        放弃继续战斗 立即结算
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__zombies_abandon, data=data)
        return re

    def open_act(self, run_time=None):
        """
        开启丧尸入侵活动
        :param run_time: 持续多长时间
        :return: 活动当前配置
        """
        data = ZombiesData()
        time_utils = TimeUtils()
        update = {
            "actswitch": 1,
            "begintime": f"5;{time_utils.get_today_date()}"
        }
        if run_time:
            update["lasttime"] = run_time
        condition = {
            "id": data.act_id
        }
        result = self.db_utils.update_db("act_center", update, condition)
        if result > 0:
            self.reload_setting()
        act_config = self.db_utils.sql_conn.getRow(f"select * from act_center where id={data.act_id}")
        act_config["start_date"] = time_utils.get_today_date()
        return act_config

    def count_score(self, monsters, wave):
        """
        计算丧尸入侵分数
        :param monsters: 击杀的怪物
        :param wave: 波次
        :return:
        """
        monster_manager = MonsterManager(self.db_utils.sql_conn)
        score_manager = ZombiesManager(self.db_utils.sql_conn)
        score = 0
        for monster_id, num in monsters.items():
            monster = monster_manager.get_monster_by_id(int(monster_id))
            score += (score_manager.get_score_by_type_num(1, monster.state) * num)
            print(f"monster = {monster.state}, num = {num} score = {score_manager.get_score_by_type_num(1, monster.state) * num}")
        api_log.info(f"计算完杀怪积分: {score}")
        wave_scores = score_manager.get_score_by_type(2)
        for wave_score in wave_scores:
            if wave_score.num < wave:
                score += wave_score.integral
        return score

    def add_zombies_team_rank(self, open_day: date, num, multi=1):
        """
        添加丧尸入侵排行榜
        :param multi: 每一位的分数是排名的多少倍
        :param num: 添加多少位
        :param open_day: 要添加排行榜的活动开启日期
        :return:
        """
        rank = {}
        with allure.step(f"添加丧尸入侵战队排行榜: open_day={open_day} 添加排行榜人数={num}, 排行榜分值为排名的几倍={multi}"):
            teams = self.db_utils.sql_conn.getLine(f"select teamid from {self.setting.DB}.team_info limit {num}")
            act_num = num if len(teams) >= num else len(teams)
            api_log.info(f"预期添加排名人数为: {num}, 实际添加排名人数为: {act_num}")
            for i in range(1, act_num + 1):
                team = teams[i - 1]
                score = (num - i) + 1
                # 第二个排名 构建相同的排名值, 但时间顺序在后面
                if i == 2:
                    score = score - 1
                score = TimeUtils().generate_time_value(score * multi)
                self.db_utils.api_redis.conn.zadd(self.data.team_rank_key.format(open_day=open_day), {
                    f'{team}': score
                })
                rank[i] = {"team": team, "score": score}
        return rank
