from copy import copy
from time import time
from apis.base_api import BaseApi


class ChatApi(BaseApi):
    __world_talk_url = '/game/chat/chatTalk'
    __user_talk_url = "/game/chat/user/talk"

    def chat_talk(self, msg):
        """
        发送世界聊天
        :param msg: 世界聊天消息
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"chattype": 2, "type": 0, "wtime": int(time() * 1000), "msg": msg}
        re = self.http_post(self.__world_talk_url, data=data)
        return re

    def user_chat_talk(self, msg, to_user):
        """
        发送私聊
        :param to_user: 发送给谁
        :param msg: 玩家私聊信息
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"type": 0, "froleid": to_user, "msg": msg}
        re = self.http_post(self.__user_talk_url, data=data)
        return re
