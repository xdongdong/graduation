from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class GlobalManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.conn = conn
        self.config = None
        self.values = {}
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(GlobalManager, "_instance"):
            GlobalManager._instance = object.__new__(cls)
        return GlobalManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_global')
        for con in self.config:
            self.values[con.get('id')] = GlobalEntity(con)
        db_log.info('加载配置成功: basic_global')

    def get_value_by_id(self, _id):
        value = self.values.get(_id)
        if not value:
            raise AssertionError(f"basic_global中找不到该数据: {_id}")
        db_log.info(f"根据id获取到global配置: {_id}, {value.value}")
        return value.value


class GlobalEntity:
    """basic_global"""

    def __init__(self, value):
        self.id = value.get('id')
        self.value = value.get('value')
        self.note = value.get('note')
