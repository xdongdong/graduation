from copy import copy
from apis.base_api import BaseApi


class ItemApi(BaseApi):
    # 英雄
    __open_prop_path = "/game/props/open"


    def open_prop(self, package_id, num, **kwargs):
        """
        开启物品
        :param package_id: 要开启的物品
        :param num: 数量
        :param kwargs: 可选参数  pid: {pid: num} 选择类物品
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "packageid": package_id,
            "num": num,
            **kwargs
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__open_prop_path, data=data)
        return re
