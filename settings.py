import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = BASE_DIR + '/data/'
LOG_DIR = BASE_DIR + '/log/'
ENV_NAME = "DOOM_ENV"


class Config:
    # 根据环境变量取配置
    HOST = ""
    DB_NAME = "game_doom_sys"
    GENERATE_REPORT = True
    REPORT_DIR = BASE_DIR + '/allure-report/'
    REPORT_TMP_DIR = BASE_DIR + '/allure-results/'

    # 测试平台服务的地址
    TEST_PLATFORM_HOST = "http://xxxxx:5063"
    # 上传用例的接口
    TEST_UPLOAD_ADDR = TEST_PLATFORM_HOST + "/case/loadcase"


class LocalConfig(Config):
    ENV = "local 啄木鸟"
    ZONE = 997
    ZONE_NAME = "啄木鸟"
    DB_KEY = "local"
    REDIS_KEY = "local"
    DB = "game_doom_server_1001"
    REDIS_API_DB = 42
    REDIS_CROSS_DB = 41


class StageConfig(Config):
    HOST = ""
    ENV = "审核服"
    ZONE = 1001
    DB_KEY = "audit"
    REDIS_KEY = "audit"
    DB = f"game_doom_server"
    REDIS_API_DB = 2
    REDIS_CROSS_DB = 1


class TestConfig(Config):
    ENV = "test"
    ZONE = 1001
    ZONE_NAME = "测试1服"
    DB_KEY = "test"
    REDIS_KEY = "test"
    DB = f"game_doom_server_{ZONE}"
    REDIS_API_DB = 42
    REDIS_CROSS_DB = 41


class CloudConfig(Config):
    ENV = "云服务器"
    ZONE = 998
    DB_KEY = "cloud"
    REDIS_KEY = "cloud"
    DB = f"game_doom_server_1001"
    REDIS_API_DB = 0
    REDIS_CROSS_DB = 1


class MyLocalConfig(Config):
    ENV = "local 冬瓜"
    ZONE = 992
    DB_KEY = "local"
    REDIS_KEY = "local"
    DB = "game_doom_server_1001"
    REDIS_API_DB = 42
    REDIS_CROSS_DB = 41


envs = {
    "local": LocalConfig,
    "testing": TestConfig,
    "default": TestConfig,
    "stage": StageConfig,
    "cloud": CloudConfig,
    "mine": MyLocalConfig
}


def get_config():
    return envs.get(os.environ.get(ENV_NAME, "stage"))

