from utils.log import db_log


class AllManager:
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.config = conn.getALL('select * from basic_all')
        self.values = {}
        for con in self.config:
            self.values[con.get('aid')] = AllEntity(con)
        db_log.info('加载配置成功: basic_all')

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(AllManager, "_instance"):
            AllManager._instance = object.__new__(cls)
        return AllManager._instance

    def getConfigByAid(self, aid):
        return self.values.get(aid)


class AllEntity:
    """basic_all"""

    def __init__(self, value):
        self.aid = value.get('aid')
        self.note = value.get('note')
        self.type = value.get('type')
        self.basic = value.get('basic')
