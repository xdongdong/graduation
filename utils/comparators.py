import copy
import json
import re
from typing import Text, Any, Union
import allure
from tables.award import AwardEntity
from testcase.constant import Constant
from utils.log import api_log
from utils.util import parse_changing


class Comparators:

    @staticmethod
    def equal(check_value: Any, expect_value: Any, msg: Text = ""):
        api_log.info(f"断言相等: {check_value} == {expect_value}, {msg}")
        with allure.step(f"断言相等: {check_value} == {expect_value} msg={msg}"):
            assert check_value == expect_value, msg

    @staticmethod
    def success_code(check_response: dict, msg: Text = ""):
        api_log.info(f"断言code码为成功响应的code码: {check_response} ")
        with allure.step(f"断言接口返回的code码为成功响应的code码: {check_response} msg={msg}"):
            assert check_response.get("code") == "1", msg

    @staticmethod
    def error_code(check_response: dict, expect_code: str = "", msg: Text = ""):
        api_log.info(f"断言code码为请求失败的code码: {check_response} , expect_code={expect_code}")
        # 如果没有期望的code码 则默认断言code不为1
        check_code = check_response.get("code")
        with allure.step(f"断言接口返回的code码为失败响应的code码: {check_response} expect_code={expect_code} msg={msg}"):
            if not expect_code:
                assert check_code != "1" and check_code != "0", msg
            else:
                assert check_code == expect_code, msg

    @staticmethod
    def greater_than(
            check_value: Union[int, float], expect_value: Union[int, float], message: Text = ""
    ):
        api_log.info(f"断言更大值: {check_value} > {expect_value}, {message}")
        assert check_value > expect_value, message

    @staticmethod
    def equal_bool(
            check_value: Union[int, float], expect_value: Union[int, float], message: Text = ""
    ):
        api_log.info(f"断言bool值: {check_value}  期望值: {expect_value}, {message}")
        if expect_value:
            assert check_value, message
        else:
            assert not check_value, message

    @staticmethod
    def less_than(
            check_value: Union[int, float], expect_value: Union[int, float], message: Text = ""
    ):
        api_log.info(f"断言更小值: {check_value} < {expect_value}, {message}")
        assert check_value < expect_value, message

    @staticmethod
    def greater_or_equals(
            check_value: Union[int, float], expect_value: Union[int, float], message: Text = ""
    ):
        api_log.info(f"断言大于等于: {check_value} >= {expect_value}, {message}")
        assert check_value >= expect_value, message

    @staticmethod
    def less_or_equals(
            check_value: Union[int, float], expect_value: Union[int, float], message: Text = ""
    ):
        api_log.info(f"断言小于等于: {check_value} <= {expect_value}, {message}")
        assert check_value <= expect_value, message

    @staticmethod
    def not_equal(check_value: Any, expect_value: Any, message: Text = ""):
        api_log.info(f"断言不相等: {check_value} != {expect_value}, {message}")
        assert check_value != expect_value, message

    @staticmethod
    def string_equals(check_value: Text, expect_value: Any, message: Text = ""):
        api_log.info(f"断言字符类型相等: {check_value} == {expect_value}, {message}")
        assert str(check_value) == str(expect_value), message

    @staticmethod
    def length_equal(check_value: Text, expect_value: int, message: Text = ""):
        api_log.info(f"断言长度相等: {check_value}  期望长度值 {expect_value}, {message}")
        assert isinstance(expect_value, int), "expect_value should be int type"
        assert len(check_value) == expect_value, message

    @staticmethod
    def length_greater_than(
            check_value: Text, expect_value: Union[int, float], message: Text = ""
    ):
        api_log.info(f"断言长度大于某个值: len -> {check_value}    > {expect_value}, {message}")
        assert isinstance(
            expect_value, (int, float)
        ), "expect_value should be int/float type"
        assert len(check_value) > expect_value, message

    @staticmethod
    def length_greater_or_equals(
            check_value: Text, expect_value: Union[int, float], message: Text = ""
    ):
        assert isinstance(
            expect_value, (int, float)
        ), "expect_value should be int/float type"
        assert len(check_value) >= expect_value, message

    @staticmethod
    def length_less_than(
            check_value: Text, expect_value: Union[int, float], message: Text = ""
    ):
        assert isinstance(
            expect_value, (int, float)
        ), "expect_value should be int/float type"
        assert len(check_value) < expect_value, message

    @staticmethod
    def length_less_or_equals(
            check_value: Text, expect_value: Union[int, float], message: Text = ""
    ):
        assert isinstance(
            expect_value, (int, float)
        ), "expect_value should be int/float type"
        assert len(check_value) <= expect_value, message

    @staticmethod
    def contains(check_value: Any, expect_value: Any, message: Text = ""):
        assert isinstance(
            check_value, (list, tuple, dict, str, bytes)
        ), "expect_value should be list/tuple/dict/str/bytes type"
        assert expect_value in check_value, message

    @staticmethod
    def contained_by(check_value: Any, expect_value: Any, message: Text = ""):
        assert isinstance(
            expect_value, (list, tuple, dict, str, bytes)
        ), "expect_value should be list/tuple/dict/str/bytes type"
        assert check_value in expect_value, message

    @staticmethod
    def type_match(check_value: Any, expect_value: Any, message: Text = ""):
        def get_type(name):
            if isinstance(name, type):
                return name
            elif isinstance(name, str):
                try:
                    return __builtins__[name]
                except KeyError:
                    raise ValueError(name)
            else:
                raise ValueError(name)

        if expect_value in ["None", "NoneType", None]:
            assert check_value is None, message
        else:
            assert type(check_value) == get_type(expect_value), message

    @staticmethod
    def regex_match(check_value: Text, expect_value: Any, message: Text = ""):
        assert isinstance(expect_value, str), "expect_value should be Text type"
        assert isinstance(check_value, str), "check_value should be Text type"
        assert re.match(expect_value, check_value), message

    @staticmethod
    def startswith(check_value: Any, expect_value: Any, message: Text = ""):
        assert str(check_value).startswith(str(expect_value)), message

    @staticmethod
    def int_range(check_value: int, expect_value: int, expect_range: int, message: Text = ""):
        less = expect_value - expect_range
        more = expect_value + expect_range
        if not less <= check_value <= more:
            raise AssertionError(f"范围断言失败: 检查值: {check_value} \n"
                                 f"期望值: {expect_value} \n"
                                 f"期望值浮动范围: {expect_range} \n"
                                 f"message: {message}")

    @staticmethod
    def endswith(check_value: Text, expect_value: Any, message: Text = ""):
        assert str(check_value).endswith(str(expect_value)), message

    @staticmethod
    def changing(response: dict, before_prop: dict, changed_prop: dict, setting_drop: AwardEntity = None, blank=None):
        """
        检查changing 掉落
        :param blank: 不需要检查的物品
        :param response:
        :param before_prop:
        :param changed_prop:
        :param setting_drop:
        :return:
        """
        blank = blank if blank else []
        constant = Constant()
        # 将changed_prop拷贝备份
        if setting_drop is None:
            setting_drop = {}
        changing_prop = parse_changing(response)
        local_changed_prop = copy.deepcopy(changed_prop)

        if setting_drop and not changing_prop:
            raise AssertionError(f"配置表有掉落, 但实际无掉落: \n"
                                 f"setting_drop: {setting_drop} \n"
                                 f"response: {response}")
        # 没有物品变化时,直接断言local_changed_prop == before_prop
        for prop in changing_prop:
            droped = False
            pid = str(prop.get("pid"))
            num = prop.get("num")
            if pid in blank:
                continue

            if pid not in before_prop.keys() and pid not in changed_prop.keys():
                raise AssertionError(f"pid: {pid} not in \n "
                                     f"before_prop: {before_prop} \n "
                                     f"or changed_prop: {changed_prop}")
            changed_num = 0 if pid not in changed_prop.keys() else changed_prop.get(pid)
            before_num = 0 if pid not in before_prop.keys() else before_prop.get(pid)
            assert changed_num - before_num == num, f"掉落物品数量不一致: pid: {pid}\n" \
                                                    f"changing_num: {num} \n" \
                                                    f"before_num: {before_num} \n" \
                                                    f"changed_num: {changed_num}"
            # 验证配置掉落与实际掉落
            if setting_drop and pid not in constant.not_check:
                fix_reward = setting_drop.reward or []
                random_reward = setting_drop.random or []
                fix_num = None
                random_num = None
                for fix in fix_reward:
                    if fix.get("pid") == pid:
                        fix_num = fix.get("num")
                for random in random_reward:
                    if random.get("pid") == pid:
                        random_num = random.get("num")
                # 如果固定和随机都不掉落该物品或掉落该物品但数量不对,则掉落错误
                if fix_num and fix_num == num:
                    droped = True
                if random_num and random_num == num:
                    droped = True
                # 校验是否与配置表内的物品掉落一致
                assert droped, f"掉落与配置表中的物品不一致: pid={pid}\n" \
                               f"fix: {setting_drop.reward} \n" \
                               f"random: {setting_drop.random} \n" \
                               f"changing: {changing_prop}"
            # 去掉已经校验的key
            try:
                local_changed_prop.pop(pid)
                before_prop.pop(pid)
            except KeyError:
                pass

        # 校验其他物品
        assert local_changed_prop == before_prop, "除返回的物品变化外有其他物品变化: \n" \
                                                  f"before_prop: {before_prop} \n" \
                                                  f"changed_prop: {local_changed_prop}"

    @staticmethod
    def mail_reward(mail, setting_drop: AwardEntity = None):
        if not mail:
            raise AssertionError(f"未找到邮件")
        reward = json.loads(mail.get("reward"))
        fix_reward = setting_drop.reward or []
        random_reward = setting_drop.random or []
        for pid, num in reward.items():
            drop = False
            fix_num = None
            random_num = None
            for fix in fix_reward:
                if fix.get("pid") == pid:
                    fix_num = fix.get("num")
            for random in random_reward:
                if random.get("pid") == pid:
                    random_num = random.get("num")
            # 如果固定和随机都不掉落该物品或掉落该物品但数量不对,则掉落错误
            if fix_num and fix_num == num:
                drop = True
            if random_num and random_num == num:
                drop = True
            # 校验是否与配置表内的物品掉落一致
            assert drop, f"掉落与配置表中的物品不一致: pid={pid}\n" \
                         f"fix: {setting_drop.reward} \n" \
                         f"random: {setting_drop.random} \n" \
                         f"mail: {mail}"
