from copy import copy
from random import choice

from apis.base_api import BaseApi
from apis.camp_api import CampApi
from utils.param_encrypt import decrypt
from utils.time_utlis import TimeUtils
from utils.util import parse_json


class TeamApi(BaseApi):
    __create_team = "/game/team/createteam"
    __join_team = "/game/team/jointeam"

    def create_team(self, name, language=9531001, surface="9522001|#ffffff", grain="9523001|#000000",
                    banner="9521001|#ffffff", affiche='', recruitway=0):
        """
        创建战队
        :param banner: 战队旗帜id
        :param name: 小队名称
        :param language: 语言
        :param surface: 战队底纹id
        :param grain: 战队旗徽id
        :param affiche: 小队公告
        :param recruitway: 招募方式 0 公开招募 1 审核加入
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "name": name,
            "language": language,
            "surface": surface,
            "grain": grain,
            "affiche": affiche,
            "recruitway": recruitway,
            "banner": banner
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__create_team, data=data)
        return re

    def join_team(self, team_id):
        """
        加入小队
        :param team_id: 要加入的小队id
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "teamid": team_id,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__join_team, data=data)
        return re

    def leave_team(self):
        """
        离开战队， 删除数据库内的数据
        :return:
        """
        user_team = self.db_utils.sql_conn.getRow(f"select * from {self.setting.DB}.user_team where roleid = {self.role_id}")
        team_id = user_team.get("teamid")
        team_info = self.db_utils.sql_conn.getRow(f"select * from {self.setting.DB}.team_info where teamid = {team_id}")
        team_leader = team_info.get("roleid")
        now_people = team_info.get("nowpeople")
        # 当前战队有且仅有自己时
        if team_leader == self.role_id and now_people == 1:
            # 删除战队
            self.db_utils.sql_conn.delete(f"delete from {self.setting.DB}.team_info where teamid = {team_id}")
        # 当自己为队长且不止一个人时
        elif team_leader == self.role_id:
            new_leader = self.db_utils.sql_conn.getRow(
                f"select * from {self.setting.DB}.user_team where teamid = {team_id} and roleid != {self.role_id}")
            # 将队长修改为另外的队员
            self.db_utils.sql_conn.update(
                f"update {self.setting.DB}.team_info set roleid = {new_leader.get('roleid')} where teamid={team_id}")

        # 删除自己在战队的信息
        self.db_utils.sql_conn.delete(f"delete from {self.setting.DB}.user_team where roleid = {self.role_id}")
        # 将user_info里的战队改为0
        self.db_utils.sql_conn.update(f"update {self.setting.DB}.user_info set team = 0 where roleid= {self.role_id}")

    def user_team(self, new=None, camp=None):
        """
        检查是否有战队 如果没有则创建战队  如果传了New 则必定新建小队
        :param camp: 指定哪个阵营创建小队
        :param new: 是否新建小队
        :return:
        """
        time_utils = TimeUtils()
        server_user = self.db_utils.sql_conn.getRow(f"select * from {self.setting.DB}.user_info where roleid = {self.role_id}")
        camps = self.db_utils.sql_conn.getLine(f"select id from basic_camp")

        team = server_user.get("team")
        server_camp = server_user.get("camp")

        join_camp = camp if camp else choice(camps)
        if not server_camp or server_camp != join_camp:
            self.db_utils.sql_conn.update(f"update {self.setting.DB}.user_info set camp = {join_camp} where roleid = {self.role_id}")
        if (not team) or new:
            re = self.create_team("test" + str(time_utils.get_current_timestamp())[-4:])
            response = decrypt(re.text)
            changing = parse_json("changing", response)
            new_team = parse_json("team", changing)
            return new_team
        elif team and not new:
            return team
        else:
            self.leave_team()
            re = self.create_team("test" + str(time_utils.get_current_timestamp())[-4:])
            response = decrypt(re.text)
            changing = parse_json("changing", response)
            new_team = parse_json("team", changing)
            return new_team
