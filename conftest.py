import inspect
import os
import pytest
import requests
from settings import ENV_NAME, get_config


def pytest_addoption(parser):
    """
    添加命令行参数
    :param parser:
    :return:
    """
    parser.addoption(
        "--save",
        action="store",
        type=bool,
        default=False,
        help="是否将收集到的用例保存到对应平台上",
    )
    parser.addoption(
        "--env",
        action="store",
        type=str,
        default="testing",
        help="执行环境",
    )


@pytest.fixture(scope='session')
def get_env(request):
    try:
        return request.config.getoption("--env")
    except ValueError as e:
        return "default"


@pytest.fixture(scope='session', autouse=True)
def set_env(get_env):
    """将自定义参数的值写入环境变量"""
    os.environ[ENV_NAME] = get_env


def pytest_collection_modifyitems(config, items):
    """
    测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上
    :return:
    """
    is_save = config.getoption("--save")
    result = []
    setting = get_config()

    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")
        # 节点
        name_split = item.nodeid.split("[")
        node = name_split[0]
        # 备注
        func = getattr(item.cls, item.originalname) if item.cls else None
        if func and func.__doc__:
            remark = func.__doc__.split(":param")[0].split(":return")[0].strip().replace("\n", " ")
        elif len(name_split) > 1:
            remark = name_split[-1].replace("]", " ")
        else:
            remark = node

        module = f"{item.cls}[{inspect.getdoc(item.cls)}]" if inspect.getdoc(item.cls) else str(item.cls)
        # api用例还是app用例 添加标记
        if 'api' in module:
            item.add_marker(pytest.mark.api)
            tp = "api"
        elif 'app' in module:
            item.add_marker(pytest.mark.app)
            tp = "app"
        else:
            tp = "None"
        result.append({
            "node_id": node,
            "remark": remark,
            "module": module,
            "type": tp
        })
    for r in result:
        print(r)
    if is_save:
        post = {
            "data": result
        }
        requests.post(setting.TEST_UPLOAD_ADDR, json=post)




