import math

from settings import get_config
from utils.GetHashCode import GetHashCode
from utils.comparators import Comparators
from utils.log import api_log
from utils.mysql import Mysql
from utils.util import merge_dict, parse_setting


class HeroAttr:

    def __init__(self, role_id: dict, sql_conn: Mysql, mode="basic"):
        self.compare = Comparators()
        self.__conn: Mysql = sql_conn
        # 属性配置
        self.__attr_config = {}
        attr_config = self.__conn.getALL('select * from game_doom_sys.basic_attribute')
        self.__attr_name_config = {}
        for attr in attr_config:
            self.__attr_config[str(attr.get("attrid"))] = attr
            self.__attr_name_config[attr.get("attr")] = attr
        self.role_id = role_id
        self.db = get_config().DB
        tables = {
            # 普通的计算
            "basic": {
                "hero": f"{self.db}.user_hero_{GetHashCode.getHashCode(self.role_id)}",
                "exclusive": f"{self.db}.user_hero_exclusive",
                "resonance": f"{self.db}.user_hero_resonance"
            },
            # 佣兵副本
            "mer": {
                "hero": f"{self.db}.user_mercenary_hero",
                "exclusive": f"{self.db}.user_mercenary_hero_exclusive"
            }
        }
        self.tables = tables.get(mode)
        self.mode = mode

    def get_hero_attr(self, hid):
        hero_attr, count_attr = self.__get_hero_attr(hid)
        return count_attr

    def assert_hero_attr(self, hid):
        hero_attr, count_attr = self.__get_hero_attr(hid)
        for attr, value in hero_attr.items():
            self.compare.int_range(
                value,
                count_attr.get(attr),
                expect_range=1,
                message=f"hero: {hid} check_attr: {attr}"
            )

    def get_hero_ce(self, hid):
        hero_ce = self.__get_hero_ce(hid)
        hero_attr, count_attr = self.__get_hero_attr(hid)
        count_ce = 0
        for attr, value in hero_attr.items():
            attr_config = self.__attr_name_config.get(attr)
            power = attr_config.get("power")
            count_ce += value * power
        # 战力值需要除以25
        count_ce = count_ce / 25
        return hero_ce, int(count_ce)

    def __get_hero_ce(self, hid):
        conn = self.__conn
        try:
            hero = conn.getRow(f"select * from {self.tables.get('hero')} where hid = {hid}")
        except Exception as e:
            raise ValueError(f"找不到该英雄: user_mercenary_hero, {hid}")
        return hero.get("ce")

    # 专属属性
    def __get_exclusive_attr(self, hero, base_attr, per_attr):
        hid = hero.get("hid")
        # 专属
        user_exclusive = self.__conn.getRow(f'select * from {self.tables.get("exclusive")} where hid = {hid}')
        hero_exclusive_attr = self.__conn.getOne(
            f"select attr from basic_hero_exclusive where exclusiveid = "
            f"{user_exclusive.get('pid')} and `level` = {user_exclusive.get('level')}")
        hero_exclusive_attr = parse_setting(hero_exclusive_attr)

        hero_base_attr, per_attr = self.__add_attr(base_attr, per_attr, hero_exclusive_attr)
        return hero_base_attr, per_attr

    # 等级和星级的属性
    def __get_level_start_attr(self, hero):
        per_attr = {}
        # 获取等级的基础属性
        hero_base_attr = self.__conn.getOne(f"select attr from basic_hero_lvlup where heroid = "
                                            f"{hero.get('heroid')} and level = {hero.get('level')} and state = {hero.get('state')}")
        hero_base_attr = parse_setting(hero_base_attr)
        # 星级
        hero_star_config = self.__conn.getRow(f"select attr, attrper from basic_hero_star where heroid = "
                                              f"{hero.get('heroid')} and state = {hero.get('grade')} and star = {hero.get('star')}")
        # 星级固定属性
        hero_star_attr = parse_setting(hero_star_config.get("attr"))
        # 星级百分比属性
        hero_star_attr_per = parse_setting(hero_star_config.get("attrper"))
        hero_base_attr, per_attr = self.__add_attr(hero_base_attr, per_attr, hero_star_attr)
        hero_base_attr, per_attr = self.__add_attr(hero_base_attr, per_attr, hero_star_attr_per)
        return hero_base_attr, per_attr

    # 佣兵副本共鸣属性
    def __get_mer_resonance_attr(self, hero, hero_base_attr, per_attr, resonance_config):
        user_resonance = hero.get("reslevel")
        if user_resonance:
            resonance_attr = {}
            # 六个位置是否有英雄， 是否到达红色激活加成
            for i in range(1, user_resonance + 1):
                resonance_link_setting = resonance_config.get(i)
                # 判断品阶和星级是否满足，满足则添加属性
                if resonance_link_setting.get("attr"):
                    resonance_attr = merge_dict(resonance_attr,
                                                parse_setting(resonance_link_setting.get("attr")))
            hero_base_attr, per_attr = self.__add_attr(hero_base_attr, per_attr, resonance_attr)
        return hero_base_attr, per_attr

    # 共鸣属性
    def __get_base_resonance_attr(self, hero, hero_base_attr, per_attr, resonance_config):
        user_resonance = self.__conn.getRow(
            f'select * from {self.tables.get("resonance")} where hid = {hero.get("hid")}')
        if user_resonance:
            resonance_attr = {}
            # 六个位置是否有英雄， 是否到达红色激活加成
            for i in range(1, 7):
                resonance_hid = user_resonance.get(f"position{i}")
                if not resonance_hid:
                    continue
                resonance_hero = self.__conn.getRow(
                    f"select * from {self.tables.get('hero')} where hid = {resonance_hid}")
                resonance_link_setting = resonance_config.get(i)
                # 判断品阶和星级是否满足，满足则添加属性
                if (resonance_hero.get("grade") >= resonance_link_setting.get("linkquality") and
                        resonance_hero.get("star") >= resonance_link_setting.get("linkstar")):
                    if resonance_link_setting.get("attr"):
                        resonance_attr = merge_dict(resonance_attr,
                                                    parse_setting(resonance_link_setting.get("attr")))
            hero_base_attr, per_attr = self.__add_attr(hero_base_attr, per_attr, resonance_attr)
        return hero_base_attr, per_attr

    # 军衔属性
    def __get_vip_buff_attr(self, hero_base_attr, per_attr):
        # 军衔加成
        user_vip = self.__conn.getOne(f"select camprank from {self.db}.user_info where roleid = {self.role_id}")
        vip_buff = self.__conn.getOne(f"select `show` from game_doom_sys.basic_camp_rank where id = {user_vip}")
        vip_buff = parse_setting(vip_buff)
        # 固定增加百分比属性， 写死
        buff_value = vip_buff.get("1007")
        if buff_value:
            hero_base_attr, per_attr = self.__add_attr(hero_base_attr, per_attr, {"1030001": vip_buff.get("1007")})
        return hero_base_attr, per_attr

    # 返回实际获取的属性值和计算的属性值
    def __get_hero_attr(self, hid):
        conn = self.__conn
        try:
            hero = conn.getRow(f"select * from {self.tables.get('hero')} where hid = {hid}")
        except Exception as e:
            raise ValueError(f"找不到该英雄: {hid}")

        # 等级和星级
        hero_base_attr, per_attr = self.__get_level_start_attr(hero)

        # 专属
        hero_base_attr, per_attr = self.__get_exclusive_attr(hero, hero_base_attr, per_attr)

        # 共鸣
        resonance_list = conn.getALL(
            f'select * from game_doom_sys.basic_hero_resonance where heroid = {hero.get("heroid")}')
        resonance_config = {}
        for resonance in resonance_list:
            resonance_config[resonance.get("linknum")] = resonance
        if not resonance_list:
            api_log.error(f"没有该英雄的共鸣配置: {hero.get('heroid')}")

        if self.mode == "basic":
            hero_base_attr, per_attr = self.__get_base_resonance_attr(hero, hero_base_attr, per_attr, resonance_config)
            hero_base_attr, per_attr = self.__get_vip_buff_attr(hero_base_attr, per_attr)
        elif self.mode == "mer":
            hero_base_attr, per_attr = self.__get_mer_resonance_attr(hero, hero_base_attr, per_attr, resonance_config)

        # 计算百分比加成
        for attr, value in per_attr.items():
            attr_config = self.__attr_config.get(attr)
            to_attr = str(attr_config.get("toattr"))
            hero_base_attr[to_attr] = math.ceil(hero_base_attr[to_attr] * (1 + value / 10000))
            hero_base_attr[attr] = value
        # 将id转为名字存入最终属性
        final_attr = self.__attr_to_name(hero_base_attr)

        return eval(hero.get("attr")), final_attr

    def __add_attr(self, base_attr, per_attr, up_attr):
        for key, value in up_attr.items():
            attr_config = self.__attr_config.get(key)
            to_attr = str(attr_config.get("toattr"))
            if to_attr != "-1":
                per_attr = merge_dict(per_attr, {key: value})
            else:
                base_attr = merge_dict(base_attr, {key: value})
        return base_attr, per_attr

    def __attr_to_name(self, attrs):
        name_attr = {}
        for key, value in attrs.items():
            attr_config = self.__attr_config.get(key)
            name = attr_config.get("attr")
            name_attr[name] = value
        return name_attr
