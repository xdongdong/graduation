import datetime
from copy import copy

import allure

from apis.base_api import BaseApi
from testcase.data.mercenary_data import MercenaryData

"""
佣兵副本api类
"""


class MercenaryApi(BaseApi):
    # 拉取信息
    _get_info_path = "/game/mercenary/info"
    # 英雄相关
    _hero_info_path = "/game/mercenary/heros"
    _add_hero_path = "/game/mercenary/addhero"
    _del_hero_path = "/game/mercenary/delhero"
    _hero_level_path = "/game/mercenary/levelup"
    _hero_level_all_path = "/game/mercenary/levelupbreak"
    _exclusive_up_path = "/game/mercenary/exclusiveup"
    _exclusive_up_all_path = "/game/mercenary/exclusiveupbreak"
    _hero_star_path = "/game/mercenary/promotestar"
    _hero_star_all_path = "/game/mercenary/promotestarbreak"
    _hero_resonance_path = "/game/mercenary/reslevelup"
    _hero_resonance_all_path = "/game/mercenary/reslevelupbreak"
    # 关卡相关
    _choose_level_path = "/game/mercenary/chooselevel"
    _challenge_path = "/game/mercenary/challenge"
    _settle_path = "/game/mercenary/settle"
    _clean_path = "/game/mercenary/clean"
    _abandon_path = "/game/mercenary/abandontask"
    # 奖励相关
    _get_reward_path = "/game/mercenary/countreward"
    _reward_info_path = "/game/mercenary/countrewardinfo"

    def choose_level(self, mission_id, **kwargs):
        """
        选择难度
        :param mission_id:任务id
        :param kwargs: multiple-翻倍倍数 最高难度关卡可用
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "missionid": mission_id,
            **kwargs
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self._choose_level_path, data=data)
        return re

    def abandon_task(self, mission_id):
        """
        放弃难度
        :param mission_id: 任务id
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "missionid": mission_id,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self._abandon_path, data=data)
        return re

    def challenge(self, mission_id, random_id):
        """
        挑战关卡
        :param mission_id: 难度id
        :param random_id:  关卡随机id
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "missionid": mission_id,
            "id": random_id
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self._challenge_path, data=data)
        return re

    def settle(self, mission_id, random_id, complete_status, verify, attrs=None):
        """
        关卡结算
        :param attrs: 雇主或物资剩余属性
        :param mission_id: 难度id
        :param random_id: 关卡随机ID
        :param complete_status:  0：失败 1：成功
        :param verify: 校验码
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "missionid": mission_id,
            "id": random_id,
            "completestatus": complete_status,
            "verify": verify,
        }
        if attrs:
            encrypt_data["battle"] = attrs
        data["encryptdata"] = encrypt_data
        re = self.http_post(self._settle_path, data=data)
        return re

    def clean(self, mission_id, num):
        """
        关卡扫荡
        :param mission_id: 难度id
        :param num: 扫荡次数
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "missionid": mission_id,
            "num": num,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self._clean_path, data=data)
        return re

    def get_reward(self, mission_id, count):
        """
        领取次数奖励
        :param mission_id: 任务难度ID
        :param count: 领取的次数奖励
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "missionid": mission_id,
            "count": count,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self._get_reward_path, data=data)
        return re

    def get_reward_info(self, mission_id):
        """
        获取奖励详情
        :param mission_id: 难度id
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "missionid": mission_id,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self._reward_info_path, data=data)
        return re

    def add_hero(self, hids):
        """
        雇佣英雄
        :param hids:要雇佣的英雄ID列表
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hids": hids}
        re = self.http_post(self._add_hero_path, data=data)
        return re

    def get_info(self):
        """
        获取佣兵副本详情
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self._get_info_path, data=data)
        return re

    def get_hero_info(self):
        """
        获取佣兵副本英雄信息
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self._hero_info_path, data=data)
        return re

    def del_hero(self, hids):
        """
        取消雇佣
        :param hids: 要取消雇佣的英雄主键ID集合
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hids": hids}
        re = self.http_post(self._del_hero_path, data=data)
        return re

    def hero_level_up(self, hid):
        """
        英雄升级
        :param hid: 要升级的英雄
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hid": hid}
        re = self.http_post(self._hero_level_path, data=data)
        return re

    def hero_level_all(self, hid):
        """
        英雄一键升级
        :param hid: 要升级的英雄
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hid": hid}
        re = self.http_post(self._hero_level_all_path, data=data)
        return re

    def exclusive_level_up(self, upid):
        """
        专属装备升级
        :param upid: 要升级的专属
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"upid": upid}
        re = self.http_post(self._exclusive_up_path, data=data)
        return re

    def exclusive_level_all(self, upid):
        """
        专属装备一键升级
        :param upid: 要升级的专属
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"upid": upid}
        re = self.http_post(self._exclusive_up_all_path, data=data)
        return re

    def hero_star(self, hid):
        """
        英雄升星
        :param hid: 要升星的英雄
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hid": hid}
        re = self.http_post(self._hero_star_path, data=data)
        return re

    def hero_star_all(self, hid):
        """
        英雄一键升星
        :param hid: 要升星的英雄
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hid": hid}
        re = self.http_post(self._hero_star_all_path, data=data)
        return re

    def hero_resonance(self, hid):
        """
        英雄共鸣
        :param hid: 要升级共鸣的英雄
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hid": hid}
        re = self.http_post(self._hero_resonance_path, data=data)
        return re

    def hero_resonance_all(self, hid):
        """
        英雄一键共鸣
        :param hid: 要升级共鸣的英雄
        :return:
        """
        data = copy(self.user_info)
        data["encryptdata"] = {"hid": hid}
        re = self.http_post(self._hero_resonance_all_path, data=data)
        return re

    def unlock_mercenary(self):
        """
        解锁玩家佣兵副本功能
        :return:
        """
        # 获取解锁关卡
        data = MercenaryData()
        with allure.step(f"解锁玩家佣兵副本功能"):
            unlock_stage = self.db_utils.sql_conn.getOne(
                f"select value from game_doom_sys.basic_global where id = {data.unlock_global_id}")
            self.db_utils.insert_user_stage(self.role_id, unlock_stage)

    def clear_user_mercenary(self):
        """
        清除玩家佣兵副本记录
        :return:
        """
        data = MercenaryData()
        with allure.step(f"清除玩家的佣兵副本记录"):
            # 清除数据库数据
            for table in data.tables:
                if table == "user_mercenary_maxtaskplot_record":
                    self.db_utils.sql_conn.delete(f"delete from {self.setting.DB}.{table}")
                else:
                    self.db_utils.sql_conn.delete(
                        f"delete from {self.setting.DB}.{table} where roleid = {self.role_id}")

            # 清除缓存数据
            today = datetime.date.today()
            for mission in data.missions:
                count_key = f"{data.win_count_key}:{mission}:{today}"
                reward_key = f"{data.win_reward_key}:{mission}:{today}"
                if self.db_utils.api_redis.conn.exists(count_key):
                    self.db_utils.api_redis.conn.zrem(count_key, f'"{self.role_id}"')
                if self.db_utils.api_redis.conn.exists(reward_key):
                    self.db_utils.api_redis.conn.hdel(reward_key, f'"{self.role_id}"')

    def unlock_difficult(self, missions):
        """
        解锁佣兵副本对应难度
        :param missions: 要解锁的任务
        :return:
        """
        with allure.step(f"解锁佣兵副本难度: {missions}"):
            for mission in missions:
                self.db_utils.sql_conn.insertOne(f"insert ignore into {self.setting.DB}.user_mercenary_task values("
                                                 f"{self.role_id}, {mission}, 1, 3, 1, NULL ,1, 0)")
