from testcase.constant import Constant


class MercenaryData(Constant):
    unlock_global_id = 400101
    lower_ticket = 2010101
    high_ticket = 2010102
    cap = 2010103
    # 任务id
    missions = [1000, 2000, 3000, 4000]

    # 缓存key
    win_count_key = "mercenary_plot_win_count"
    win_reward_key = "mercenary_plot_win_getreward"
    # 数据表
    tables = [
        "user_mercenary_battle",
        "user_mercenary_hero",
        "user_mercenary_hero_exclusive",
        "user_mercenary_hero_random",
        "user_mercenary_hide_reward",
        "user_mercenary_maxtaskplot_record",
        "user_mercenary_plot",
        "user_mercenary_strength_unlock",
        "user_mercenary_task"
    ]
