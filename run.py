import pytest

if __name__ == '__main__':
    # --reruns 重试次数  -n 多进程 -m 指定mark --reruns-delay 延迟多少秒之后重试

    # # 并发执行时
    addons = ['-sv', "--clean-alluredir", "-n", "auto", "--collect-only", "--env", "mine"]

    # addons = ['-sv', "--clean-alluredir"]
    # if setting.GENERATE_REPORT:
    #     addons.extend(['--alluredir', setting.REPORT_TMP_DIR, '--reruns', '1', '--reruns-delay', '5'])
    pytest.main(addons)

    # 生成报告
    # if setting.GENERATE_REPORT:
    #     os.system('allure generate ' + setting.REPORT_TMP_DIR + ' -o ' + setting.REPORT_DIR + ' --clean')
