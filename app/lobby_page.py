import re
import allure
from app.base_page import BasePage
from utils.db_utils import DbUtils
from utils.log import ui_log


class LobbyPage(BasePage):
    _FRIEND_BUTTON = {"name": "friendnode"}
    _TASK_BUTTON = {"name": "tasknode"}
    _BAG_BUTTON = {"name": "bagnode"}
    _LEADER_BUTTON = {"name": "leadernode"}
    _HERO_BUTTON = {"name": "heronode"}
    _FIGHT_BUTTON = {"name": "fightnode"}
    _CHAT_BUTTON = {"name": "chatbtn"}

    _LINE_NODES = {"name": "listnode", "offspring": {"name": "linenode"}}

    _COIN_NUM = {"name": "item1", "offspring": {"name": "text"}}
    _GEM_NUM = {"name": "item2", "offspring": {"name": "text"}}
    _RED_GEM_NUM = {"name": "item3", "offspring": {"name": "text"}}

    def goto_hero_page(self):
        with allure.step(f"切换至英雄界面"):
            self.find_and_click(self._HERO_BUTTON)

            from app.hero_page import HeroPage
            return HeroPage(self.driver, self.poco)

    def goto_chat_page(self):
        with allure.step(f"切换至聊天界面"):
            self.find_and_click(self._CHAT_BUTTON)
            from app.chat_page import ChatPage
            return ChatPage(self.driver, self.poco)

    def get_user_role(self):
        with allure.step(f"获取用户的name和role_id"):
            # 获取玩家用户名对应的id
            username = self.find(self._USERNAME).get_text()
            # username为未替换括号前的Username 游戏内均要这样展示
            return_name = username
            name_pattern = "\([0-9a-fA-F]{3}\)"
            matched = re.findall(name_pattern, username)
            for match in matched:
                username = username.replace(match, '')
            # 用替换掉括号内容后的username去查找role_id
            db_utils = DbUtils()
            role = db_utils.get_role_by_name(username)
            ui_log.info(f"获取到玩家名字: {return_name}, role_id: {role}")
            return return_name, role

    def get_user_resource(self):
        with allure.step(f"获取用户大厅界面的三个货币数量"):
            coin = self.find(self._COIN_NUM).get_text().replace(",", '')
            gem = self.find(self._GEM_NUM).get_text().replace(",", '')
            red_gem = self.find(self._RED_GEM_NUM).get_text().replace(",", '')

            return {
                "coin": coin,
                "gem": gem,
                "red_gem": red_gem
            }
