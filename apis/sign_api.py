from copy import copy

import allure

from apis.base_api import BaseApi


class SignApi(BaseApi):
    __today_sign = "/game/sign/newsign"
    __sign_reward = "/game/sign/upAwartStatus"

    def sign(self, is_status=1):
        """
        签到/拉取签到信息
        :param is_status: 自动签到/不自动签到状态
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "isStatus": is_status,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__today_sign, data=data)
        return re

    def total_sign_reward(self, day):
        """
        领取累计签到的天数奖励
        :param day: 天数
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "day": day,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__sign_reward, data=data)
        return re

    def clear_user_sign(self):
        with allure.step(f"清除玩家签到数据"):
            self.db_utils.sql_conn.delete(f"delete from {self.setting.DB}.user_sign where roleid= {self.role_id}")
