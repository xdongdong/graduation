import allure
from apis.chat_api import ChatApi
from apis.item_api import ItemApi
from tables.award import AwardEntity
from tables.item import ItemManager
from tables.store_award import StoreAwardManager
from testcase.data.item_data import ItemData
from utils.comparators import Comparators
from utils.param_encrypt import decrypt
from utils.util import parse_setting, parse_json, generate_openid


class TestMercenary:

    def setup_class(self):
        # api类
        self.api = ItemApi()
        # 将openid进行随机,每次都是用不重复的openid
        openid = generate_openid()
        # 登录
        user_info, host = self.api.login(openid)
        # 用户信息
        self.role_id = user_info.get("roleid")

        # 数据类
        self.data = ItemData()
        # 初始化chat api
        self.chat_api = ChatApi(self.api.base_data)
        # 断言类
        self.compare = Comparators()
        # basic_item表
        self.item_manager = ItemManager(self.api.db_utils.sql_conn)
        self.choose_item = self.item_manager.get_item_by_type(self.data.choose_open_type)
        # store_award表
        self.store_manager = StoreAwardManager(self.api.db_utils.sql_conn)

    def test_choose_open(self):
        """
        测试开启选择类道具choose_item
        :return:
        """
        for item in self.choose_item:
            pid = item.aid
            use = int(item.useargs)
            item_cfg = self.store_manager.get_settings_by_award(use)
            num = len(item_cfg)

            # 获取物品
            with allure.step(f"获取测试物品: {pid}, {num}个"):
                self.chat_api.chat_talk(f"gm prop {pid} {num}")
            with allure.step("开启物品"):
                open_item = {}
                reward = []
                # 每个物品开一个
                for cfg in item_cfg:
                    goods_cfg = parse_setting(cfg.goods)
                    for i in list(goods_cfg.keys()):
                        open_item[i] = 1
                        reward.append({"pid": i, "num": goods_cfg.get(i)})
                before_prop = self.api.db_utils.get_user_props(self.role_id)
                # 配置物品
                reward.append({"pid": str(pid), "num": -num})
                setting_prop = AwardEntity(reward=reward)
                # 开启物品
                re = self.api.open_prop(pid, num, pid=open_item)
                response = decrypt(re.text)
                changed_prop = self.api.db_utils.get_user_props(self.role_id)
                self.compare.changing(response, before_prop, changed_prop, setting_prop)

    def test_choose_error(self):
        """
        传入错误的数量
        :return:
        """
        test_item = self.choose_item[0]
        pid = test_item.aid
        use = int(test_item.useargs)
        item_cfg = self.store_manager.get_settings_by_award(use)
        # 获取物品
        with allure.step(f"获取测试物品: {pid}, {1}个"):
            self.chat_api.chat_talk(f"gm prop {pid} {1}")

        goods_cfg = parse_setting(item_cfg[0].goods)

        # 开启物品
        with allure.step(f"传入数量: -1 进行开启"):
            open_item = {list(goods_cfg.keys())[0]: -1}
            re = self.api.open_prop(pid, -1, pid=open_item)
            response = decrypt(re.text)
            code = parse_json("code", response)

            # {'code': '-110014', 'msg': '物品数量错误'}
            self.compare.equal("-110014", code, "传入错误的参数进行兑换")

        # 开启物品
        with allure.step(f"拥有数量: 1, 传入数量: 2 进行开启"):
            open_item = {list(goods_cfg.keys())[0]: 2}
            re = self.api.open_prop(pid, 2, pid=open_item)
            response = decrypt(re.text)
            code = parse_json("code", response)

            # {'code': '-110001', 'msg': '材料不足'}
            self.compare.equal("-110001", code, "传入错误的参数进行兑换")

    def test_concurrent_open(self):
        test_item = self.choose_item[0]
        pid = test_item.aid
        use = int(test_item.useargs)
        item_cfg = self.store_manager.get_settings_by_award(use)
        num = 1
        # 获取物品
        with allure.step(f"获取测试物品: {pid}, {1}个"):
            self.chat_api.chat_talk(f"gm prop {pid} {1}")

        goods_cfg = parse_setting(item_cfg[0].goods)
        open_item = {list(goods_cfg.keys())[0]: num}
        reward = [{"pid": str(pid), "num": -1},
                  {"pid": list(goods_cfg.keys())[0], "num": goods_cfg.get(list(goods_cfg.keys())[0])}]
        setting_prop = AwardEntity(reward=reward)

        before_prop = self.api.db_utils.get_user_props(self.role_id)
        # times 并发2次  其余参数为并发方法的参数
        params = {"times": 2, "package_id": pid, "num": num, "pid": open_item}
        results = self.api.concurrent_func(self.api.open_prop, **params)
        changed_prop = self.api.db_utils.get_user_props(self.role_id)
        success = False
        for result in results:
            response = decrypt(result.text)
            if parse_json("code", response) == "1":
                self.compare.equal_bool(success, False, "首次成功")
                self.compare.changing(response, before_prop, changed_prop, setting_prop)
                success = True
