import os
import re
import time
from random import randint

from testcase.constant import Constant


def parse_setting(s: str) -> dict:
    """
    切割配置表中的| 和;  传入string类型
    :param s: 待切割的字符串
    :return:
    """
    if not isinstance(s, str) or not s:
        raise ValueError(f'必须传入字符串, value: {s}, type: {type(s)}')
    data = {}
    line_split = s.split('|')
    for s in line_split:
        semicolon_split = s.split(';')
        # 如果能转成Int就转成int
        try:
            val = int(semicolon_split[1])
        except Exception as e:
            val = semicolon_split[1]

        if semicolon_split[0] in data.keys():
            value = data.get(semicolon_split[0])
            data[semicolon_split[0]] = value + val
        else:
            data[semicolon_split[0]] = val
    return data


def parse_json(key, jsons):
    """
    通过参数key，在jsons中进行匹配并输出该key对应的value
    :param key: 需要查找的key
    :param jsons: 需要解析的json串
    :return: 返回找到的第一个值,如果有多个，返回列表
    """
    params_list = []

    def nomal_func(key, jsons):
        if isinstance(jsons, dict):
            if key in jsons.keys():
                key_value = jsons.get(key)
                params_list.append(key_value)
            else:
                for json_result in jsons.values():
                    nomal_func(key, json_result)
        elif isinstance(jsons, list):
            for json_array in jsons:
                nomal_func(key, json_array)
        else:
            return ''
        if params_list is []:
            pass
        else:
            return params_list

    params_list = nomal_func(key, jsons)
    for x in params_list:
        if x == '':
            params_list.remove(x)
    if len(params_list) == 1:
        return params_list[0]
    else:
        return {}


def merge_dict(dict1, dict2):
    """
    合并字典 有重复的key则相加
    :param dict1:
    :param dict2:
    :return:
    """
    if dict1 and not dict2:
        return dict1
    if not dict1 and dict2:
        return dict2
    if not dict1 and not dict2:
        return {}
    m_dict = {}
    for k, v in dict1.items():
        m_dict[k] = v
    for k, v in dict2.items():
        if k in m_dict.keys() and isinstance(v, int):
            val = m_dict.get(k)
            m_dict[k] = val + v
        else:
            m_dict[k] = v
    return m_dict


def parse_changing(response):
    constant = Constant()
    changing = parse_json("changing", response)
    # 如果没有changing
    if not changing:
        return []
    props = changing.get("prop") or []
    user_prop = changing.get("user") or {}

    for pid in user_prop.keys():
        num = user_prop.get(pid)
        if pid in constant.user_resource.keys():
            # 将其替换为对应的pid
            pid = constant.user_resource.get(pid)
        props.append({"pid": pid, "num": num})
    return props


def generate_openid():
    """
    构建不一样的openid
    :return:
    """
    openid = str(int(time.time() * 1000))[-6:] + str(randint(1, 100) + os.getpid())
    return openid


def get_set_nums(num, min_num=1, max_num=100):
    """
    获取随机不重复的数字num个
    :param num: 多少个
    :param min_num: 最小区间
    :param max_num: 最大区间
    :return:
    """
    random_num = []
    while True:
        n = randint(min_num, max_num)
        random_num.append(n)
        if len(set(random_num)) == num:
            break
    return random_num


def replace_color(text):
    """
    替换掉对应字符串中包含的color
    :return:
    """
    start_pattern = "<color=#[0-9a-fA-F]{6}>"
    end_pattern = "</color>"
    start_match = re.findall(start_pattern, text)
    end_match = re.findall(end_pattern, text)
    start_match.extend(end_match)
    for match in start_match:
        text = text.replace(match, '')
    return text.strip()
