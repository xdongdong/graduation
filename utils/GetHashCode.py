
class GetHashCode:
    @staticmethod
    def convert_n_bytes(n, b):
        bits = b * 8
        return abs((n + 2 ** (bits - 1)) % 2 ** bits - 2 ** (bits - 1))

    def convert_4_bytes(self, n):
        return self.convert_n_bytes(n, 4) % 10

    @classmethod
    def getHashCode(cls, s):
        s = str(s)
        h = 0
        n = len(s)
        for i, c in enumerate(s):
            h = h + ord(c) * 31 ** (n - 1 - i)
        return cls().convert_4_bytes(h)


if __name__ == '__main__':
    print(GetHashCode.getHashCode('100010106'))




