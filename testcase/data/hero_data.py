class HeroData:
    # 测试配置
    test_hero = 1002001
    test_quality = 3

    test_resonance_hero = 1001002
    test_resonance_hero_quality = 3

    test_exclusive_hero = 1001002

    test_equip = 2520135

    # 装备升级经验道具的ID 及一个道具提供的经验
    equip_exp_item = 2520001
    item_exp = 10

    # global表中的最大等级id
    global_level_max = 2010

    # 购买英雄栏位消耗id
    less_5_cost = 250003
    between_6_and_10_cost = 250004
    more_10_cost = 250005
    add_hero_num = 10

    # 英雄可以突破的等级
    hero_break = {"level": 10, "state": 0}

    # 升星消耗类型  1是种族 2是指定英雄
    star_cost_type = [1, 2]

    # app测试数据
    init_hero = "1002001"
    # 获取英雄的ID和品级
    get_hero = "1002005"
    get_hero_quality = "3"
    # 英雄升级消耗物品
    hero_level_cost1 = 2040101
    hero_level_cost2 = 2000001
