from abc import ABC

from tables.base import BaseManager
from tables.basic_global import GlobalManager
from testcase.data.hero_data import HeroData
from utils.log import db_log
from utils.util import merge_dict, parse_setting


class HeroManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "lev_config"):
            return
        self.conn = conn
        self.lev_config = None
        self.star_config = None
        self.hero_config = None
        self.exclusive_config = None
        self.resonance_config = None

        self.level_by_hero = {}
        self.star_by_hero = {}
        self.hero_info = {}
        self.exclusive_level = {}
        self.resonance_value = {}
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(HeroManager, "_instance"):
            HeroManager._instance = object.__new__(cls)
        return HeroManager._instance

    def load_config(self):
        self.lev_config = self.conn.getALL('select * from basic_hero_lvlup')
        self.star_config = self.conn.getALL('select * from basic_hero_star')
        self.hero_config = self.conn.getALL('select * from basic_hero_info')
        self.exclusive_config = self.conn.getALL("select * from basic_hero_exclusive")
        self.resonance_config = self.conn.getALL("select * from basic_hero_resonance")

        for lev in self.lev_config:
            hero_id = lev.get('heroid')
            if hero_id in self.level_by_hero.keys():
                value = self.level_by_hero.get(hero_id)
                value.append(HeroLevelEntity(lev))
            else:
                self.level_by_hero[hero_id] = [HeroLevelEntity(lev)]
        db_log.info("加载配置成功: basic_hero_lvlup")

        for hero in self.hero_config:
            self.hero_info[hero.get('heroid')] = HeroInfoEntity(hero)
        db_log.info("加载配置成功: basic_hero_info")

        for star in self.star_config:
            hero_id = star.get('heroid')
            if hero_id in self.star_by_hero.keys():
                value = self.star_by_hero.get(hero_id)
                value.append(HeroStarEntity(star))
            else:
                self.star_by_hero[hero_id] = [HeroStarEntity(star)]
        db_log.info("加载配置成功: basic_hero_star")

        for exclusive in self.exclusive_config:
            hero_id = exclusive.get('heroid')
            if hero_id in self.exclusive_level.keys():
                value = self.exclusive_level.get(hero_id)
                value.append(HeroExclusiveEntity(exclusive))
            else:
                self.exclusive_level[hero_id] = [HeroExclusiveEntity(exclusive)]
        db_log.info("加载配置成功: basic_hero_exclusive")

        for resonance in self.resonance_config:
            hero_id = resonance.get('heroid')
            if hero_id in self.resonance_value.keys():
                value = self.resonance_value.get(hero_id)
                value.append(HeroResonanceEntity(resonance))
            else:
                self.resonance_value[hero_id] = [HeroResonanceEntity(resonance)]
        db_log.info("加载配置成功: basic_hero_resonance")

    def get_hero_by_id(self, hero_id):
        """
        根据id获取hero_info的信息
        :param hero_id:
        :return:
        """
        result = self.hero_info.get(hero_id)
        if not result:
            raise AssertionError(f"basic_hero_info中没有该英雄: {hero_id}")
        return result

    def get_hero_by_faction(self, faction):
        """
        根据种族获得英雄列表
        :param faction:
        :return:
        """
        result = [hero for hero in self.hero_info.values() if hero.faction == faction]
        return result

    def get_level_by_hero(self, hero):
        """
        获取英雄升级配置
        :param hero: hero_id
        :return:
        """
        return self.level_by_hero.get(hero)

    def get_hero_max_level(self, hero):
        """
        获取英雄最大等级的配置
        :return:
        """
        hero = int(hero)
        global_manager = GlobalManager(self.conn)
        hero_data = HeroData()
        global_max = int(global_manager.get_value_by_id(hero_data.global_level_max))
        setting_max = self.get_level_by_hero(hero)[-1]
        # 取其更小值
        return setting_max if setting_max.level < global_max \
            else self.get_hero_level_setting(hero, global_max, int(str(global_max)[:-1]) - 1)

    def get_hero_level_cost(self, hero, start_level=-1, start_state=-1, level=-1, state=-1):
        """
        获取英雄升到N级所需消耗物品
        :param start_state: 起始state 不填为初始等级
        :param start_level: 起始等级 不填为初始等级
        :param state: state字段 不填为最大等级
        :param hero: hero_id
        :param level: 要升到的等级 不填为最大等级
        :return:
        """
        hero = int(hero)
        hero_setting = [s for s in self.get_level_by_hero(hero) if s.level >= start_level and s.state >= start_state]
        cost = {}
        for setting in hero_setting:
            if setting.level == level and setting.state == state:
                break
            if setting.levelcost:
                cost = merge_dict(cost, parse_setting(setting.levelcost))
            if setting.breakcost:
                cost = merge_dict(cost, parse_setting(setting.breakcost))
        return cost

    def get_hero_level_setting(self, hero, level, state):
        """
        获取英雄某一级的配置
        :param hero: hero_id
        :param level: 等级
        :param state: 状态
        :return:
        """
        hero = int(hero)
        data = None
        for value in self.level_by_hero.get(hero):
            if value.level == level and value.state == state:
                data = value
        if not data:
            raise AssertionError(f"未在basic_hero_lvlup表中找到该数据: hero={hero}, level={level}, state={state}")
        db_log.info(f"获取英雄等级配置:level={level}, state={state} data={data}")
        return data

    def get_next_level_setting(self, hero, level, state):
        """
        获取英雄下一级的配置
        :param hero: hero_id
        :param level: 等级
        :param state: 状态
        :return:
        """
        current = self.get_hero_level_setting(hero, level, state)
        hero_setting = self.get_level_by_hero(hero)
        index = hero_setting.index(current)
        try:
            result = hero_setting[index + 1]
        except IndexError:
            raise AssertionError(f"没有当前等级的下一级的配置: hero={hero}, level={level}, state={state}")
        else:
            return result

    def get_star_by_hero(self, hero):
        """
        获取一个英雄的所有升星配置
        :param hero:
        :return:
        """
        result = self.star_by_hero.get(hero)
        if not result:
            raise AssertionError(f"basic_hero_star中没有该英雄的数据: {hero}")
        return result

    def get_all_star_setting(self):
        """
        获取所有升星配置
        :return:
        """
        result = []
        for hero in self.star_config:
            result.append(HeroStarEntity(hero))
        return result

    def get_hero_star_setting(self, hero, state, star):
        """
        获取英雄某一星级的配置
        :param hero:
        :param state: 品级  紫橙红
        :param star: 星级  红1星 红2星
        :return:
        """
        data = None
        for value in self.star_by_hero.get(hero):
            if value.state == state and value.star == star:
                data = value
        if not data:
            raise AssertionError(f"未在basic_hero_star表中找到该数据: hero={hero}, state={state}, star={star}")
        return data

    def get_next_star_setting(self, hero, state, star):
        """
        获取英雄下一星级配置
        :param hero:
        :param state: 当前品级
        :param star: 当前星级
        :return:
        """
        cur = self.get_hero_star_setting(hero, state, star)
        hero_setting = self.get_star_by_hero(hero)
        index = hero_setting.index(cur)
        try:
            _next = hero_setting[index + 1]
        except IndexError:
            raise AssertionError(f"未在basic_hero_star表中找到下一星级数据: 当前hero={hero}, state={state}, star={star}")
        else:
            return _next

    def get_star_cost_two_setting(self, hero):
        """
        获取英雄升星需要消耗两个的配置
        :param hero:
        :return:
        """
        for setting in self.get_star_by_hero(hero):
            if setting.cost1 != "-1" and setting.cost2 != "-1":
                return setting
        raise AssertionError(f"该英雄未配置需要消耗两个材料升星的品级: {hero}")

    def get_exclusive_by_hero(self, hero):
        """
        通过英雄id获取专属配置
        :param hero:
        :return:
        """
        result = self.exclusive_level.get(hero)
        if not result:
            raise AssertionError(f"专属升级表中未配置该英雄的专属")
        return result

    def get_exclusive_level_setting(self, hero, level):
        """
        根据英雄id和专属等级获取配置
        :param hero:
        :param level: 专属等级
        :return:
        """
        results = self.get_exclusive_by_hero(hero)
        for result in results:
            if result.level == level:
                return result
        raise AssertionError(f"没有找到对应的专属等级配置: hero={hero}, level={level}")

    def get_next_exclusive_setting(self, hero, level):
        """
        获取传入等级的下一级专属升级配置
        :param hero:
        :param level:
        :return:
        """
        current = self.get_exclusive_level_setting(hero, level)
        exclusive_setting = self.get_exclusive_by_hero(hero)
        index = exclusive_setting.index(current)
        try:
            _next = exclusive_setting[index + 1]
        except IndexError:
            raise AssertionError(f"没有当前专属等级的下一级的配置: hero={hero}, level={level}")
        else:
            return _next

    def get_exclusive_level_cost(self, hero, start_level=-1, to_level=-1):
        """
        获取专属升到N级所需消耗物品
        :param start_level: 起始等级
        :param hero: hero_id
        :param to_level: 要升到的等级
        :return:
        """
        exclusive_setting = [s for s in self.get_exclusive_by_hero(hero) if s.level >= start_level]
        cost = {}
        for setting in exclusive_setting:
            if setting.level == to_level:
                break
            if setting.cost:
                cost = merge_dict(cost, parse_setting(setting.cost))
        return cost

    def get_exclusive_level_unlock(self, hero, start_level=-1, to_level=-1):
        """
        获取专属升到N级会解锁的皮肤和技能
        :param start_level: 起始等级
        :param hero: hero_id
        :param to_level: 要升到的等级
        :return:
        """
        exclusive_setting = [s for s in self.get_exclusive_by_hero(hero) if s.level >= start_level]
        unlock_skill = None
        unlock_skin = []
        for setting in exclusive_setting:
            if setting.unlockskin != -1:
                unlock_skin.append(setting.unlockskin)
            if setting.skill != -1:
                unlock_skill = setting.skill
            if setting.level == to_level:
                break
        return unlock_skill, unlock_skin

    def get_hero_resonance(self, hero):
        """
        获取英雄的共鸣配置
        :param hero: 英雄id
        :return:
        """
        value = self.resonance_value.get(hero)
        if not value:
            raise AssertionError(f"未找到对应英雄的共鸣配置: {hero}")
        return value

    def get_resonance_by_link(self, hero, link_num):
        """
        根据英雄和第几个链接获取配置
        :param hero: 英雄id
        :param link_num: 第几个共鸣
        :return:
        """
        hero_resonance = self.get_hero_resonance(hero)
        for resonance in hero_resonance:
            if resonance.linknum == link_num:
                return resonance
        raise AssertionError(f"未找到对应的共鸣配置: {hero}, link_num={link_num}")


class HeroExclusiveEntity:
    """basic_hero_exclusive"""

    def __init__(self, value):
        self.exclusiveid = value.get('exclusiveid')
        self.level = value.get('level')
        self.state = value.get('state')
        self.star = value.get('star')
        self.attr = value.get('attr')
        self.skill = value.get('skill')
        self.cost = value.get('cost')
        self.skinid = value.get('skinid')
        self.heroid = value.get('heroid')
        self.name = value.get('name')
        self.icon = value.get('icon')
        self.model = value.get('model')
        self.desc = value.get('desc')
        self.unlockskin = value.get('unlockskin')
        self.note = value.get('note')


class HeroInfoEntity:
    """basic_hero_info"""

    def __init__(self, value):
        self.heroid = value.get('heroid')
        self.note = value.get('note')
        self.name = value.get('name')
        self.title = value.get('title')
        self.info = value.get('info')
        self.faction = value.get('faction')
        self.role = value.get('role')
        self.gender = value.get('gender')
        self.position = value.get('position')
        self.skill1 = value.get('skill1')
        self.skill2 = value.get('skill2')
        self.skill3 = value.get('skill3')
        self.exclusive = value.get('exclusive')
        self.open = value.get('open')
        self.head = value.get('head')
        self.body = value.get('body')
        self.mode = value.get('mode')
        self.energy = value.get('energy')
        self.range = value.get('range')
        self.defskin = value.get('defskin')
        self.choose = value.get('choose')
        self.skillbody = value.get('skillbody')
        self.hatred = value.get('hatred')
        self.style = value.get('style')
        self.victorytext1 = value.get('victorytext1')
        self.victorytext2 = value.get('victorytext2')
        self.defeattext1 = value.get('defeattext1')
        self.defeattext2 = value.get('defeattext2')
        self.character = value.get('character')
        self.suit = value.get('suit')
        self.random1 = value.get('random1')
        self.rangetag = value.get('rangetag')
        self.show = value.get('show')
        self.aid = value.get('aid')
        self.choosehero = value.get('choosehero')
        self.highpoint = value.get('highpoint')


class HeroStarEntity:
    """basic_hero_star"""

    def __init__(self, value):
        self.heroid = value.get('heroid')
        self.state = value.get('state')
        self.star = value.get('star')
        self.cost1 = value.get('cost1')
        self.cost2 = value.get('cost2')
        self.attr = value.get('attr')
        self.attrper = value.get('attrper')
        self.costtype1 = value.get('costtype1')
        self.costtype2 = value.get('costtype2')


class HeroLevelEntity:
    """basic_hero_lvlup"""

    def __init__(self, value):
        self.heroid = value.get('heroid')
        self.level = value.get('level')
        self.state = value.get('state')
        self.levelcost = value.get('levelcost')
        self.breakcost = value.get('breakcost')
        self.skill = value.get('skill')
        self.attr = value.get('attr')


class HeroResonanceEntity:
    """basic_hero_resonance"""

    def __init__(self, value):
        self.heroid = value.get('heroid')
        self.resquality = value.get('resquality')
        self.resstar = value.get('resstar')
        self.linknum = value.get('linknum')
        self.linkquality = value.get('linkquality')
        self.linkstar = value.get('linkstar')
        self.attr = value.get('attr')
        self.skill = value.get('skill')
