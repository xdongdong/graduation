import datetime
# from loguru import logger as log
import os

from settings import LOG_DIR
import logging


# log_path = LOG_DIR + "loguru_" + str(datetime.date.today()) + '.log'
# log_handle = log.add(
#     sink=log_path,
#     encoding="utf-8",
#     # 日志保存时间上限
#     retention="2 days"
# )


class Logger:

    def __init__(self, file_prefix):
        self.log_path_prefix = LOG_DIR
        if not os.path.exists(LOG_DIR):
            os.makedirs(LOG_DIR)
        self.logger = logging.getLogger(file_prefix)
        self.filename = f"{file_prefix}_{str(datetime.date.today())}.log"
        self.logger.setLevel(logging.DEBUG)

    def get_logger(self):
        log_path = self.log_path_prefix + self.filename
        file_handler = logging.FileHandler(log_path, encoding="utf-8")
        console_handler = logging.StreamHandler()
        file_handler.setFormatter(
            logging.Formatter("[%(asctime)s] - [%(levelname)s] - [%(filename)s:%(lineno)d] - [%(message)s]"))
        console_handler.setFormatter(logging.Formatter("[%(asctime)s] - [%(levelname)s] - [%(filename)s:%(lineno)d] - [%(message)s]"))
        self.logger.addHandler(file_handler)
        self.logger.addHandler(console_handler)
        return self.logger


api_log = Logger("api").get_logger()
ui_log = Logger("ui").get_logger()
db_log = Logger("db").get_logger()