import os
from configparser import ConfigParser
import yaml
from settings import DATA_DIR, BASE_DIR
from utils.log import api_log


class ReadData:

    def get_yaml_data(self, file):
        api_log.info("获取yaml文件数据: " + file)
        if not os.path.isfile(DATA_DIR + file):
            raise FileNotFoundError("文件路径不存在， 请检查路劲是否正确： %s" % DATA_DIR + file)
        with open(DATA_DIR + file, encoding="utf-8") as f:
            data = yaml.safe_load(f)
        return data

    def get_db_info(self, section, db=None):
        config = ConfigParser()
        config.clear()
        config.read(BASE_DIR + '/mysql_config.ini', encoding="utf-8")
        data = dict(config.items(section))
        data['port'] = int(data.get('port'))
        data['db'] = db
        return data

    def get_redis_info(self, section, db=0):
        cfg = ConfigParser()
        cfg.clear()
        cfg.read(BASE_DIR + '/redis_config.ini', encoding="utf-8")
        data = dict(cfg.items(section))
        data['port'] = int(data.get('port'))
        data['db'] = db
        return data
