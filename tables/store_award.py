from abc import ABC
from tables.base import BaseManager
from utils.log import db_log
from utils.mysql import Mysql


class StoreAwardManager(BaseManager, ABC):
    def __init__(self, conn: Mysql):
        if hasattr(self, "config"):
            return
        self.config = None
        self.id_setting = {}
        self.award_setting = {}
        self.conn = conn

        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(StoreAwardManager, "_instance"):
            StoreAwardManager._instance = object.__new__(cls)
        return StoreAwardManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_storeaward')

        for con in self.config:
            self.id_setting[con.get('id')] = StoreAwardEntity(con)
            store_award = con.get("storeaward")
            if store_award in self.award_setting.keys():
                value = self.award_setting.get(store_award)
                value.append(StoreAwardEntity(con))
            else:
                self.award_setting[store_award] = [StoreAwardEntity(con)]
        db_log.info('加载配置成功: basic_storeaward')

    def get_settings_by_award(self, award_id):
        config = self.award_setting.get(award_id)
        if not config:
            raise AssertionError(f"未在basic_storeaward表中找到对应的id： {award_id}")
        return config


class StoreAwardEntity:

    def __init__(self, value):
        self.id = value.get('id')
        self.storeaward = value.get('storeaward')
        self.goods = value.get('goods')
        self.type = value.get('type')
        self.price = value.get('price')
        self.weight = value.get('weight')
        self.order = value.get('order')
        self.rprice = value.get('rprice')
