from copy import copy
from apis.base_api import BaseApi


class PetApi(BaseApi):
    # 英雄
    __quick_level_path = "/game/pet/quickgenestrengthen"


    def quick_level(self, aid, to_level):
        """
        开启物品
        :param aid: 生化兽id
        :param to_level: 要升到的等级
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "aid": aid,
            "tolevel": to_level,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__quick_level_path, data=data)
        return re
