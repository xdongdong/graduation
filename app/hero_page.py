import allure
from poco.proxy import UIObjectProxy
from app.base_page import BasePage
from testcase.data.hero_data import HeroData
from utils.util import replace_color


class HeroPage(BasePage):
    _HERO_LIST = {"name": "c", "children": True, "parent": {"name": "v", "parent": {"name": "heros"}}}
    _LEVEL_UP_BUTTON = {"name": "btn_uplevel"}
    _LEVEL_ATTR = {"name": "aimattr", "parent": {"name": "attr1"}}
    _LEVEL_SKILL_UP = {"name": "123"}
    _COST = {"name": "cost", "offspring": {"name": "comparison"}}

    _QUICK_LEVEL = {"name": "btn_onekey"}

    def back_to_lobby(self):
        with allure.step(f"返回大厅页面"):
            self.find_and_click(self._BACK_BUTTON)

        from app.lobby_page import LobbyPage
        return LobbyPage(self.driver, self.poco)

    def switch_to_hero(self, hero):
        """
        切换到对应id的hero
        :param hero:  hero_id
        :return:
        """
        with allure.step(f"切换至对应的英雄: {hero}"):
            locator = {"name": str(hero)}
            self.find_and_click(locator)
        return self

    def click_level_up(self):
        """
        点击升级按钮
        :return:
        """
        with allure.step(f"点击升级按钮"):
            self.find_and_click(self._LEVEL_UP_BUTTON)
        return self

    def click_quick_level(self):
        """
        点击升级按钮
        :return:
        """
        with allure.step(f"点击快速升级按钮"):
            self.find_and_click(self._QUICK_LEVEL)
        return self

    def get_hero_level(self, hero):
        """
        查找英雄列表中对应英雄的等级
        :param hero:
        :return:
        """
        with allure.step(f"获取英雄前端展示等级: {hero}"):
            if isinstance(hero, str):
                hero_locator = {"name": "level", "parent": {"name": "root", "parent": {"name": str(hero)}}}
                level = self.find(hero_locator)
            elif isinstance(hero, UIObjectProxy):
                level = hero.child("root").child("level")
            else:
                raise ValueError(f"传入错误的类型: {type(hero)}, 仅支持 str, UIObjectProxy")

            level_text = replace_color(level.get_text())
            level_text = level_text.replace("Lv", '')
        return int(level_text)

    def get_hero_list(self):
        with allure.step(f"获取当前页面的英雄列表"):
            return self.find(self._HERO_LIST)

    def show_skill_up(self):
        with allure.step(f"升级到指定等级后等待技能升级界面出现"):
            # 升级成功后技能等级提升
            self.wait_for_appearance(self._LEVEL_SKILL_UP)

    def get_hero_level_cost(self, hero):
        """
        获取英雄升级消耗
        :param hero:
        :return:
        """
        data = HeroData()
        with allure.step(f"获取当前英雄升级所需消耗"):
            self.switch_to_hero(hero)
            costs = self.find(self._COST)
            cost_num1 = replace_color(costs[0].get_text()).split("/")[-1]
            cost_num2 = replace_color(costs[1].get_text()).split("/")[-1]
        return {
            data.hero_level_cost1: int(cost_num1),
            data.hero_level_cost2: int(cost_num2)
        }

    def quick_level_exits(self, timeout=None):
        print("quick", timeout)
        return self.is_exits(self._QUICK_LEVEL, timeout)
