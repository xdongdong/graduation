import random
import allure
import pytest
from apis.chat_api import ChatApi
from apis.training_api import TrainApi
from tables.award import AwardManager
from tables.train import TrainManager
from testcase.data.train_data import TrainData
from utils.comparators import Comparators
from utils.param_encrypt import decrypt
from utils.util import parse_json, generate_openid


class TestTrain:

    def setup_class(self):
        # api类
        self.api = TrainApi()
        # 将openid进行随机,每次都是用不重复的openid
        openid = generate_openid()
        # 登录
        user_info, host = self.api.login(openid)
        # 用户信息
        self.role_id = user_info.get("roleid")
        # 数据类
        self.data = TrainData()
        # 初始化chat api
        self.chat_api = ChatApi(self.api.base_data)
        # 断言类
        self.compare = Comparators()
        # 奖励类
        self.award = AwardManager(self.api.db_utils.sql_conn)
        # train
        self.train = TrainManager(self.api.db_utils.sql_conn)

    @pytest.mark.smoke
    def test_smoke(self):
        self.api.clear_user_train()

        # 解锁关卡
        self.chat_api.chat_talk("gm addallsid 1")
        with allure.step("拉取关卡信息"):
            res = self.api.train_info()
            response = decrypt(res.text)
            self.compare.equal(response.get("code"), "1", "拉取信息")

        with allure.step("开始挑战"):
            start_res = self.api.train_start()
            start_response = decrypt(start_res.text)
            self.compare.equal(start_response.get("code"), "1", "开始挑战")

            # 验证码及随机buff
            verify = parse_json("verify", start_response)
            random_buff = parse_json("randombuff", start_response)

        with allure.step("保存波次"):
            save_res = self.api.train_save(verify, random_buff[0])
            save_response = decrypt(save_res.text)
            self.compare.equal(save_response.get("code"), "1", "中途保存")

        with allure.step("开始下一波"):
            start_res = self.api.train_start()
            start_response = decrypt(start_res.text)
            # 验证码及随机buff
            verify = parse_json("verify", start_response)

        with allure.step("进行失败结算"):
            # 成功结算
            settle_res = self.api.train_settle(verify, 0)
            settle_response = decrypt(settle_res.text)
            self.compare.equal(settle_response.get("code"), "1", "失败结算")

    def test_lock(self):
        """
        未解锁关卡的情况下拉取接口
        :return:
        """
        self.api.db_utils.clear_user_stage(self.role_id)
        with allure.step("未解锁时拉取info接口"):
            res = self.api.train_info()
            response = decrypt(res.text)
            self.compare.not_equal(response.get("code"), "1", f"未解锁时拉取信息: {response}")
        with allure.step("未解锁时拉取start接口"):
            res = self.api.train_start()
            response = decrypt(res.text)
            self.compare.not_equal(response.get("code"), "1", f"未解锁时开始挑战: {response}")

    def test_unlock(self):
        """
        已解锁时拉取信息
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        with allure.step("已解锁时拉取信息"):
            res = self.api.train_info()
            response = decrypt(res.text)

        plot_info = parse_json("plotinfo", response)
        first_level = self.train.get_all_stages()[0]
        with allure.step("对拉取信息接口返回的数据进行验证"):
            self.check_info(plot_info, first_level.level, 1)

    def test_error_save(self):
        """
        传入错误的参数进行保存
        :return:
        """
        self.api.unlock_train()

        with allure.step("拉取信息"):
            info_re = self.api.train_info()
            info_response = decrypt(info_re.text)
            plot_info = parse_json("plotinfo", info_response)

        with allure.step("开始挑战"):
            re = self.api.train_start()
            response = decrypt(re.text)
            verify = parse_json("verify", response)
            buffs = parse_json("randombuff", response)
            self.check_buff(plot_info, buffs)

        with allure.step("传入错误的验证码进行波次保存"):
            save_re = self.api.train_save(verify + "A", buffs[0])
            save_response = decrypt(save_re.text)
            # {'code': '-108000', 'msg': '非法结算'}
            self.compare.equal(save_response.get("code"), "-108000", "传入错误的验证码进行保存波次")

        with allure.step("传入未在可选择列表中的buff进行保存"):
            all_buff = self.train.get_all_buff_id()
            test_buff = None
            for buff in all_buff:
                if buff not in buffs:
                    test_buff = buff
                    break
            save_buff_re = self.api.train_save(verify, test_buff)
            save_buff_response = decrypt(save_buff_re.text)
            self.compare.equal(save_buff_response.get("code"), "109019", "选择未在可选列表中的buff进行保存波次")

        with allure.step("重新获取一个验证码"):
            re = self.api.train_start()
            response = decrypt(re.text)
            verify = parse_json("verify", response)
            buffs = parse_json("randombuff", response)
            self.check_buff(plot_info, buffs)

        with allure.step("重复使用验证码进行保存"):
            save_first_re = self.api.train_save(verify, buffs[0])
            save_first_response = decrypt(save_first_re.text)
            self.compare.equal(save_first_response.get("code"), "1", "首次保存应成功")

            save_second_re = self.api.train_save(verify, buffs[0])
            save_second_response = decrypt(save_second_re.text)
            self.compare.equal(save_second_response.get("code"), "-108000", "重复保存应失败")

    def test_advance_settle(self):
        """
        波次仅通关第三波, 进行胜利结算
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        # 波次仅通关第三波, 进行胜利结算
        plot = self.challenge(wave=3, complete_status=1)
        # 中途胜利后拉取信息
        info_res = self.api.train_info()
        info_response = decrypt(info_res.text)
        server_plot = parse_json("plotinfo", info_response)

        self.compare.equal(server_plot.get("level"), plot.get("level"), f"中途胜利通关后难度等级无变化")
        self.compare.equal(server_plot.get("wave"), 1, f"中途胜利通关后关卡波次被重置")

    def test_success_settle(self):
        """
        波次全通, 胜利结算
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        # 波次全通, 胜利结算
        plot = self.challenge(wave=-1, complete_status=1)
        # 通关后拉取信息
        info_res = self.api.train_info()
        info_response = decrypt(info_res.text)
        server_plot = parse_json("plotinfo", info_response)

        self.compare.equal(server_plot.get("level"), plot.get("level") + 1, f"通关后难度等级+1")
        self.compare.equal(server_plot.get("wave"), 1, f"通关后波次变为第一波")

    def test_continue_success(self):
        """
        连续通关两次
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        # 连续通关两次
        first = self.challenge(wave=-1, complete_status=1)
        second = self.challenge(wave=-1, complete_status=1)
        self.compare.equal(first.get("level") + 1, second.get("level"), f"通关第二次难度非第一次难度+1: \n"
                                                                        f"first: {first}\n"
                                                                        f"second: {second}")

    def test_fail_settle(self):
        """
        波次全通, 失败结算
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        # 波次全通, 失败结算
        plot = self.challenge(wave=-1, complete_status=0)
        # 失败后拉取信息
        info_res = self.api.train_info()
        info_response = decrypt(info_res.text)
        server_plot = parse_json("plotinfo", info_response)

        self.compare.equal(server_plot.get("level"), plot.get("level"), f"失败后当前等级不变化")
        self.compare.equal(server_plot.get("wave"), 1, f"失败后波次变为第一波")

    def test_restart_success(self):
        """
        中途重新开始挑战并成功结算
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        # 先拉取一次信息 服务器存入初始数据
        self.api.train_info()
        # 将玩家难度修改至非初始难度
        all_length = len(self.train.get_all_stages())
        test_level = self.train.get_all_stages()[int(all_length / 2)].level
        self.update_user_train(test_level)
        # 在第四波的时候重新开始挑战并通关
        info = self.challenge(wave=-1, complete_status=1, restart=4)
        self.compare.equal(test_level, info.get("level"), f"重新开始后难度不一致: {info}")

    def test_start_max(self):
        """
        在已经达到最大关卡时进行挑战
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        max_setting = self.train.get_all_stages()[-1]
        # 先拉取一次信息 服务器存入初始数据
        self.api.train_info()
        with allure.step(f"将玩家当前进度修改至最大: level={max_setting.level}, wave={self.data.max_wave}"):
            self.update_user_train(max_setting.level, wave=self.data.max_wave)

        with allure.step(f"检查当前关卡已达最大等级时,info接口返回数据是否正常"):
            res = self.api.train_info()
            response = decrypt(res.text)
            plot_info = parse_json("plotinfo", response)
            # 由于是最大关卡 wave服务器单独处理了+1
            self.check_info(plot_info, max_setting.level, self.data.max_wave + 1)

        with allure.step(f"开始挑战"):
            start_re = self.api.train_start()
            start_response = decrypt(start_re.text)
            self.compare.not_equal(start_response.get("code"), "1", f"已达最大等级时进行挑战: {start_response}")

    def test_restart_max(self):
        """
        在已经达到最大关卡时进行重开
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        max_setting = self.train.get_all_stages()[-1]
        # 先拉取一次信息 服务器存入初始数据
        self.api.train_info()
        with allure.step(f"将玩家当前进度修改至最大: level={max_setting.level}, wave={self.data.max_wave}"):
            self.update_user_train(max_setting.level, wave=self.data.max_wave)

        with allure.step(f"检查当前关卡已达最大等级时,info接口返回数据是否正常"):
            res = self.api.train_info()
            response = decrypt(res.text)
            plot_info = parse_json("plotinfo", response)
            # 由于是最大关卡 wave服务器单独处理了+1
            self.check_info(plot_info, max_setting.level, self.data.max_wave + 1)

        with allure.step(f"进行重新开始"):
            restart_re = self.api.train_restart()
            restart_response = decrypt(restart_re.text)
            self.compare.not_equal(restart_response.get("code"), "1", f"已达最大等级时进行重新开始: {restart_response}")

    def test_concurrent_settle(self):
        """
        并发结算
        :return:
        """
        self.api.clear_user_train()
        self.api.unlock_train()

        # 先拉取一次信息 服务器存入初始数据
        self.api.train_info()
        # 获取任意一条配置
        cur_setting = random.choice(self.train.get_all_stages())

        with allure.step(f"将玩家当前进度修改至还差一关结算: level={cur_setting.level}, wave={self.data.max_wave - 1}"):
            self.update_user_train(cur_setting.level, wave=self.data.max_wave - 1)

        with allure.step(f"开始挑战"):
            re = self.api.train_start()
            response = decrypt(re.text)
            verify = parse_json("verify", response)

        with allure.step(f"进行并发结算"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            params = {"verify": verify, "complete_status": 1}
            results = self.api.concurrent_func(self.api.train_settle, **params)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            setting_prop = self.award.get_award_drop(cur_setting.award)
            success = False
            for result in results:
                response = decrypt(result.text)
                if parse_json("code", response) == "1":
                    self.compare.equal_bool(success, False, "首次成功")
                    self.compare.changing(response, before_prop, changed_prop, setting_prop)
                    success = True

    def check_info(self, plot_info, level, wave):
        """
        检查Info接口返回信息是否正确
        :param plot_info: 服务器返回的关卡信息
        :param level: 期望的当前难度
        :param wave: 期望的当前波次
        :return:
        """
        cur_level = plot_info.get("level")
        cur_wave = plot_info.get("wave")
        cur_award = plot_info.get("award")
        cur_status = plot_info.get("status")

        cur_setting = self.train.get_stage_by_level(level)
        max_setting = self.train.get_all_stages()[-1]

        self.compare.equal(level, cur_level, f"当前训练等级不一致")
        self.compare.equal(wave, cur_wave, f"当前训练波次不一致")
        self.compare.equal(cur_setting.award, cur_award, f"当前返回奖励掉落组不一致")
        if level == max_setting.level and wave > len(max_setting.distribute.split("|")) + 1:
            self.compare.equal(cur_status, 1, f"状态为已通关: level={level}, wave={wave}")
        else:
            self.compare.equal(cur_status, 0, f"状态为未通关: level={level}, wave={wave}")

    def check_buff(self, plot, random_buff):
        """
        检查随机的buff是否为对应配置的buff
        :param plot: 关卡信息
        :param random_buff:  服务器返回的随机buff
        :return:
        """
        level = plot.get("level")
        wave = plot.get("wave")
        lvl_setting = self.train.get_stage_by_level(level)
        wave_key = int(lvl_setting.distribute.split("|")[wave - 1])
        for buff in random_buff:
            buff_setting = self.train.get_buff_by_id(buff)
            buff_key = [int(b) for b in buff_setting.distribute.split("|")]
            self.compare.contains(buff_key, wave_key, f"随机buff错误: level={level}, wave={wave}, buff={buff}")

    def update_user_train(self, level, wave=0):
        """
        修改玩家模拟训练的难度及波次
        :param level: 难度
        :param wave: 波次
        :return:
        """
        self.api.db_utils.sql_conn.update(
            f"update {self.api.setting.DB}.user_train_info set `level`={level}, `wave`={wave} where roleid = {self.role_id}"
        )

    def challenge(self, wave=-1, complete_status=1, settle=1, restart=-1):
        """
        挑战关卡
        :param restart: 重新开始的波次 -1为不重开
        :param settle: 是否强制结算，即到了wave波次后进行结算 否则仅在通关时结算或者失败的状态下到达指定波次结算
        :param complete_status: 是否通关 1 通关 0 失败
        :param wave: 需要通关多少波次 -1为全部通关
        :return:
        """
        with allure.step("拉取信息"):
            info_re = self.api.train_info()
            info_response = decrypt(info_re.text)
            plot_info = parse_json("plotinfo", info_response)

        # 获取当前关卡的配置
        cur_level = plot_info.get("level")
        cur_wave = plot_info.get("wave")
        cur_setting = self.train.get_stage_by_level(cur_level)
        # 已选择的buff
        choose_buff = []
        # 获取开战前的物品
        before_prop = self.api.db_utils.get_user_props(self.role_id)
        setting_drop = self.award.get_award_drop(cur_setting.award)
        stage_length = len(cur_setting.distribute.split("|")) + 1
        restart_flag = False
        while cur_wave <= stage_length:
            plot_info["wave"] = cur_wave
            with allure.step("开始挑战"):
                re = self.api.train_start()
                response = decrypt(re.text)
                verify = parse_json("verify", response)
                random_buff = parse_json("randombuff", response)
                buffs = parse_json("buff", response)
                # 非第一波时验证返回的buff信息是否为自己所选择的
                if cur_wave != 1:
                    self.compare.equal(choose_buff, buffs, f"服务器返回buff与玩家选择buff不一致: \n"
                                                           f"server: {buffs} \n"
                                                           f"user: {choose_buff}")
            # 最后一关结算
            if cur_wave == stage_length:
                with allure.step(f"结算关卡: 当前波次:{wave} 结算状态: {complete_status}"):
                    settle_re = self.api.train_settle(verify, complete_status)
                    settle_response = decrypt(settle_re.text)
                    self.compare.equal(settle_response.get("code"), "1", f"当前难度: {cur_level} 成功结算: {settle_response}")
                if complete_status == 1:
                    with allure.step("成功结算后拉取信息是否难度提升"):
                        info_re = self.api.train_info()
                        info_response = decrypt(info_re.text)
                        plot_info = parse_json("plotinfo", info_response)
                        # 非最后一个难度
                        if cur_level != self.train.get_all_stages()[-1].level:
                            self.compare.equal(cur_level + 1, plot_info.get("level"), f"成功通关后难度未提升: \n"
                                                                                      f"结算前难度: {cur_level} \n"
                                                                                      f"结算后难度: {plot_info.get('level')}")
                break
            # 中途失败结算
            elif cur_wave == wave and complete_status == 0:
                with allure.step(f"结算关卡: 当前波次:{wave} 结算状态: {complete_status}"):
                    settle_re = self.api.train_settle(verify, complete_status)
                    settle_response = decrypt(settle_re.text)
                    self.compare.equal(settle_response.get("code"), "1", f"当前难度: {cur_level} 成功结算: {settle_response}")
                    break
            # 到达指定波次后强制结算
            elif cur_wave == wave and settle == 1:
                with allure.step(f"结算关卡: 当前波次:{wave} 结算状态: {complete_status}"):
                    settle_re = self.api.train_settle(verify, complete_status)
                    settle_response = decrypt(settle_re.text)
                    self.compare.equal(settle_response.get("code"), "1",
                                       f"当前难度: {cur_level}波次: {cur_wave} 结算: {settle_response}")
                    break
            # 当前波次为重新开始的波次 且并没有重新开始过
            elif cur_wave == restart and not restart_flag:
                with allure.step(f"在第{cur_wave}波 进行重新开始然后继续挑战"):
                    restart_re = self.api.train_restart()
                    restart_response = decrypt(restart_re.text)
                    # 重开后获得新的verify 和random buff
                    verify = parse_json("verify", restart_response)
                    random_buff = parse_json("randombuff", restart_response)
                    self.compare.equal(restart_response.get("code"), "1", f"重新开始关卡: {restart_response}")

                    with allure.step("重开后重新拉取信息"):
                        info_re = self.api.train_info()
                        info_response = decrypt(info_re.text)
                        plot_info = parse_json("plotinfo", info_response)
                    with allure.step("断言波次无变化"):
                        self.compare.equal(cur_level, plot_info.get("level"), f"重新开始后难度变化了")
                        self.compare.equal(1, plot_info.get("wave"), f"重新开始后波次不为1")

                    # 重置当前波次和已选择的buff
                    cur_wave = 1
                    choose_buff = []
                    # 重新开始过了
                    restart_flag = True

                    # 重新开始之后直接保存, 不需要再调重新开始接口
                    self.check_buff(plot_info, random_buff)
                    with allure.step("重开后保存波次信息"):
                        # 将已保存的buff信息存储
                        get_buff = random.choice(random_buff)
                        choose_buff.append(get_buff)
                        save_re = self.api.train_save(verify, get_buff)
                        save_response = decrypt(save_re.text)
                        self.compare.success_code(save_response,
                                                  msg=f"重开后当前难度: {cur_level}保存第{cur_wave}波次: {save_response}")
                    cur_wave += 1

            # 其余波次保存信息
            else:
                # 非最后一波的其余波次检测buff随机是否正确
                self.check_buff(plot_info, random_buff)
                with allure.step("保存波次信息"):
                    # 将已保存的buff信息存储
                    get_buff = random.choice(random_buff)
                    choose_buff.append(get_buff)
                    save_re = self.api.train_save(verify, get_buff)
                    save_response = decrypt(save_re.text)
                    self.compare.equal(save_response.get("code"), "1",
                                       f"当前难度: {cur_level}保存第{cur_wave}波次: {save_response}")
                cur_wave += 1

        # 验证结算奖励
        changed_prop = self.api.db_utils.get_user_props(self.role_id)
        # 成功结算，且非提前结算
        if complete_status == 1 and cur_wave == stage_length:
            self.compare.changing(settle_response, before_prop, changed_prop, setting_drop)
        else:
            changing = parse_json("changing", settle_response)
            self.compare.equal(changing, {}, f"通关波次: {wave} 结算状态: {complete_status}, 返回值内物品变化: {settle_response}")
            # 验证结算奖励 需要验证
            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            self.compare.equal(before_prop, changed_prop, f"通关波次: {wave}, 是否胜利: {complete_status}, 物品变化: \n"
                                                          f"before: {before_prop} \n"
                                                          f"changed: {changed_prop}")
        return {
            "level": cur_level,
            "wave": cur_wave,
            "response": settle_response
        }
