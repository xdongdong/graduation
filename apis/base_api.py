import json
import socket
from copy import copy
from json import JSONDecodeError
import allure
import requests
from settings import get_config
from utils.db_utils import DbUtils
from utils.log import api_log
from utils.my_thread import MyThread
from utils.param_encrypt import decrypt, encrypt
from utils.util import parse_json


class BaseApi:
    __reload_setting_path = "/game/test/reloadsetting"

    def __init__(self, data=None):
        if data:
            self.host = data.get("host")
            self.user_info = data.get("user_info")
            self.db_utils = data.get("db_utils")
            self.setting = data.get("setting")
            self.role_id = data.get("role_id")
        else:
            self.host = ''
            self.role_id = ""
            self.user_info = {}
            # 连接数据库
            self.db_utils = DbUtils()
            # 配置
            self.setting = get_config()
            api_log.info(f"当前执行环境: {self.setting.ENV}")

    def http_post(self, path, host="", **kwargs):
        """
        发送post请求
        :param path:  请求路径
        :param host:  主机地址
        :param kwargs: 请求参数
        :return: Response
        """
        # 如果传入host使用传入的, 未传入使用自身的api_host
        host = host if host else self.host
        data = kwargs.get("data")
        if data:
            data["lan"] = "cn"
        with allure.step(f"发送post请求: {host + path}, data={data}"):
            # 加密
            api_log.info(f"send post api: {host}{path} -{data}")
            if data and data.get("encryptdata"):
                data["encryptdata"] = encrypt(data.get("encryptdata"))
            # 三次重试机会
            times = 3
            for i in range(times):
                try:
                    res = requests.post(host + path, **kwargs)
                except (requests.ConnectionError, socket.error) as e:
                    if i == times - 1:
                        raise ValueError(f"http 链接异常: {e}")
                    api_log.error(f"第{i + 1}次请求连接异常: {host + path}: {e}")
                    continue
                else:
                    break
            try:
                api_log.info(
                    f"response:  \n {json.dumps(decrypt(res.text), sort_keys=True, indent=2, separators=(',', ':'), ensure_ascii=False)}")
            except JSONDecodeError as e:
                api_log.error(f"response 解析失败: {res.text}")
        return res

    def http_get(self, path, host="", **kwargs):
        """
        发送get请求
        :param path:  请求路径
        :param host:  主机地址
        :param kwargs: 请求参数
        :return: Response
        """
        host = host if host else self.host
        api_log.info(f"send get api: {host}-{path} -{kwargs}")
        res = requests.post(host + path, **kwargs)
        api_log.info(
            f"response: \n {json.dumps(decrypt(res.text), sort_keys=True, indent=2, separators=(',', ':'), ensure_ascii=False)}")
        return res

    def concurrent_send(self, method, path, times, host='', **kwargs):
        """
        并发发送http请求
        :param host:  主机地址
        :param method:  请求方式
        :param path:  请求路径
        :param times:  并发次数
        :param kwargs:  请求参数
        :return: List[Response]
        """
        threads = []
        results = []
        host = host if host else self.host
        for i in range(times):
            threads.append(MyThread(target=requests.request,
                                    kwargs=({"method": method, "url": host + path, "data": kwargs.get("data")})))
        for t in threads:
            api_log.info(f"concurrent send post api, times: {times}: {host}{path} -{kwargs} ")
            t.start()
        for t in threads:
            t.join()
            result = t.get_result()
            api_log.info(
                f"concurrent response:  \n {json.dumps(decrypt(result.text), sort_keys=True, indent=2, separators=(',', ':'), ensure_ascii=False)}")
            results.append(result)
        return results

    def concurrent_func(self, func, times=2, **kwargs):
        """
        并发执行某个方法
        :param times: 次数
        :param func: 方法
        :param kwargs: 参数
        :return:
        """
        threads = []
        results = []
        for i in range(times):
            threads.append(MyThread(target=func, kwargs=kwargs))
        for t in threads:
            api_log.info(f"concurrent send post api, times: {times}: func: {func}  params:{kwargs} ")
            t.start()
        for t in threads:
            t.join()
            result = t.get_result()
            results.append(result)
        return results

    def login(self, openid, zoneid=None):
        """
        登录
        :param zoneid: 区服id
        :param openid: 登录id
        :return: (user_info, host)
        """
        if zoneid is None:
            zoneid = self.setting.ZONE
        openid, zoneid = str(openid), str(zoneid)
        api_log.info(f"用户登录: {openid}-{zoneid}")

        center_host = self.setting.HOST
        api_host = ''
        # 登录中心服
        login_data = {
            "encryptdata": {"channelid": 1, "openkey": 1, "sign": '1', "openid": openid, "plat": 1, "os": "pc",
                            "appid": 1}}
        login_response = decrypt(self.http_post('/login', host=center_host, data=login_data).text)
        try:
            data_content = login_response.get("data")
        except KeyError:
            print(openid, login_response)
            raise ValueError("登录失败")
        token = data_content['token']
        uuid = data_content['uid']

        # 获取区服
        zones_data = {"uuid": uuid, "token": token, "lan": "cn"}
        zones_response = decrypt(self.http_post("/zones", host=center_host, data=zones_data).text)
        zones = parse_json("zones", zones_response)
        for zone in zones:
            if str(zone.get("zoneid")) == zoneid:
                self.host = api_host = zone.get("apiurl")
                break
        # 登录到服务器
        login_server_data = {"uid": uuid, "zoneid": zoneid, "client_version": 0, "mac": "ACDE48001122",
                             "network_type": 2, "device_model": 0, "platform": "pc"}
        login_sever_data = {'uuid': uuid, 'token': token, 'encryptdata': login_server_data}
        login_server_response = decrypt(
            self.http_post('/login/server', host=center_host, data=login_sever_data).text)
        role_list = parse_json('rolelist', login_server_response)

        # 获取roleid
        roleid = None
        for role in role_list:
            if str(role.get('zoneid')) == zoneid:
                roleid = role.get('roleid')
                break

        # 同步用户
        getuser_data = {"version": "0"}
        getuser_data = {'encryptdata': getuser_data, 'roleid': roleid, 'uuid': uuid, 'token': token}
        self.http_post('/game/sync/user', host=api_host, data=getuser_data)
        self.user_info = {'roleid': roleid, 'uuid': uuid, 'token': token, 'zoneid': zoneid}
        self.role_id = roleid
        # self.host = api_host

        return self.user_info, api_host

    def reload_setting(self):
        """
        重新加载配置
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__reload_setting_path, data=data)
        return re

    @property
    def base_data(self):
        return {
            "host": self.host,
            "user_info": self.user_info,
            "role_id": self.role_id,
            "setting": self.setting,
            "db_utils": self.db_utils
        }
