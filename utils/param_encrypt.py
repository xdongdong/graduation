# -*- encoding=utf8 -*-
import json
import requests


# 这是将参数加密的方法
def encrypt(json_code):
    session = requests.session()
    login_data = ""
    login_url = ""
    login_re = session.post(login_url, data=login_data)
    login_re.close()
    encode_url = ""
    json_code = json.dumps(json_code)
    json_data = {'rawStr': json_code, 'type': 1}
    encode_re = session.post(encode_url, data=json_data)
    encode_re.close()
    en_params = encode_re.text
    en_params = json.loads(en_params)
    en_params = en_params['data']
    en_text = en_params['qLBasic64']
    session.close()
    return en_text


# 参数解密方法
def decrypt(str_code):
    session = requests.session()
    login_data = ""
    login_url = ""
    login_re = session.post(login_url, data=login_data)
    login_re.close()
    decode_url = ""
    de_data = {"QLBasic64": str_code, "type": 3}
    dedata_re = session.post(decode_url, data=de_data)
    dedata_re.close()
    de_json = json.loads(dedata_re.text)
    de_data = de_json['data']
    de_data = de_data['rawStr']
    session.close()
    return json.loads(de_data)
