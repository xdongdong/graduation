import json
from copy import copy
import allure
from apis.base_api import BaseApi
from apis.chat_api import ChatApi
from testcase.data.camp_act_data import CampActData
from utils.time_utlis import TimeUtils


class CampActApi(BaseApi):
    # 排行信息
    __stage_rank = "/game/camp/stage/rank"
    # 副本挑战活动
    __copy_info = '/game/camp/copyinfo'
    __copy_start = "/game/camp/copystart"
    __copy_settle = "/game/camp/copysettle"
    __copy_buy = "/game/camp/buy"

    data = CampActData()

    def stage_rank(self, start=1):
        """
        拉取事件排行信息
        :param start: 从第几名开始
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "start": start,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__stage_rank, data=data)
        return re

    def copy_info(self):
        """
        获取阵营副本挑战活动详情
        :return:
        """
        data = copy(self.user_info)
        re = self.http_post(self.__copy_info, data=data)
        return re

    def copy_start(self, level):
        """
        开始挑战
        :param level: 难度等级
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "level": level,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__copy_start, data=data)
        return re

    def copy_settle(self, verify, complete_status):
        """
        副本结算
        :param verify: 验证码
        :param complete_status: 1 成功  0失败
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "completestatus": complete_status,
            "verify": verify
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__copy_settle, data=data)
        return re

    def copy_buy(self, count):
        """
        购买副本门票
        :param count: 要购买的数量
        :return:
        """
        data = copy(self.user_info)
        encrypt_data = {
            "count": count,
        }
        data["encryptdata"] = encrypt_data
        re = self.http_post(self.__copy_buy, data=data)
        return re

    def update_event_finish(self, finish_num):
        """
        修改当前事件进度值
        :param finish_num:
        :return:
        """
        # 修改缓存数值
        with allure.step(f"修改当前正在进行的事件的进度值为: {finish_num}"):
            value = self.db_utils.api_redis.conn.get(self.data.event_info_key)
            value = json.loads(value)
            value["finish"] = finish_num
            self.db_utils.api_redis.conn.set(self.data.event_info_key, json.dumps(value))

    def reset_camp_event(self):
        """
        重置阵营事件
        :return:
        """
        with allure.step(f"清除阵营活动当前数据"):
            tables = [self.setting.DB + '.' + table for table in self.data.tables]
            self.db_utils.clear_db_data(tables)
            self.db_utils.clear_redis_data("*camp*")

    def add_event_rank(self, stage, num, multi=1):
        """
        添加排名数据
        :param multi: 排名值是排名的几倍 默认为1
        :param stage: 事件id
        :param num: 需要添加的数量
        :return:
        """
        rank = {}
        with allure.step(f"添加阵营事件排行榜数据: stage={stage} 添加排行榜人数={num}, 排行榜分值为排名的几倍={multi}"):
            users = self.db_utils.sql_conn.getLine(f"select roleid from {self.setting.DB}.user_info order by ce desc limit {num}")
            for i in range(1, num + 1):
                role = users[i - 1]
                score = (num - i) + 1
                # 第二个排名 构建相同的排名值, 但时间顺序在后面
                if i == 2:
                    score = score - 1
                score = TimeUtils().generate_time_value(score * multi)
                self.db_utils.api_redis.conn.zadd(self.data.event_rank_key.format(stage), {
                    f'"{role}"': score
                })
                rank[i] = {"role_id": role, "score": score}
        return rank

    def open_act(self, act_data):
        """
        开启对应的活动
        :param act_data: 对应活动的配置数据
        :return:
        """
        # 清除阵营数据
        tables = [self.setting.DB + '.' + table for table in self.data.tables]
        with allure.step(f"清除阵营活动当前数据"):
            self.db_utils.clear_db_data(tables)
            self.db_utils.clear_redis_data("*camp*")

        # 修改配置并开启活动
        with allure.step(f"修改配置开启活动: {act_data}"):
            open_stage = self.update_act(act_data)
        return open_stage

    def update_act(self, act_data):
        """
        修改开启活动的配置
        :param act_data: 活动配置
        :return:
        """
        chat_api = ChatApi()
        data = CampActData()
        update_count = 0
        # 获取第一个事件
        stages = self.db_utils.sql_conn.getOne(f"select stage from basic_camp_event where id = {data.camp}")
        first_stage = int(stages.split("|")[0])
        open_act = act_data.get('act_id')

        stage_setting = self.db_utils.sql_conn.getRow(f"select * from basic_camp_event_stage where stage = {first_stage}")
        act_setting = self.db_utils.sql_conn.getRow(f"select * from basic_camp_event_stage where activity = {open_act}")

        stage = stage_setting.pop("stage")
        act = act_setting.pop("stage")
        # 原本的数据去除要修改的字段
        for field in act_data.get("field"):
            field = field.replace("`", "")
            stage_setting.pop(field)
            act_setting.pop(field)
        # 更新
        stage_setting.update(act_data.get("field"))
        # 如果第一个事件配置的活动不是该活动, 则全部更改
        if stage_setting.get("activity") != open_act:
            update_count += self.db_utils.update_db("basic_camp_event_stage", stage_setting, {"stage": act})
            update_count += self.db_utils.update_db("basic_camp_event_stage", act_setting, {"stage": stage})
        # 否则只修改自己定义的字段
        else:
            update_count += self.db_utils.update_db("basic_camp_event_stage", act_data.get("field"), {"stage": stage})

        # 获取原本活动中心中该阶段的活动配置和要修改的活动配置
        stage_value = self.db_utils.sql_conn.getRow(f"select * from act_center where begintime like '%%{first_stage}%%'")
        act_value = self.db_utils.sql_conn.getRow(f"select * from act_center where id = {open_act}")

        # 将该活动绑定的阶段修改为测试的阶段
        stage_id = stage_value.pop("id")
        act_id = act_value.pop("id")
        if stage_id != act_id:
            # 修改开启的配置
            update_count += self.db_utils.update_db("act_center",
                                           {"begintime": stage_value.get("begintime")},
                                           {"id": act_id})
            update_count += self.db_utils.update_db("act_center",
                                           {"begintime": act_value.get("begintime")},
                                           {"id": stage_id})
        # 刷新缓存
        if update_count > 0:
            self.reload_setting()
        chat_api.chat_talk(f"gm campprogress 1001 {data.schedule}")
        return first_stage
