from abc import ABC
from tables.all_prop import AllManager
from tables.base import BaseManager
from tables.item import ItemManager
from utils.log import db_log


class AwardManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "values"):
            return
        self.conn = conn
        self.config = None
        self.values = {}

        self.PropManager = AllManager(conn)
        self.ItemManager = ItemManager(conn)
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(AwardManager, "_instance"):
            AwardManager._instance = object.__new__(cls)
        return AwardManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_award')
        for con in self.config:
            award = AwardEntity(**con)
            rewards = []
            # 解析固定掉落
            if award.reward:
                rewards_split = award.reward.split('|')
                for reward in rewards_split:
                    reward_split = [int(r) for r in reward.split(';')]
                    pid = reward_split[0]
                    num = reward_split[1]
                    rewards.append({
                        'pid': str(pid),
                        'num': num
                    })
                award.reward = rewards
            # 解析随机掉落
            if award.random:
                randoms = []
                random_split = award.random.split('|')
                for reward in random_split:
                    reward_split = [int(r) for r in reward.split(';')]
                    randoms.append({
                        'pid': reward_split[0],
                        'num': reward_split[1],
                        'weight': reward_split[2],
                        'lockweight': reward_split[3],
                    })
                award.random = randoms
            self.values[con.get('rewid')] = award
        db_log.info('加载配置成功: basic_award')

    # 只获得配置第一级的掉落物品
    def get_reward_config(self, rew):
        return self.values.get(int(rew))

    # 检查所有掉落物品 有自动打开的 继续掉落
    def get_award_drop(self, rew):
        award = self.get_reward_config(rew)
        if not award:
            return None
        all_fix_reward = []
        all_random_reward = []

        def get(reward_id):
            # 获取到随机和固定奖励
            rew_reward = self.get_reward_config(reward_id)
            if not rew_reward:
                return None
            fix_rewards = rew_reward.reward
            random_rewards = rew_reward.random
            # 遍历固定奖励
            for fix_reward in fix_rewards:
                fix_reward["pid"] = str(fix_reward.get("pid"))
                save = True
                pid = fix_reward.get('pid')
                aid_info = self.PropManager.getConfigByAid(int(pid))
                fix_reward['name'] = aid_info.note
                if aid_info.basic == 'basic_item':
                    pid_info = self.ItemManager.getConfigByAid(int(pid))
                    if pid_info:
                        fix_reward['name'] = pid_info.note
                    # 61类型是英雄
                    if pid_info and pid_info.auto and pid_info.usetype != 61:
                        # 如果是会被自动打开的物品则不存入掉落
                        save = False
                        get(pid_info.useargs)
                if save:
                    all_fix_reward.append(fix_reward)

            # 随机奖励
            for random_reward in random_rewards:
                random_reward["pid"] = str(random_reward.get("pid"))
                save = True
                pid = random_reward.get('pid')
                aid_info = self.PropManager.getConfigByAid(int(pid))
                random_reward['name'] = aid_info.note
                random_reward['mode'] = rew_reward.mode
                random_reward['lock'] = rew_reward.lock
                random_reward['number'] = rew_reward.number
                random_reward['back'] = rew_reward.back
                if aid_info.basic == 'basic_item':
                    pid_info = self.ItemManager.getConfigByAid(int(pid))
                    if pid_info:
                        random_reward['name'] = pid_info.note
                    # 61类型是英雄
                    if pid_info and pid_info.auto and pid_info.usetype != 61:
                        save = False
                        get(pid_info.useargs)
                if save and random_reward.get("num") != 0:
                    all_random_reward.append(random_reward)

        get(rew)
        award.reward = all_fix_reward
        award.random = all_random_reward
        return award


class AwardEntity:
    """basic_award"""
    def __init__(self, **kwargs):
        self.rewid = kwargs.get('rewid')
        self.note = kwargs.get('note')
        self.reward = kwargs.get('reward') or []
        self.lock = kwargs.get('lock')
        self.random = kwargs.get('random') or []
        self.mode = kwargs.get('mode')
        self.number = kwargs.get('number')
        self.back = kwargs.get('back')
