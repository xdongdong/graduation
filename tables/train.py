from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class TrainManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "train_config"):
            return
        self.train_config = None
        self.buff_config = None
        self.train_values = {}
        self.buff_values = {}

        self.conn = conn
        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(TrainManager, "_instance"):
            TrainManager._instance = object.__new__(cls)
        return TrainManager._instance

    def load_config(self):
        self.train_config = self.conn.getALL('select * from basic_training')
        self.buff_config = self.conn.getALL('select * from basic_training_buff')
        for con in self.train_config:
            self.train_values[con.get('level')] = TrainEntity(con)
        db_log.info('加载配置成功: basic_training')
        for con in self.buff_config:
            self.buff_values[con.get('id')] = TrainBuffEntity(con)
        db_log.info('加载配置成功: basic_training_buff')

    def get_stage_by_level(self, lvl):
        return self.train_values.get(lvl)

    def get_all_stages(self):
        return list(self.train_values.values())

    def get_buff_by_id(self, _id):
        return self.buff_values.get(_id)

    def get_all_buff(self):
        return list(self.buff_values.values())

    def get_all_buff_id(self):
        return list(self.buff_values.keys())


class TrainEntity:

    def __init__(self, value):
        self.stageid = value.get('stageid')
        self.level = value.get('level')
        self.chip = value.get('chip')
        self.mainboard = value.get('mainboard')
        self.award = value.get('award')
        self.distribute = value.get('distribute')


class TrainBuffEntity:

    def __init__(self, value):
        self.id = value.get('id')
        self.note = value.get('note')
        self.name = value.get('name')
        self.desc = value.get('desc')
        self.distribute = value.get('distribute')
        self.weights = value.get('weights')
        self.quality = value.get('quality')
        self.type = value.get('type')
        self.args = value.get('args')
        self.icon = value.get('icon')
        self.addcombat = value.get('addcombat')
        self.unit = value.get('unit')
        self.strike = value.get('strike')
