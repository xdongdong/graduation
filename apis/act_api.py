from apis.base_api import BaseApi
from utils.comparators import Comparators
from utils.time_utlis import TimeUtils


class ActApi(BaseApi):


    def check_act_open(self, act_config, start, end):
        """
        检查活动开启时间和结束时间是否正确
        :param act_config:  活动配置 dict
        :param start:  开始时间
        :param end:  结束时间
        :return:
        """
        time_utils = TimeUtils()
        compare = Comparators()
        if not act_config.get("actswitch"):
            raise AssertionError(f"活动配置为未开启: {act_config}")
        act_begin = act_config.get("begintime")
        begin_type, begin_param = act_begin.split(";")
        if begin_type == "1":
            pass
        elif begin_type == "5":
            start_time = time_utils.str_to_datetime(begin_param)
            start_timestamp = time_utils.datetime_to_timestamp(start_time)
            compare.equal(start_timestamp, start, f"开始时间验证: {act_config}")

            end_timestamp = start_timestamp + (act_config.get("lasttime") * 1000)
            compare.equal(end_timestamp, end, f"结束时间验证: {act_config}")
