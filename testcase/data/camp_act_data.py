import datetime


class CampActData:
    # 要加入的阵营
    camp = 1001
    # 阵营活动要清除数据的表
    tables = [
        "camp_donate_logs",
        "camp_event_info",
        "camp_event_kill",
        "camp_event_stage",
        "camp_event_stage_buff",
        "camp_event_stage_record",
        "user_camp_join_history",
        "camp_event_success",
        "camp_copy_info"
    ]
    # 触发事件所需的贡献值
    schedule = 100000
    copy_ticket = "2010105"
    # 免费挑战次数key
    free_challenge_key = f"camp_copy_free_challenge:{datetime.date.today()}"
    # 免费挑战全局id
    free_times_global = 10110
    # 购买挑战次数global id
    buy_times_global = 10111
    # 活动延迟关闭时间 global id
    delay_close_time = 10109
    # 事件进度值
    event_info_key = "camp_event_stage_trigger"
    # 事件排名key 需要添加对应事件id
    event_rank_key = "camp_stage_rank:{}"
    # 副本活动
    act_copy = {
        "act_id": 2011122,
        # 修改配置的字段
        "field": {
            "`or`": 1,
            "`time`": 60 * 60,
        }
    }
    # 阵营事件成功邮件奖励id
    mail_event_reward = 7200010
    # 阵营事件排行奖励邮件id
    mail_event_rank = 7200011
    # 阵营事件完成次数邮件奖励id
    mail_event_time = 7200012
    # 阵营事件未领取奖励发放邮件id
    mail_event_not_get = 7200013
