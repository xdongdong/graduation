from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class TechManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.config = None
        self.values = {}
        self.conn = conn

        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(TechManager, "_instance"):
            TechManager._instance = object.__new__(cls)
        return TechManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_tech')

        for con in self.config:
            self.values[con.get('tecid')] = TechEntity(con)

        db_log.info('加载配置成功: basic_tech')

    def get_tech(self):
        return list(self.values.values())


class TechEntity:

    def __init__(self, value):
        self.tecid = value.get('tecid')
        self.id = value.get('id')
        self.leadgroup = value.get('leadgroup')
        self.level = value.get('level')
        self.maxlevel = value.get('maxlevel')
        self.age = value.get('age')
        self.type = value.get('type')
        self.name = value.get('name')
        self.desc = value.get('desc')
        self.mess = value.get('mess')
        self.icon = value.get('icon')
        self.position = value.get('position')
        self.connect = value.get('connect')
        self.lock = value.get('lock')
        self.cost = value.get('cost')
        self.time = value.get('time')
        self.paddid = value.get('paddid')
        self.showattr = value.get('showattr')
        self.power = value.get('power')
