import copy
import datetime
import allure
import pytest
from apis.chat_api import ChatApi
from apis.mercenary_api import MercenaryApi
from tables.award import AwardManager
from tables.award import AwardEntity
from tables.mercenary import MercenaryIntensifyManager
from testcase.data.mercenary_data import MercenaryData
from utils.GetHashCode import GetHashCode
from utils.comparators import Comparators
from utils.hero_attr import HeroAttr
from utils.log import api_log
from utils.param_encrypt import decrypt, encrypt
from utils.util import parse_json, merge_dict, generate_openid


class TestMercenary:


    def setup_class(self):
        # api类
        self.api = MercenaryApi()
        # 将openid进行随机,每次都是用不重复的openid
        openid = generate_openid()
        # 登录
        self.user_info, host = self.api.login(openid)
        # 用户信息
        self.role_id = self.user_info.get("roleid")
        # 数据类
        self.data = MercenaryData()
        # 初始化chat api
        self.chat_api = ChatApi(self.api.base_data)
        # 断言类
        self.compare = Comparators()
        # 奖励类
        self.award = AwardManager(self.api.db_utils.sql_conn)
        # 计算属性类
        self.attr = HeroAttr(self.role_id, self.api.db_utils.sql_conn, mode="mer")
        # 佣兵副本强化配置类
        self.mer_intensify = MercenaryIntensifyManager(self.api.db_utils.sql_conn)

    def test_lock(self):
        """
        未解锁的情况下
        :return:
        """
        self.api.db_utils.clear_user_stage(self.role_id)

        api_log.info(f"当前执行用例: 未解锁情况下拉取信息接口")
        res = self.api.get_info()
        response = decrypt(res.text)
        self.compare.equal(response.get("code"), "1", "断言code为1")
        self.compare.equal(response.get("data").get("lock"), 1, "断言lock为1")

    def test_cross_difficulty(self):
        """
        跨难度挑战
        :return:
        """
        self.api.unlock_mercenary()

        api_log.info(f"当前执行用例: 跨难度进行挑战")
        # 解锁关卡
        res = self.api.get_info()
        response = decrypt(res.text)
        tasks = response.get("data").get("task")
        setting_tasks = self.api.db_utils.sql_conn.getALL("select * from basic_mercenary_mission")

        # 任务数量与配置一致
        self.compare.equal(len(tasks), len(setting_tasks))

        # 选择第二个难度
        choose_res = self.api.choose_level(self.data.missions[1])
        choose_response = decrypt(choose_res.text)

        # response {'code': '109009', 'msg': '关卡难度未开启'}
        self.compare.equal(choose_response.get("code"), "109009", "断言code为难度尚未开启的code")

    def test_lack_ticket(self):
        """
        缺少材料时选择关卡
        :return:
        """
        self.api.unlock_mercenary()

        res = self.api.choose_level(self.data.missions[0])
        response = decrypt(res.text)
        self.compare.equal(response.get("code"), "-110001")

    @pytest.mark.smoke
    def test_difficulty_C(self):
        """
        已解锁的情况下通关C难度
        :return:
        """
        self.api.unlock_mercenary()
        self.api.db_utils.clear_user_package(self.role_id)

        self.challenge_mission(self.data.missions[0])

    @pytest.mark.xfail(1==1, reason="客户端做了限制")
    def test_no_hero(self):
        """
        未选择英雄时进行挑战
        :return:
        """
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()

        # 获取一张低级门票
        with allure.step(f"获取物品: {self.data.lower_ticket}"):
            self.chat_api.chat_talk(f"gm prop {self.data.lower_ticket} 1")
        with allure.step("选择关卡"):
            # 选择关卡
            self.api.choose_level(self.data.missions[0])
        # 重新拉取信息接口， 获取随机到的关卡信息
        with allure.step("获取关卡信息"):
            info_res = self.api.get_info()
            info_response = decrypt(info_res.text)
            stages = info_response.get("data").get("stage")
            # 按position这个key进行排序
            stages.sort(key=lambda x: x["position"])
        # 未选择英雄时挑战
        with allure.step("未选择英雄时进行挑战"):
            challenge_stage = stages[0]
            ch_res = self.api.challenge(challenge_stage.get("missionid"), challenge_stage.get("id"))
            ch_response = decrypt(ch_res.text)
            # 非法操作
            self.compare.equal(ch_response.get("code"), "-110000")

    def test_cross_stage(self):
        """
        不按照顺序进行挑战
        :return:
        """
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()

        # 获取一张低级门票
        with allure.step(f"获取物品: {self.data.lower_ticket}"):
            self.chat_api.chat_talk(f"gm prop {self.data.lower_ticket} 1")
        with allure.step("选择关卡"):
            # 选择关卡
            self.api.choose_level(self.data.missions[0])
        # 重新拉取信息接口， 获取随机到的关卡信息
        with allure.step("获取关卡信息"):
            info_res = self.api.get_info()
            info_response = decrypt(info_res.text)
            stages = info_response.get("data").get("stage")
            # 按position这个key进行排序
            stages.sort(key=lambda x: x["position"])
        # 不按顺序进行挑战
        with allure.step("不按顺序进行挑战"):
            challenge_stage = stages[1]
            ch_res = self.api.challenge(challenge_stage.get("missionid"), challenge_stage.get("id"))
            ch_response = decrypt(ch_res.text)
            # 非法操作
            self.compare.equal(ch_response.get("code"), "-110000")


    def test_failed(self):
        """
        关卡中途失败
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions[:1])

        self.challenge_mission(self.data.missions[1], win=5)

    def test_difficult_B_A(self):
        """
        通关B难度 A难度
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions[:1])

        test_missions = [self.data.missions[1], self.data.missions[2]]
        for test_mission in test_missions:
            self.challenge_mission(test_mission)

    def test_multiple_C(self):
        """
        给C级难度进行翻倍
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()

        res = self.api.choose_level(self.data.missions[0], multiple=2)
        response = decrypt(res.text)
        code = parse_json("code", response)
        # {"code":"-110000", "msg":"非法操作"}
        self.compare.equal(code, "-110000", "非法操作")


    def test_multiple_S_no_pass(self):
        """
        未通关S级难度的情况下 进行翻倍
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions[:-1])

        res = self.api.choose_level(self.data.missions[-1], multiple=2)
        response = decrypt(res.text)
        code = parse_json("code", response)
        # {"code":"109014", "msg":"通关一次S级任务才可进行翻倍"}
        self.compare.equal(code, "109014", "通关一次S级任务才可进行翻倍")


    def test_first_difficult_S(self):
        """
        首次通关S级难度
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions[:-1])

        test_mission = self.data.missions[-1]
        self.challenge_mission(test_mission)

        # 获取门票ID并获取该物品
        mission_info = self.api.db_utils.sql_conn.getRow(
            f"select * from basic_mercenary_mission where missionid = {test_mission}")
        ticket = mission_info.get("ticket").split(";")[0]
        with allure.step(f"获取门票: {ticket}"):
            self.chat_api.chat_talk(f"gm prop {ticket} 3")

        # 通关后进行翻倍选择
        res = self.api.choose_level(self.data.missions[-1], multiple=2)
        response = decrypt(res.text)
        code = parse_json("code", response)
        self.compare.equal(code, "1", "通关一次S级难度后进行翻倍成功")

    def test_multiple_S(self):
        """
        满足条件的情况下对S级难度翻倍
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        test_mission = self.data.missions[-1]
        self.challenge_mission(test_mission, multiple=5)

    def test_abandon_task(self):
        """
        中途放弃关卡
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        test_mission = self.data.missions[-1]
        self.challenge_mission(test_mission, multiple=1, abandon=True, win=5)
        with allure.step(f"通关{test_mission}难度后拉取信息判断是否解锁"):
            info_res = self.api.get_info()
            info_response = decrypt(info_res.text)
            tasks = parse_json("task", info_response)
            for task in tasks:
                self.compare.equal(task.get("finish"), 1, f"所有难度的通关次数均只为一次: {info_response}")

    def test_get_unlock_reward(self):
        """
        领取未达成的次数奖励
        :return:
        """
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        test_mission = self.data.missions[-1]
        mission_info = self.api.db_utils.sql_conn.getRow(
            f"select * from basic_mercenary_mission where missionid = {test_mission}")
        times_reward = mission_info.get("timesreward").split("|")
        # 获取次数奖励信息
        with allure.step("获取次数奖励信息"):
            info_res = self.api.get_reward_info(test_mission)
            info_response = decrypt(info_res.text)
            reward_info = parse_json("info", info_response)
            for reward in reward_info:
                self.compare.equal(int(times_reward[reward.get("count") - 1]), reward.get("rewardid"), "实际配置奖励与返回数据不符合")
                self.compare.equal(reward.get("status"), 0, "未通关时任意次数奖励状态为0")
        # 领取未解锁的次数奖励
        with allure.step("领取未解锁的次数奖励"):
            count = reward_info[0].get("count")
            res = self.api.get_reward(test_mission, count=count)
            response = decrypt(res.text)
            code = parse_json("code", response)
            self.compare.equal(code, "-110000", "未解锁奖励不可领取")

    def test_get_reward(self):
        """
        领取次数奖励
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions[:-1])

        test_mission = self.data.missions[-1]
        mission_info = self.api.db_utils.sql_conn.getRow(
            f"select * from basic_mercenary_mission where missionid = {test_mission}")
        # 通过关卡数 = 任务reward的长度
        win_count = len(mission_info.get("reward").split("|"))
        with allure.step(f"通关{test_mission}难度 {win_count}关"):
            self.challenge_mission(self.data.missions[-1], multiple=1)
        # 获取次数奖励信息
        with allure.step("获取次数奖励信息"):
            info_res = self.api.get_reward_info(test_mission)
            info_response = decrypt(info_res.text)
            reward_info = parse_json("info", info_response)
            for reward in reward_info:
                count = reward.get("count")
                if count <= win_count:
                    self.compare.equal(reward.get("status"), 2, "已通关对应次数时奖励状态为2")
                    with allure.step(f"领取次数奖励: {count}"):
                        # 领奖前的物品
                        before_prop = self.api.db_utils.get_user_props(self.role_id)
                        # 领取奖励
                        reward_res = self.api.get_reward(test_mission, count)
                        reward_response = decrypt(reward_res.text)
                        # 领奖后的物品
                        changed_prop = self.api.db_utils.get_user_props(self.role_id)
                        setting_prop = self.award.get_award_drop(reward.get("rewardid"))
                        self.compare.changing(reward_response, before_prop, changed_prop, setting_prop)
                else:
                    self.compare.equal(reward.get("status"), 0, "未通关对应次数时奖励状态为0")

    def test_repeat_get_reward(self):
        """
        重复领取奖励
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        test_mission = self.data.missions[-1]
        # 获取次数奖励信息
        with allure.step("获取次数奖励信息"):
            info_res = self.api.get_reward_info(test_mission)
            info_response = decrypt(info_res.text)
            reward_info = parse_json("info", info_response)
        win_count = reward_info[0].get("count")
        with allure.step(f"通关{test_mission}难度 {win_count}关"):
            self.challenge_mission(self.data.missions[-1], multiple=1, win=win_count)
        with allure.step(f"第一次领取奖励: {win_count}"):
            # 领奖前的物品
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            # 领取奖励
            reward_res = self.api.get_reward(test_mission, win_count)
            reward_response = decrypt(reward_res.text)
            self.compare.success_code(reward_response, msg="领取奖励返回code")
            # 领奖后的物品
            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            setting_prop = self.award.get_award_drop(reward_info[0].get("rewardid"))
            self.compare.changing(reward_response, before_prop, changed_prop, setting_prop)
        with allure.step(f"第二次领取奖励: {win_count}"):
            # 领取奖励
            reward_res = self.api.get_reward(test_mission, win_count)
            reward_response = decrypt(reward_res.text)
            self.compare.error_code(reward_response, msg=f"第二次领取同一次数奖励")

    def test_expire_get_reward(self):
        """
        有已过期的满足条件的数据领取奖励
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        test_mission = self.data.missions[-1]
        # 获取次数奖励信息
        with allure.step("获取次数奖励信息"):
            info_res = self.api.get_reward_info(test_mission)
            info_response = decrypt(info_res.text)
            reward_info = parse_json("info", info_response)
        win_count = reward_info[0].get("count")
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        with allure.step(f"添加昨日完成次数: 日期-{yesterday}, count-{win_count}"):
            # 添加昨日关卡完成次数
            key = f"{self.data.win_count_key}:{test_mission}:{yesterday}"
            self.api.db_utils.api_redis.conn.zadd(key, {f'"{self.role_id}"': win_count})

        with allure.step(f"领取奖励: {win_count}"):
            # 领取奖励
            reward_res = self.api.get_reward(test_mission, win_count)
            reward_response = decrypt(reward_res.text)
            code = parse_json("code", reward_response)
            self.compare.equal(code, "-110000", f"领取昨日奖励信息")

    def test_concurrent_get_reward(self):
        """
        并发领取奖励
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        test_mission = self.data.missions[-1]
        # 获取次数奖励信息
        with allure.step("获取次数奖励信息"):
            info_res = self.api.get_reward_info(test_mission)
            info_response = decrypt(info_res.text)
            reward_info = parse_json("info", info_response)
        win_count = reward_info[0].get("count")
        reward = self.award.get_award_drop(reward_info[0].get("rewardid"))
        today = datetime.date.today()
        with allure.step(f"添加完成次数: 日期-{today}, count-{win_count}"):
            # 添加昨日关卡完成次数
            key = f"{self.data.win_count_key}:{test_mission}:{today}"
            self.api.db_utils.api_redis.conn.zadd(key, {f'"{self.role_id}"': win_count})

        with allure.step(f"并发领取奖励: {win_count}"):
            data = copy.copy(self.user_info)
            data["encryptdata"] = encrypt({"missionid": test_mission, "count": win_count})
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            results = self.api.concurrent_send("post", self.api._get_reward_path, 2, data=data)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            success = False
            for result in results:
                response = decrypt(result.text)
                if parse_json("code", response) == "1":
                    self.compare.equal_bool(success, False, "首次成功")
                    self.compare.changing(response, before_prop, changed_prop, reward)
                    success = True

    def test_clean(self):
        """
        正常扫荡C难度关卡
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        # 可扫荡关卡
        clean_mission = self.data.missions[0]
        mission_info = self.api.db_utils.sql_conn.getRow(
            f"select * from basic_mercenary_mission where missionid = {clean_mission}")
        # 获取物品
        with allure.step("获取扫荡消耗物品"):
            self.chat_api.chat_talk(f"gm prop {self.data.lower_ticket} 10")
            self.chat_api.chat_talk(f"gm prop {self.data.high_ticket} 10")
        # 获取可扫荡关卡的物品掉落
        rewards = mission_info.get("reward").split("|")
        # 只能检查固定掉落
        fix_reward = []
        for reward in rewards:
            reward_info = self.award.get_award_drop(reward)
            s_fix = reward_info.reward
            for fix in s_fix:
                have_fix = [f for f in fix_reward if f.get("pid") == fix.get("pid")]
                if have_fix:
                    have_fix = have_fix[0]
                    have_fix["num"] = have_fix.get("num") + fix.get("num")
                else:
                    fix_reward.append(fix)
        # 单次扫荡
        with allure.step("对可扫荡的关卡进行单次扫荡"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            clean_res = self.api.clean(clean_mission, 1)
            clean_response = decrypt(clean_res.text)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            drops = []
            # 断言是否掉落了固定掉落物品
            for fix in fix_reward:
                pid = fix.get("pid")
                num = fix.get("num")
                changing = parse_json("changing", clean_response)
                drops.extend([f for f in changing.get("prop") if f.get("pid") == pid and f.get("num") == num])
            self.compare.not_equal(drops, [], f"未掉落对应的固定掉落物品,\n"
                                               f"changing: {changing} \n"
                                               f"setting_fix_drop: {fix_reward} \n"
                                               f"rewards:{rewards}")
            self.compare.changing(clean_response, before_prop, changed_prop)

        with allure.step("对可扫荡的关卡进行多次扫荡"):
            times = 5
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            clean_res = self.api.clean(clean_mission, times)
            clean_response = decrypt(clean_res.text)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            drops = []
            # 断言是否掉落了固定掉落物品
            for fix in fix_reward:
                pid = fix.get("pid")
                num = fix.get("num") * times
                changing = parse_json("changing", clean_response)
                drops.extend([f for f in changing.get("prop") if f.get("pid") == pid and f.get("num") == num])
            self.compare.not_equal(drops, [], f"未掉落对应的固定掉落物品,\n"
                                               f"changing: {changing} \n"
                                               f"setting_fix_drop: {fix_reward} \n"
                                               f"rewards:{rewards}")
            self.compare.changing(clean_response, before_prop, changed_prop)


    def test_not_clean(self):
        """
        对无法扫荡的关卡进行扫荡
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()
        self.api.unlock_difficult(missions=self.data.missions)

        # 不可扫荡关卡
        not_clean_missions = self.data.missions[1:]
        for mission in not_clean_missions:
            with allure.step(f"扫荡不可扫荡的关卡: {mission}"):
                clean_res = self.api.clean(mission, 1)
                clean_response = decrypt(clean_res.text)
                code = parse_json("code", clean_response)
                self.compare.equal(code, "109011", "不可扫荡的关卡返回code")


    def test_add_hero(self):
        """
        购买英雄测试
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()

        # 任务相关信息
        test_mission = self.data.missions[0]
        # 获取门票ID并获取该物品
        with allure.step(f"获取门票: {self.data.lower_ticket}"):
            self.chat_api.chat_talk(f"gm prop {self.data.lower_ticket} {1}")
        # 选择关卡
        with allure.step(f"选择任务: {test_mission}"):
            self.api.choose_level(test_mission)
        # 获取英雄信息
        with allure.step("拉取英雄信息"):
            hero_res = self.api.get_hero_info()
            hero_response = decrypt(hero_res.text)
            heros = parse_json("heros", hero_response)
            # 获取英雄配置
            setting_hero = self.api.db_utils.sql_conn.getALL("select * from basic_mercenary_choose")
            self.compare.equal(len(heros), 10, "随机10个英雄进行选择")
            for hero in heros:
                self.compare.contained_by(hero.get("heroid"), [h.get("heroid") for h in setting_hero], "随机出的英雄在配置的英雄中")
        # 选择英雄
        choose_hero = heros[0]
        # 获取到该英雄的对应配置
        s_hero = [hero for hero in setting_hero if hero.get("heroid") == choose_hero.get("heroid")][0]

        with allure.step(f"单个选择英雄: {choose_hero.get('heroid')}"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            # 发送请求
            choose_res = self.api.add_hero([choose_hero.get("heroid")])
            choose_response = decrypt(choose_res.text)
            code = parse_json("code", choose_response)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            # 扣除对应配置的瓶盖
            setting_prop = AwardEntity(reward=[{"pid": str(self.data.cap), "num": -s_hero.get("costcap")}])
            # 断言
            self.compare.equal(code, "1", "选择英雄响应code")
            self.compare.changing(choose_response, before_prop, changed_prop, setting_prop)

        # 多选
        choose_hero = heros[1: 3]
        # 获取到该英雄的对应配置
        s_hero = [hero for hero in setting_hero if hero.get("heroid") in [hero.get("heroid") for hero in choose_hero]]
        with allure.step(f"同时选择多个英雄: {choose_hero}"):
            before_prop = changed_prop

            # 发送请求
            hero_id = [hero.get("heroid") for hero in choose_hero]
            choose_res = self.api.add_hero(hero_id)
            choose_response = decrypt(choose_res.text)
            code = parse_json("code", choose_response)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            fix = {}
            # 扣除对应配置的瓶盖
            for hero in s_hero:
                fix = merge_dict(fix, {str(self.data.cap): -hero.get("costcap")})
            reward = []
            for pid, num in fix.items():
                reward.append({"pid": pid, "num": num})
            setting_drop = AwardEntity(reward=reward)
            # 断言
            self.compare.equal(code, "1", "选择英雄响应code")
            self.compare.changing(choose_response, before_prop, changed_prop, setting_drop)

    def test_hero_intensify(self):
        """
        英雄强化测试
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()

        # 任务相关信息
        test_mission = self.data.missions[0]
        # 获取门票ID并获取该物品
        with allure.step(f"获取门票: {self.data.lower_ticket}"):
            self.chat_api.chat_talk(f"gm prop {self.data.lower_ticket} {1}")
        # 选择关卡
        with allure.step(f"选择任务: {test_mission}"):
            self.api.choose_level(test_mission)
        # 获取英雄信息
        with allure.step("拉取英雄信息"):
            hero_res = self.api.get_hero_info()
            hero_response = decrypt(hero_res.text)
            heros = parse_json("heros", hero_response)
            # 获取英雄配置
            setting_hero = self.api.db_utils.sql_conn.getALL("select * from basic_mercenary_choose")
            for hero in heros:
                self.compare.contained_by(hero.get("heroid"), [h.get("heroid") for h in setting_hero], "随机出的英雄在配置的英雄中")
        # 选择英雄
        cost_cap = {}
        with allure.step("选择英雄"):
            choose_res = self.api.add_hero([heros[0].get("heroid")])
            cost_cap[str(self.data.cap)] = -heros[0].get("employcost")
            choose_response = decrypt(choose_res.text)
            code = parse_json("code", choose_response)
            self.compare.equal(code, "1", "选择英雄响应code")
            choose_hero = parse_json("heros", choose_response)[0]
        # 验证英雄属性
        with allure.step("验证英雄初始属性是否正确"):
            hid = choose_hero.get("hid")
            self.attr.assert_hero_attr(hid)
        # 获取足够多的瓶盖进行强化
        with allure.step(f"获取足够多的瓶盖进行强化"):
            self.chat_api.chat_talk(f"gm prop {self.data.cap} 999999")
        current_level = choose_hero.get("level")
        # 升级的类型为1
        current_setting = self.mer_intensify.get_setting_by_type_args(1, current_level)
        next_setting = self.mer_intensify.get_setting_by_id(current_setting.nextid)
        # 升级
        with allure.step("英雄升一级"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)

            lvl_res = self.api.hero_level_up(hid)
            lvl_response = decrypt(lvl_res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            setting_drop = AwardEntity(reward=[{"pid": str(self.data.cap), "num": -next_setting.amount}])
            cost_cap = merge_dict(cost_cap, {str(self.data.cap): -next_setting.amount})
            hero = parse_json("hero", lvl_response)

            self.compare.equal(hero.get("level"), next_setting.args, f"升级后等级验证")
            self.compare.changing(lvl_response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)

        with allure.step("英雄一键升级"):
            fix = {}
            while True:
                next_setting = self.mer_intensify.get_setting_by_id(next_setting.nextid)
                fix = merge_dict(fix, {str(self.data.cap): -next_setting.amount})
                if next_setting.nextid == 0:
                    break
            before_prop = changed_prop

            lvl_all_res = self.api.hero_level_all(hid)
            lvl_all_response = decrypt(lvl_all_res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            cost_cap = merge_dict(cost_cap, fix)
            reward = []
            for pid, num in fix.items():
                reward.append({"pid": pid, "num": num})
            setting_drop = AwardEntity(reward=reward)
            hero = parse_json("hero", lvl_all_response)

            self.compare.equal(hero.get("level"), next_setting.args, f"升级后等级验证")
            self.compare.changing(lvl_all_response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)
        # 升星
        current_grade = f'{hero.get("grade")}{hero.get("star")}'
        current_setting = self.mer_intensify.get_setting_by_type_args(2, int(current_grade))
        next_setting = self.mer_intensify.get_setting_by_id(current_setting.nextid)
        with allure.step("英雄升星一次"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)

            res = self.api.hero_star(hid)
            response = decrypt(res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            cost_cap = merge_dict(cost_cap, {str(self.data.cap): -next_setting.amount})
            hero = parse_json("hero", response)

            setting_drop = AwardEntity(reward=[{"pid": str(self.data.cap), "num": -next_setting.amount}])
            self.compare.equal(int(f'{hero.get("grade")}{hero.get("star")}'), next_setting.args, f"升星后星级验证")
            self.compare.changing(response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)

        with allure.step("英雄一键升星"):
            fix = {}
            while True:
                next_setting = self.mer_intensify.get_setting_by_id(next_setting.nextid)
                fix = merge_dict(fix, {str(self.data.cap): -next_setting.amount})
                if next_setting.nextid == 0:
                    break
            before_prop = changed_prop

            res = self.api.hero_star_all(hid)
            response = decrypt(res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            cost_cap = merge_dict(cost_cap, fix)
            hero = parse_json("hero", response)

            reward = []
            for pid, num in fix.items():
                reward.append({"pid": pid, "num": num})
            setting_drop = AwardEntity(reward=reward)

            self.compare.equal(int(f'{hero.get("grade")}{hero.get("star")}'), next_setting.args, f"一键升星后星级验证")
            self.compare.changing(response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)
        # 专属
        current_exclusive = hero.get("exclusiveEntity").get("level")
        current_setting = self.mer_intensify.get_setting_by_type_args(3, current_exclusive)
        next_setting = self.mer_intensify.get_setting_by_id(current_setting.nextid)
        with allure.step("英雄专属升级一次"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)

            res = self.api.exclusive_level_up(hero.get("exclusiveEntity").get("upid"))
            response = decrypt(res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            fix = {str(self.data.cap): -next_setting.amount}
            cost_cap = merge_dict(cost_cap, fix)
            hero = parse_json("hero", response)

            reward = []
            for pid, num in fix.items():
                reward.append({"pid": pid, "num": num})
            setting_drop = AwardEntity(reward=reward)

            self.compare.equal(hero.get("exclusiveEntity").get("level"), next_setting.args, f"专属升级后等级验证")
            self.compare.changing(response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)

        with allure.step("专属一键升级"):
            fix = {}
            while True:
                next_setting = self.mer_intensify.get_setting_by_id(next_setting.nextid)
                fix = merge_dict(fix, {str(self.data.cap): -next_setting.amount})
                if next_setting.nextid == 0:
                    break
            before_prop = changed_prop

            res = self.api.exclusive_level_all(hero.get("exclusiveEntity").get("upid"))
            response = decrypt(res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            cost_cap = merge_dict(cost_cap, fix)
            hero = parse_json("hero", response)

            reward = []
            for pid, num in fix.items():
                reward.append({"pid": pid, "num": num})
            setting_drop = AwardEntity(reward=reward)

            self.compare.equal(hero.get("exclusiveEntity").get("level"), next_setting.args, f"专属一键升级后等级验证")
            self.compare.changing(response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)
        # 共鸣
        current_resonance = hero.get("reslevel")
        current_setting = self.mer_intensify.get_setting_by_type_args(4, current_resonance)
        next_setting = self.mer_intensify.get_setting_by_id(current_setting.nextid)
        with allure.step("英雄共鸣升级一次"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)

            res = self.api.hero_resonance(hid)
            response = decrypt(res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            fix = {str(self.data.cap): -next_setting.amount}
            cost_cap = merge_dict(cost_cap, fix)
            hero = parse_json("hero", response)

            reward = []
            for pid, num in fix.items():
                reward.append({"pid": pid, "num": num})
            setting_drop = AwardEntity(reward=reward)

            self.compare.equal(hero.get("reslevel"), next_setting.args, f"共鸣升级后等级验证")
            self.compare.changing(response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)

        with allure.step("共鸣一键升级"):
            fix = {}
            while True:
                next_setting = self.mer_intensify.get_setting_by_id(next_setting.nextid)
                fix = merge_dict(fix, {str(self.data.cap): -next_setting.amount})
                if next_setting.nextid == 0:
                    break
            before_prop = changed_prop

            res = self.api.hero_resonance_all(hid)
            response = decrypt(res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)
            cost_cap = merge_dict(cost_cap, fix)
            hero = parse_json("hero", response)

            reward = []
            for pid, num in fix.items():
                reward.append({"pid": pid, "num": num})
            setting_drop = AwardEntity(reward=reward)

            self.compare.equal(hero.get("reslevel"), next_setting.args, f"共鸣一键升级后等级验证")
            self.compare.changing(response, before_prop, changed_prop, setting_drop)
            self.attr.assert_hero_attr(hid)
        # 拉取英雄信息 验证返回的属性及消耗物品
        with allure.step("强化完成后拉取信息验证结果"):
            hero_res = self.api.get_hero_info()
            hero_response = decrypt(hero_res.text)
            heros = parse_json("heros", hero_response)
            for h in heros:
                if h.get("heroid") == hero.get("heroid"):
                    return_hero = h
            # 验证英雄属性
            hero_attr = return_hero.get("attrmap")
            count_attr = self.attr.get_hero_attr(hid)
            for attr, value in hero_attr.items():
                self.compare.int_range(
                    value,
                    count_attr.get(attr),
                    expect_range=1,
                    message=f"强化完成后拉取接口属性验证---hero: {hid} check_attr: {attr}"
                )
            hero_cap = return_hero.get("capcost")
            self.compare.equal(cost_cap.get(str(self.data.cap)), -hero_cap, "强化整体消耗验证")

        with allure.step("删除英雄后验证返还瓶盖数量是否正确"):
            before_prop = changed_prop

            del_res = self.api.del_hero([hid])
            del_response = decrypt(del_res.text)

            changed_prop = self.api.db_utils.get_user_props(self.role_id)

            setting_drop = AwardEntity(reward=[{"pid": str(self.data.cap), "num": hero_cap}])

            self.compare.changing(del_response, before_prop, changed_prop, setting_drop)

    def test_lack_intensify(self):
        """
        材料不足的情况下英雄强化测试
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.clear_user_mercenary()
        self.api.unlock_mercenary()

        # 任务相关信息
        test_mission = self.data.missions[0]
        user_hash = GetHashCode.getHashCode(str(self.role_id))
        # 获取门票ID并获取该物品
        with allure.step(f"获取门票: {self.data.lower_ticket}"):
            self.chat_api.chat_talk(f"gm prop {self.data.lower_ticket} {1}")
        # 选择关卡
        with allure.step(f"选择任务: {test_mission}"):
            self.api.choose_level(test_mission)

        # 获取英雄信息
        with allure.step("拉取英雄信息"):
            hero_res = self.api.get_hero_info()
            hero_response = decrypt(hero_res.text)
            heros = parse_json("heros", hero_response)

        with allure.step("选择英雄"):
            choose_res = self.api.add_hero([heros[0].get("heroid")])
            choose_response = decrypt(choose_res.text)
            code = parse_json("code", choose_response)
            self.compare.equal(code, "1", "选择英雄响应code")
            choose_hero = parse_json("heros", choose_response)[0]
        hid = choose_hero.get("hid")
        upid = choose_hero.get("exclusiveEntity").get("upid")

        with allure.step("修改剩余材料数量为0"):
            self.api.db_utils.sql_conn.update(
                f"update {self.api.setting.DB}.user_package_{user_hash} set num = 0 where roleid = {self.role_id} and pid = {self.data.cap}")

        with allure.step("执行每个强化接口请求"):
            # 升级
            with allure.step("升级验证"):
                res = self.api.hero_level_up(hid)
                code = parse_json("code", decrypt(res.text))
                self.compare.not_equal(code, "1", "材料不足")

            # 一键升级
            with allure.step("一键升级验证"):
                res = self.api.hero_level_all(hid)
                code = parse_json("code", decrypt(res.text))
                self.compare.not_equal(code, "1", "材料不足")

            # 升星
            with allure.step("升星验证"):
                res = self.api.hero_star(hid)
                code = parse_json("code", decrypt(res.text))
                self.compare.not_equal(code, "1", "材料不足")

            # 一键升星
            with allure.step("一键升星验证"):
                res = self.api.hero_star_all(hid)
                code = parse_json("code", decrypt(res.text))
                self.compare.not_equal(code, "1", "材料不足")

            # 共鸣前置条件验证
            with allure.step("共鸣前置条件验证"):
                res = self.api.hero_resonance(hid)
                response = decrypt(res.text)
                code = parse_json("code", response)
                changing = parse_json("changing", response)
                self.compare.equal(code, "1", "共鸣前置条件不满足")
                self.compare.equal(changing, {}, "共鸣前置条件不满足")

            # 共鸣消耗材料验证
            with allure.step("共鸣消耗材料验证"):
                with allure.step("添加解锁数据"):
                    # 获取共鸣的第二条解锁数据， 也就是要强化到的那一条数据
                    resonance_setting = self.mer_intensify.get_setting_by_type_args(4, 1)
                    pres = resonance_setting.preid.split("|")
                    for pre in pres:
                        self.api.db_utils.sql_conn.insertOne(
                            f"insert into {self.api.setting.DB}.user_mercenary_strength_unlock values({self.role_id}, {hid}, {pre})")
                with allure.step("进行共鸣"):
                    res = self.api.hero_resonance(hid)
                    code = parse_json("code", decrypt(res.text))
                    self.compare.not_equal(code, "1", "材料不足")

            # 一键共鸣
            with allure.step("一键共鸣验证"):
                res = self.api.hero_resonance_all(hid)
                code = parse_json("code", decrypt(res.text))
                self.compare.not_equal(code, "1", "材料不足")

            # 专属
            with allure.step("专属升级验证"):
                res = self.api.exclusive_level_up(upid)
                code = parse_json("code", decrypt(res.text))
                self.compare.not_equal(code, "1", "材料不足")

            # 一键升级专属
            with allure.step("专属一键升级验证"):
                res = self.api.exclusive_level_all(upid)
                code = parse_json("code", decrypt(res.text))
                self.compare.not_equal(code, "1", "材料不足")

        with allure.step("验证一键强化时材料不足以全部强化"):
            with allure.step("获取升级三次的配置"):
                current_level = choose_hero.get("level")
                level_setting = self.mer_intensify.get_setting_by_type_args(1, current_level)
                # 获取升级所需的材料数量
                fix = {}
                for i in range(3):
                    level_setting = self.mer_intensify.get_setting_by_id(level_setting.nextid)
                    fix = merge_dict(fix, {str(self.data.cap): -level_setting.amount})
            cost_num = -fix.get(str(self.data.cap))
            with allure.step(f"获取升级到{level_setting.args} 所需的材料数量: {cost_num}"):
                self.chat_api.chat_talk(f"gm prop {self.data.cap} {cost_num}")
            with allure.step("进行一键升级"):
                before_prop = self.api.db_utils.get_user_props(self.role_id)

                level_res = self.api.hero_level_all(hid)
                level_response = decrypt(level_res.text)
                hero = parse_json("hero", level_response)

                changed_prop = self.api.db_utils.get_user_props(self.role_id)

                reward = []
                for pid, num in fix.items():
                    reward.append({"pid": pid, "num": num})
                setting_drop = AwardEntity(reward=reward)

                self.compare.changing(level_response, before_prop, changed_prop, setting_drop)
                self.compare.equal(hero.get("level"), level_setting.args, "一键升级后等级验证")

    def challenge_mission(self, test_mission, multiple=1, win=-1, abandon=False):
        """
        挑战佣兵副本
        :param abandon: 是否以放弃的方式结束本次挑战
        :param multiple: 翻倍倍数
        :param test_mission: 要挑战的难度
        :param win: 要通过的关卡数  -1 全通关
        :return:
        """
        mission_info = self.api.db_utils.sql_conn.getRow(
            f"select * from basic_mercenary_mission where missionid = {test_mission}")
        rewards = mission_info.get("reward").split("|")
        # 获取门票ID并获取该物品
        ticket = mission_info.get("ticket").split(";")[0]
        with allure.step(f"获取门票: {ticket}"):
            self.chat_api.chat_talk(f"gm prop {ticket} {multiple}")

        with allure.step(f"选择难度: {test_mission}"):
            # 获取玩家物品
            user_props = self.api.db_utils.get_user_props(self.role_id)
            # 选择关卡
            res = self.api.choose_level(test_mission, multiple=multiple)
            response = decrypt(res.text)
            # 获取接口后玩家背包物品
            changed_props = self.api.db_utils.get_user_props(self.role_id)

        # 断言接口返回code 断言物品扣减
        self.compare.equal(response.get("code"), "1")
        with allure.step("断言物品扣减"):
            self.compare.changing(response, user_props, changed_props)

        # 重新拉取信息接口， 获取随机到的关卡信息
        with allure.step("获取关卡信息"):
            info_res = self.api.get_info()
            info_response = decrypt(info_res.text)
            stages = info_response.get("data").get("stage")
            # 按position这个key进行排序
            stages.sort(key=lambda x: x["position"])
            # 是否存在雇主或者物资属性
            attrs = parse_json("attrs", info_response)

        self.compare.equal(len(stages), len(rewards), "关卡数与奖励配置长度验证")

        # 结算奖励, 只有最后一波打完或者中途失败才会发放
        all_rewards = {}
        for stage in stages:
            # 挑战状态 1成功 0失败
            status = 0 if stages.index(stage) == win else 1
            with allure.step(f"挑战关卡: 第{stages.index(stage) + 1}关: {stage}"):
                challenge_re = self.api.challenge(test_mission, stage.get("id"))
                challenge_response = decrypt(challenge_re.text)
                # 获取该关卡验证码
                verify = parse_json("verify", challenge_response)
            with allure.step(f"结算关卡: 第{stages.index(stage) + 1}关: {stage} 是否通关: {status}"):
                before_prop = changed_props
                # 是否放弃
                if abandon and status == 0:
                    settle_re = self.api.abandon_task(test_mission)
                else:
                    settle_re = self.api.settle(test_mission, stage.get("id"), status, verify, attrs=attrs)
                changed_props = self.api.db_utils.get_user_props(self.role_id)
                settle_response = decrypt(settle_re.text)
            with allure.step(f"验证结算奖励:: 第{stages.index(stage) + 1}关"):
                # 通过该关卡才记录
                if status != 0:
                    all_rewards = merge_dict(all_rewards, parse_json("reward", settle_response))
                reward = []
                for key, value in all_rewards.items():
                    reward.append({"pid": key, "num": value})
                setting_reward = AwardEntity(reward=reward)
                # 验证结算奖励
                self.check_plot_drop(stages.index(stage), mission_info, settle_response, status, before_prop,
                                changed_props, setting_reward, multiple)
            if status == 0:
                break
        # 通关后拉取信息
        if win == -1:
            # 通关后实际选择
            if len(self.data.missions) - self.data.missions.index(test_mission) >= 2:
                # 下一难度ID
                next_mission = self.data.missions[self.data.missions.index(test_mission) + 1]
                mission_info = self.api.db_utils.sql_conn.getRow(
                    f"select * from basic_mercenary_mission where missionid = {next_mission}")
                # 获取门票ID并获取该物品
                ticket = mission_info.get("ticket").split(";")[0]
                with allure.step(f"获取下一难度门票: {ticket}"):
                    self.chat_api.chat_talk(f"gm prop {ticket} {multiple}")
                # 验证是否解锁下一难度
                with allure.step(f"验证是否解锁下一难度: {next_mission}"):
                    res = self.api.choose_level(next_mission)
                    response = decrypt(res.text)
                    code = parse_json("code", response)
                    # {'code': '109009', 'msg': '关卡难度未开启'}
                    self.compare.equal(code, "1", f"{next_mission}级难度已解锁: {response}")
                # 放弃任务
                self.api.abandon_task(next_mission)

            # 多跨一个难度选择
            if len(self.data.missions) - self.data.missions.index(test_mission) >= 3:
                # 跨一难度ID
                next_two_mission = self.data.missions[self.data.missions.index(test_mission) + 2]
                mission_info = self.api.db_utils.sql_conn.getRow(
                    f"select * from basic_mercenary_mission where missionid = {next_two_mission}")
                # 获取门票ID并获取该物品
                ticket = mission_info.get("ticket").split(";")[0]
                with allure.step(f"获取下二难度门票: {ticket}"):
                    self.chat_api.chat_talk(f"gm prop {ticket} {multiple}")
                with allure.step(f"验证是否未解锁下二难度: {next_two_mission}"):
                    res = self.api.choose_level(next_two_mission)
                    response = decrypt(res.text)
                    code = parse_json("code", response)
                    # {'code': '109009', 'msg': '关卡难度未开启'}
                    self.compare.equal(code, "109009", f"{next_two_mission}级难度应该未解锁: {response}")
                # 放弃任务
                self.api.abandon_task(test_mission)

    def check_plot_drop(self, index, mission_info, response, status, before_prop, changed_prop,
                        save_rewards, multiple):
        """
        校验玩家佣兵副本结算奖励
        :param multiple: 翻倍倍数
        :param save_rewards: 存储的奖励
        :param status: 通关状态
        :param response: 响应
        :param mission_info: 任务详情
        :param before_prop: 结算前物品
        :param changed_prop:结算后物品
        :param index:当前第几关
        :return:
        """
        rewards = mission_info.get("reward").split("|")
        plot_num = index + 1
        # 挑战成功
        if status == 1:
            # 第五波获得瓶盖奖励
            if index == 4:
                cap_reward = AwardEntity(reward=[{"pid": str(self.data.cap), "num": mission_info.get("caps")}])
                # 瓶盖奖励
                self.compare.changing(response, before_prop, changed_prop, cap_reward)
            # 最后一关结算奖励
            elif index == 9:
                self.compare.changing(response, before_prop, changed_prop)
            # 其余关卡只存储奖励， 不发放
            else:
                save_reward = parse_json("reward", response)
                setting_reward = self.award.get_award_drop(rewards[index])
                fix_reward = setting_reward.reward
                random_reward = setting_reward.random
                if not save_reward and (fix_reward or random_reward):
                    raise AssertionError(f"第{plot_num}关 配置表有奖励,但服务器未存奖励: \n"
                                         f"setting_reward: {setting_reward}")
                # 校验奖励是否与配置表中配置的奖励一致
                have = False
                if fix_reward or random_reward:
                    for pid, num in save_reward.items():
                        for item in fix_reward:
                            have = True if item.get("pid") == pid and item.get("num") * multiple == num else False
                        for item in random_reward:
                            have = True if item.get("pid") == pid and item.get("num") * multiple == num else False
                    self.compare.equal_bool(have, True, f"第{plot_num}关 倍数: {multiple} 保存奖励未在配置表中: \n"
                                                   f"save_reward: {save_reward} \n"
                                                   f"setting_reward: {setting_reward.reward} \n"
                                                   f"setting_random: {setting_reward.random}")
                else:
                    self.compare.equal(save_reward, {}, f"配置表无奖励, 不保存任何奖励: \n"
                                                   f"save_reward: {save_reward}")
                # 不获得任何奖励
                self.compare.changing({}, before_prop, changed_prop)
        elif status == 0:
            # 中途失败计算奖励
            # 将changing中的瓶盖变化放到save_reward中 避免报错， 单独断言瓶盖数量应该扣除为0
            changing = parse_json("changing", response)
            for prop in changing.get("prop"):
                if prop.get("pid") == str(self.data.cap):
                    save_rewards.reward.append({"pid": prop.get("pid"), "num": prop.get("num")})
            # 获取玩家瓶盖
            user_cap_num = changed_prop.get(str(self.data.cap))
            self.compare.equal(user_cap_num, 0, "失败后扣除玩家背包内的所有瓶盖数量")
            # 断言瓶盖以外的其他奖励
            self.compare.changing(response, before_prop, changed_prop, save_rewards)
        else:
            raise ValueError(f"status not found only 0 or 1, get {status}")