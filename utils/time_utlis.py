import datetime
import time


class TimeUtils:

    @staticmethod
    def get_current_timestamp():
        return int(time.time() * 1000)

    @staticmethod
    def get_today_date():
        return datetime.date.today()

    @staticmethod
    def get_today_datetime():
        return datetime.datetime.now().replace(microsecond=0)

    @staticmethod
    def timestamp_to_date(time_stamp):
        """
        时间戳转日期
        :param time_stamp:
        :return:
        """
        if len(str(time_stamp)) == 13:
            time_stamp = int(time_stamp / 1000)
        time_array = time.localtime(time_stamp)
        date = time.strftime("%Y-%m-%d %H:%M:%S", time_array)
        return date

    @staticmethod
    def generate_time_value(value):
        """
        根据当前时间戳构建redis的排名值
        :param value:
        :return:
        """
        return value + (0.1 - time.time() * 0.00000000001)

    @staticmethod
    def update_date(date, day=0, hour=0):
        new_date = date + datetime.timedelta(days=day, hours=hour)
        return new_date

    @staticmethod
    def get_today_start_timestamp():
        now = TimeUtils.get_today_datetime()
        start = now.replace(hour=0).replace(minute=0).replace(second=0)
        time_array = time.strptime(str(start), "%Y-%m-%d %H:%M:%S")
        start_timestamp = int(time.mktime(time_array) * 1000)
        return start_timestamp

    @staticmethod
    def str_to_datetime(date_string):
        to_time = datetime.datetime.strptime(date_string, "%Y-%m-%d")
        return to_time

    @staticmethod
    def datetime_to_timestamp(date: datetime.datetime):
        return int(date.timestamp() * 1000)

    @staticmethod
    def date_to_datetime(date):
        result = datetime.datetime.strptime(str(date), '%Y-%m-%d')
        return result
