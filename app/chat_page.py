import allure

from app.base_page import BasePage
from utils.log import ui_log


class ChatPage(BasePage):
    _CHAT_INPUT = {"name": "input"}
    _SEND_BUTTON = {"name": "sendbtn"}
    _CHAT_LIST = {"name": "chatwnd", "offspring": {"name": "peritem"}}

    _BACK_BUTTON = {"name": "backbtn"}

    def send_message(self, msg):
        with allure.step(f"发送聊天信息: {msg}"):
            ui_log.info(f"发送聊天信息: {msg}")
            self.find_and_set_text(self._CHAT_INPUT, msg)
            self.find_and_click(self._SEND_BUTTON)
        return self

    def back_to_lobby(self):
        with allure.step(f"返回大厅页面"):
            self.find_and_click(self._BACK_BUTTON)

        from app.lobby_page import LobbyPage
        return LobbyPage(self.driver, self.poco)

    def get_message_list(self):
        with allure.step(f"获取当前页面上的聊天信息记录"):
            chat_list = self.find(self._CHAT_LIST)
            msgs = []
            for chat in chat_list:
                name = chat.child("other").child("userinfo").child("nametext")
                msg = chat.child("other").child("chatinfo").child("chatbtn").child("chattext")
                msgs.append({
                    "name": name.get_text(),
                    "msg": msg.get_text()
                })
            ui_log.info(f"获取页面聊天信息记录: {msgs}")
        return msgs
