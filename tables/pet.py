from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class PetManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.config = None
        self.pet_level = {}
        self.conn = conn

        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(PetManager, "_instance"):
            PetManager._instance = object.__new__(cls)
        return PetManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_pet_lvlup')

        for con in self.config:
            aid = con.get("aid")
            if aid in self.pet_level.keys():
                value = self.pet_level.get(aid)
                value.append(PetLevelEntity(con))
            else:
                self.pet_level[aid] = [PetLevelEntity(con)]

        db_log.info('加载配置成功: basic_tech')

    def get_level_by_pet(self, aid):
        result = self.pet_level.get(aid)
        if not result:
            raise AssertionError(f"生化兽升级表中没有该生化兽信息: {aid}")
        return result


class PetLevelEntity:

    def __init__(self, value):
        self.aid = value.get('aid')
        self.id = value.get('id')
        self.preid = value.get('preid')
        self.note = value.get('note')
        self.name = value.get('name')
        self.tier = value.get('tier')
        self.type = value.get('type')
        self.attr = value.get('attr')
        self.skillvid = value.get('skillvid')
        self.cost = value.get('cost')
        self.icon = value.get('icon')
        self.state = value.get('state')
