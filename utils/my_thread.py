import threading


class MyThread(threading.Thread):
    def __init__(self, target, **kwargs):
        super(MyThread, self).__init__()
        self.result = None
        self.func = target
        self.kwargs = kwargs.get('kwargs')

    def run(self):
        self.result = self.func(**self.kwargs)

    def get_result(self):
        try:
            return self.result
        except Exception as e:
            print(e)