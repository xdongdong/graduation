import redis


class RedisClient:

    def __init__(self, _config):
        pool = redis.ConnectionPool(
            host=_config.get('host'),
            port=_config.get('port'),
            db=_config.get('db'),
            password=_config.get('password'),
            decode_responses=True)
        self.conn = redis.Redis(connection_pool=pool)
