from abc import ABC
from tables.base import BaseManager
from utils.log import db_log


class MailManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.conn = conn
        self.values = {}
        self.config = None

        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(MailManager, "_instance"):
            MailManager._instance = object.__new__(cls)
        return MailManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_mail')
        for con in self.config:
            self.values[con.get('mid')] = MailEntity(con)
        db_log.info('加载配置成功: basic_mail')

    def get_setting_by_id(self, _id):
        value = self.values.get(_id)
        if not value:
            raise AssertionError(f"basic_mail中找不到该数据: {_id}")
        db_log.info(f"根据id获取到邮件配置: {_id}, {value}")
        return value


class MailEntity:

    def __init__(self, value):
        self.mid = value.get('mid')
        self.choose = value.get('choose')
        self.note = value.get('note')
        self.sender = value.get('sender')
        self.name = value.get('name')
        self.note1 = value.get('note1')
        self.info = value.get('info')
        self.note2 = value.get('note2')
        self.window = value.get('window')
        self.awardid = value.get('awardid')
        self.icon = value.get('icon')
