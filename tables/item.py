from abc import ABC

from tables.base import BaseManager
from utils.log import db_log


class ItemManager(BaseManager, ABC):
    def __init__(self, conn):
        if hasattr(self, "config"):
            return
        self.conn = conn
        self.config = None
        self.values = {}
        self.hero = {}

        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(ItemManager, "_instance"):
            ItemManager._instance = object.__new__(cls)
        return ItemManager._instance

    def load_config(self):
        self.config = self.conn.getALL('select * from basic_item')
        for con in self.config:
            self.values[con.get('aid')] = ItemEntity(con)
            # 61类型为英雄
            if con.get("usetype") == 61:
                self.hero[f"{con.get('useargs')}_{con.get('quality')}"] = ItemEntity(con)

        db_log.info('加载配置成功: basic_item')

    def getConfigByAid(self, aid):
        return self.values.get(aid)

    def get_hero_by_quality(self, hero_id, quality):
        """
        根据英雄id和品质获取配置
        :param hero_id:
        :param quality:
        :return:
        """
        key = f"{hero_id}_{quality}"
        result = self.hero.get(key)
        if not result:
            raise AssertionError(f"未找到对应的英雄物品配置: {hero_id}, {quality}")
        return result

    def get_item_by_type(self, tp):
        """
        根据类型获取所有物品
        :param tp:
        :return:
        """
        return [item for item in self.values.values() if item.usetype == tp]


class ItemEntity:

    def __init__(self, value):
        self.aid = value.get('aid')
        self.note = value.get('note')
        self.page = value.get('page')
        self.auto = value.get('auto')
        self.usetype = value.get('usetype')
        self.useargs = value.get('useargs')
        self.buttonname = value.get('buttonname')
        self.quality = value.get('quality')
        self.value = value.get('value')
        self.transform = value.get('transform')
        self.redtype = value.get('redtype')
        self.overtime = value.get('overtime')
        self.actid = value.get('actid')
        self.jumpid = value.get('jumpid')
        self.icon = value.get('icon')
        self.name = value.get('name')
        self.desc = value.get('desc')
        self.origin = value.get('origin')
        self.order = value.get('order')
