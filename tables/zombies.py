from abc import ABC
from tables.base import BaseManager
from utils.log import db_log
from utils.mysql import Mysql


class ZombiesManager(BaseManager, ABC):
    def __init__(self, conn: Mysql):
        if hasattr(self, "score_config"):
            return
        self.score_config = None
        self.rank_config = None
        self.award_config = None
        self.score_value = {}
        self.rank_value = {}
        self.award_value = {}

        self.conn = conn

        self.load_config()

    # 如果存在已有的实例， 则返回已有的实例对象--单例模式
    def __new__(cls, *args, **kwargs):
        if not hasattr(ZombiesManager, "_instance"):
            ZombiesManager._instance = object.__new__(cls)
        return ZombiesManager._instance

    def load_config(self):
        self.score_config = self.conn.getALL('select * from basic_team_zombiesint')
        self.rank_config = self.conn.getALL('select * from basic_team_zombiesrank')
        self.award_config = self.conn.getALL("select * from basic_team_zombiesaward")

        for con in self.score_config:
            self.score_value[con.get('id')] = ZombiesScoreEntity(con)
        db_log.info('加载配置成功: basic_team_zombiesint')

        for con in self.rank_config:
            self.rank_value[con.get("rank")] = ZombiesRankEntity(con)
        db_log.info('加载配置成功: basic_team_zombiesrank')

        for con in self.award_config:
            value = ZombiesAwardEntity(con)
            self.award_value[f"{value.integral}_{value.teamintegral}"] = value
        db_log.info('加载配置成功: basic_team_zombiesaward')

    def get_score_by_type_num(self, kind, num):
        """
        根据类型和值获取积分
        :param kind:  1. 怪物  2.关卡
        :param num: Kind=1 时为 monster表的state, Kind=2 时为波次
        :return:
        """
        kind_value = self.get_score_by_type(kind)
        for i in kind_value:
            if i.type == kind and i.num == num:
                return i.integral

    def get_score_by_type(self, kind):
        """
        获取某一类的积分加成
        :param kind:  1. 怪物  2.关卡
        :return:
        """
        return [i for i in self.score_value.values() if i.type == kind]

    def get_rank_award(self, rank):
        """
        获取排名对应的奖励物品
        :param rank: 排名
        :return:
        """
        for c_rank in self.rank_value.keys():
            min_rank, max_rank = c_rank.split(";")
            if int(min_rank) <= rank < int(max_rank):
                return self.rank_value.get(c_rank)


class ZombiesScoreEntity:

    def __init__(self, value):
        self.id = value.get('id')
        self.note = value.get('note')
        self.type = value.get('type')
        self.num = value.get('num')
        self.integral = value.get('integral')


class ZombiesRankEntity:
    def __init__(self, value):
        self.id = value.get('id')
        self.note = value.get('note')
        self.rank = value.get('rank')
        self.award = value.get('award')


class ZombiesAwardEntity:
    def __init__(self, value):
        self.id = value.get('id')
        self.note = value.get('note')
        self.integral = value.get('integral')
        self.teamintegral = value.get('teamintegral')
        self.award = value.get('award')
