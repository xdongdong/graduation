import random
from random import randint
import allure
from apis.chat_api import ChatApi
from apis.hero_api import HeroApi
from tables.award import AwardEntity
from tables.hero import HeroStarEntity
from tables.basic_global import GlobalManager
from tables.hero import HeroManager
from tables.item import ItemManager
from testcase.data.hero_data import HeroData
from utils.comparators import Comparators
from utils.hero_attr import HeroAttr
from utils.param_encrypt import decrypt
from utils.util import parse_json, parse_setting, merge_dict, generate_openid


class TestApiHero:

    def setup_class(self):
        # api类
        self.api = HeroApi()
        # 将openid进行随机,每次都是用不重复的openid
        openid = generate_openid()
        # 登录
        user_info, host = self.api.login(openid)
        # 用户信息
        self.role_id = user_info.get("roleid")
        # 数据类
        self.data = HeroData()
        # 初始化chat api
        self.chat_api = ChatApi(self.api.base_data)
        # 断言类
        self.compare = Comparators()
        # basic_item
        self.item_manager = ItemManager(self.api.db_utils.sql_conn)
        # 英雄相关配置
        self.hero_manager = HeroManager(self.api.db_utils.sql_conn)
        # 计算属性类
        self.attr = HeroAttr(self.role_id, self.api.db_utils.sql_conn)
        # 全局配置表
        self.global_manager = GlobalManager(self.api.db_utils.sql_conn)
        # 将英雄数量修改为无上限
        self.api.update_hero_num(99999)

    def test_less_level(self):
        """
        材料不足时升级英雄
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]

        re = self.api.level_up(hero.get("hid"))
        response = decrypt(re.text)

        self.compare.error_code(response, "-110001", f"升级材料不足: {response}")

    def test_less_break(self):
        """
        材料不足时进行英雄突破
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        hid = hero.get("hid")

        # 修改为可以突破的等级
        self.api.update_hero(hid, self.data.hero_break)

        re = self.api.break_level(hid)
        response = decrypt(re.text)

        self.compare.error_code(response, "-110001", f"突破材料不足: {response}")

    def test_error_level(self):
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        hid = hero.get("hid")

        # 修改为可以突破的等级
        self.api.update_hero(hid, self.data.hero_break)

        re = self.api.level_up(hid)
        response = decrypt(re.text)

        self.compare.error_code(response, "-104003", f'该突破时调用升级接口： {response}')

    def test_error_break(self):
        with allure.step(f"获取英雄: {self.data.test_hero}, quality={self.data.test_quality}"):
            hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
            hid = hero.get("hid")

        with allure.step("发送突破请求"):
            re = self.api.break_level(hid)
            response = decrypt(re.text)

        with allure.step("断言code为-104004"):
            self.compare.error_code(response, "-104004", f"该升级时调用突破接口: {response}")

    def test_concurrent_level(self):
        """
        并发升级测试
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, level, state = hero.get("heroid"), hero.get("hid"), hero.get("level"), hero.get("state")
        with allure.step(f"获取升到下一级所需消耗物品: hero={hero_id} level={level}, state={state}"):
            next_setting = self.hero_manager.get_next_level_setting(hero_id, level, state)
            cost = self.hero_manager.get_hero_level_cost(hero_id, level=next_setting.level, state=next_setting.state)
            setting_prop = AwardEntity(reward=[])
            for pid, num in cost.items():
                self.chat_api.chat_talk(f"gm prop {pid} {num}")
                setting_prop.reward.append({"pid": pid, "num": -num})

        with allure.step(f"进行并发升级"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            params = {"hid": hid}
            results = self.api.concurrent_func(self.api.level_up, **params)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)

            success = False
            for result in results:
                response = decrypt(result.text)
                if parse_json("code", response) == "1":
                    self.compare.equal_bool(success, False, "首次成功")
                    self.compare.changing(response, before_prop, changed_prop, setting_prop)
                    success = True
            self.compare.equal_bool(success, True, f"两次均未成功")

        db_hero = self.api.get_hero_db_data(hid)
        self.compare.equal(db_hero.get("level"), next_setting.level, f"并发升级后等级验证")
        self.compare.equal(db_hero.get("state"), next_setting.state, f"并发升级后state验证")

    def test_quick_unlock(self):
        """
        未解锁时使用快速升级功能
        :return:
        """
        # 修改等级为1级
        self.chat_api.chat_talk("gm userlevel 1")
        with allure.step(f"获取英雄"):
            hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
            hero_id, hid, level, state = hero.get("heroid"), hero.get("hid"), hero.get("level"), hero.get("state")
        with allure.step(f"快速升级"):
            re = self.api.quick_level(hid, 10, 0)
            response = decrypt(re.text)
            self.compare.error_code(response, msg="未解锁时使用快速升级功能")

    def test_error_quick(self):
        """
        传入错误的参数进行快速升级
        :return:
        """
        self.api.db_utils.unlock_quick_function(self.role_id)

        with allure.step(f"获取英雄"):
            hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
            hero_id, hid, level, state = hero.get("heroid"), hero.get("hid"), hero.get("level"), hero.get("state")

        with allure.step("修改等级"):
            test_level = 10
            self.api.update_hero(hid, {
                "level": test_level,
                "state": 0
            })

        with allure.step(f"获取升级到最大等级的材料"):
            cost = self.hero_manager.get_hero_level_cost(hero_id)
            for pid, num in cost.items():
                self.chat_api.chat_talk(f"gm prop {pid} {num * 10}")

        with allure.step("快速升级至超出最大等级"):
            max_setting = self.hero_manager.get_level_by_hero(hero_id)[-1]
            re = self.api.quick_level(hid, max_setting.level + 2, max_setting.state + 1)
            response = decrypt(re.text)
            self.compare.error_code(response, msg=f"快速升级至超出最大等级code验证")

        with allure.step("验证等级是否变化"):
            db_hero = self.api.get_hero_db_data(hid)
            self.compare.equal(db_hero.get("level"), test_level, f"错误的升级后等级应该无变化")

        with allure.step("快速升级至更低等级"):
            re = self.api.quick_level(hid, 5, 0)
            response = decrypt(re.text)
            self.compare.error_code(response, msg=f"快速升级至更低等级code验证")

        with allure.step("验证等级是否变化"):
            db_hero = self.api.get_hero_db_data(hid)
            self.compare.equal(db_hero.get("level"), test_level, f"错误的升级后等级应该无变化")

    def test_quick_level(self):
        """
        一键升级
        :return:
        """
        self.api.db_utils.unlock_quick_function(self.role_id)

        with allure.step(f"获取英雄升级配置"):
            # 取其更小值
            max_setting = self.hero_manager.get_hero_max_level(self.data.test_hero)
            # 获取到升到最大等级的一半的配置
            half_level = int(max_setting.level / 2)
            next_setting = self.hero_manager.get_next_level_setting(self.data.test_hero, half_level,
                                                                    int(str(half_level)[:-1]) - 1)
        test_setting = [max_setting, next_setting]
        for t_setting in test_setting:
            with allure.step(f"快速升级至:{t_setting.level} {t_setting.state}验证"):
                with allure.step(f"获取英雄"):
                    hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
                    hero_id, hid, level, state = hero.get("heroid"), hero.get("hid"), hero.get("level"), hero.get(
                        "state")

                with allure.step(f"获取升级材料"):
                    cost = self.hero_manager.get_hero_level_cost(hero_id, level=t_setting.level, state=t_setting.state)
                    setting_prop = AwardEntity(reward=[])
                    for pid, num in cost.items():
                        self.chat_api.chat_talk(f"gm prop {pid} {num}")
                        setting_prop.reward.append({"pid": pid, "num": -num})

                with allure.step(f"一键升级"):
                    before_prop = self.api.db_utils.get_user_props(self.role_id)
                    re = self.api.quick_level(hid, t_setting.level, t_setting.state)
                    response = decrypt(re.text)
                    self.compare.success_code(response, f"一键升级返回code")
                    changed_prop = self.api.db_utils.get_user_props(self.role_id)

                with allure.step(f"对升级后的属性进行验证"):
                    db_hero = self.api.get_hero_db_data(hid)
                    self.compare.equal(db_hero.get("level"), t_setting.level, f"一键升级后等级验证")
                    self.compare.equal(db_hero.get("state"), t_setting.state, f"一键升级后state验证")
                    self.attr.assert_hero_attr(hid)

                with allure.step(f"'对升级后的技能技能验证"):
                    unlock_skill = {
                        "pveskill1": None,
                        "pveskill2": None,
                        "pveskill3": None,
                    }
                    for se in self.hero_manager.get_level_by_hero(hero_id):
                        if se.skill.isdigit() and se.skill != "-1":
                            key = "pveskill" + se.skill[-2]
                            unlock_skill[key] = int(se.skill)
                        if se == t_setting:
                            break
                    for key, value in unlock_skill.items():
                        self.compare.equal(value, db_hero.get(key), f"升至{t_setting.level}后, 解锁技能{key}验证")

                with allure.step(f"对消耗物品进行验证"):
                    self.compare.changing(response, before_prop, changed_prop, setting_prop)

    def test_concurrent_quick_level(self):
        """
        并发快速升级
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)
        self.api.db_utils.unlock_quick_function(self.role_id)

        with allure.step(f"获取英雄升级配置"):
            # 配置内的最大等级
            setting_max = self.hero_manager.get_level_by_hero(self.data.test_hero)[-1]
            # global表的最大等级
            global_max = self.global_manager.get_value_by_id(self.data.global_level_max)
            global_max = int(global_max)
            # 取其更小值
            max_setting = setting_max if setting_max.level < global_max \
                else self.hero_manager.get_hero_level_setting(self.data.test_hero, global_max,
                                                              int(str(global_max)[:-1]) - 1)
            # 获取到升到最大等级的一半的配置
            half_level = int(max_setting.level / 2)
            next_setting = self.hero_manager.get_next_level_setting(self.data.test_hero, half_level,
                                                                    int(str(half_level)[:-1]) - 1)
        with allure.step(f"获取英雄"):
            hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
            hero_id, hid, level, state = hero.get("heroid"), hero.get("hid"), hero.get("level"), hero.get(
                "state")

        with allure.step(f"获取升级材料"):
            cost = self.hero_manager.get_hero_level_cost(hero_id, level=next_setting.level, state=next_setting.state)
            setting_prop = AwardEntity(reward=[])
            for pid, num in cost.items():
                self.chat_api.chat_talk(f"gm prop {pid} {num * 2}")
                setting_prop.reward.append({"pid": pid, "num": -num})

        with allure.step(f"并发进行一键升级"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            params = {"hid": hid, "tolevel": next_setting.level, "tostate": next_setting.state}
            results = self.api.concurrent_func(self.api.quick_level, **params)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)

            success = False
            for result in results:
                response = decrypt(result.text)
                if parse_json("code", response) == "1":
                    self.compare.equal_bool(success, False, "首次成功")
                    self.compare.changing(response, before_prop, changed_prop, setting_prop)
                    success = True
            self.compare.equal_bool(success, True, f"两次均未成功")

        with allure.step(f"并发升级后对属性及等级进行验证"):
            db_hero = self.api.get_hero_db_data(hid)
            self.compare.equal(db_hero.get("level"), next_setting.level, f"并发快速升级后对level进行验证")
            self.compare.equal(db_hero.get("state"), next_setting.state, f"并发快速升级后对state进行验证")
            self.attr.assert_hero_attr(hid)

    def test_concurrent_break(self):
        """
        并发突破测试
        :return:
        """
        self.api.db_utils.clear_user_package(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 修改相关属性后重新获取该英雄
        hero = self.api.update_hero(hero.get("hid"), self.data.hero_break)
        hero_id, hid, level, state = hero.get("heroid"), hero.get("hid"), hero.get("level"), hero.get("state")

        # 获取配置及消耗物品
        with allure.step(f"获取升到下一级所需消耗物品: hero={hero_id} level={level}, state={state}"):
            next_setting = self.hero_manager.get_next_level_setting(hero_id, level, state)
            cost = self.hero_manager.get_hero_level_cost(hero_id, level=next_setting.level, state=next_setting.state)
            setting_prop = AwardEntity(reward=[])
            for pid, num in cost.items():
                self.chat_api.chat_talk(f"gm prop {pid} {num}")
                setting_prop.reward.append({"pid": pid, "num": -num})

        with allure.step(f"进行并发突破"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            params = {"hid": hid}
            results = self.api.concurrent_func(self.api.break_level, **params)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)

            success = False
            for result in results:
                response = decrypt(result.text)
                if parse_json("code", response) == "1":
                    self.compare.equal_bool(success, False, "首次成功")
                    self.compare.changing(response, before_prop, changed_prop, setting_prop)
                    success = True

        with allure.step("获取到数据库内的英雄信息进行断言"):
            db_hero = self.api.get_hero_db_data(hid)
            self.compare.equal(db_hero.get("level"), next_setting.level, f"并发升级后等级验证")
            self.compare.equal(db_hero.get("state"), next_setting.state, f"并发升级后state验证")

    def test_level_break(self):
        self.api.db_utils.clear_user_package(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, level, state = hero.get("heroid"), hero.get("hid"), hero.get("level"), hero.get("state")
        # 获取该英雄最高等级配置
        setting_max = self.hero_manager.get_level_by_hero(hero_id)[-1]
        global_max = self.global_manager.get_value_by_id(self.data.global_level_max)
        global_max = int(global_max)
        max_setting = setting_max if setting_max.level < global_max \
            else self.hero_manager.get_hero_level_setting(hero_id, global_max,
                                                          int(str(global_max)[:-1]) - 1)

        # 获取升到满级所需消耗的物品
        with allure.step(f"获取升到满级所需消耗物品: hero={hero_id}"):
            cost = self.hero_manager.get_hero_level_cost(hero_id)
            for pid, num in cost.items():
                self.chat_api.chat_talk(f"gm prop {pid} {num}")
        while True:
            # 随机升级N级后检查一次属性
            random_num = randint(1, 20)
            for i in range(random_num):
                # 获取等级的配置
                current_setting = self.hero_manager.get_hero_level_setting(hero_id, level, state)
                next_setting = self.hero_manager.get_next_level_setting(hero_id, level, state)

                before_prop = self.api.db_utils.get_user_props(self.role_id)
                # 进行升级/突破
                with allure.step(f"进行升级/突破: level={level}, state={state}"):
                    if next_setting.state > current_setting.state:
                        cost_prop = parse_setting(current_setting.breakcost)
                        res = self.api.break_level(hid)
                    else:
                        cost_prop = parse_setting(current_setting.levelcost)
                        res = self.api.level_up(hid)
                response = decrypt(res.text)

                # 获取升级/突破消耗的物品
                setting_prop = []
                for pid, num in cost_prop.items():
                    setting_prop.append({"pid": pid, "num": -num})

                with allure.step(f"对消耗物品进行断言"):
                    changed_prop = self.api.db_utils.get_user_props(self.role_id)
                    setting_prop = AwardEntity(reward=setting_prop)
                    self.compare.changing(response, before_prop, changed_prop, setting_prop)
                if next_setting.skill != "-1":
                    with allure.step(f"当前等级会解锁技能: {next_setting.skill}"):
                        skill_index = next_setting.skill[-2]
                        hero_data = self.api.get_hero_db_data(hid)
                        hero_skill = hero_data.get(f"pveskill{skill_index}")
                        self.compare.equal(str(hero_skill), next_setting.skill, f"解锁技能: {hero_data}")

                level, state = next_setting.level, next_setting.state
                if level == max_setting.level and state == max_setting.state:
                    break
            with allure.step(f"升级: {random_num}次后, 验证英雄属性是否正确"):
                self.attr.assert_hero_attr(hid)
            if level == max_setting.level and state == max_setting.state:
                break

        # 升级最大等级后继续升级/突破
        with allure.step(f"升级至最大等级后进行突破, level={level}, state={state}"):
            res = self.api.break_level(hid)
            response = decrypt(res.text)
            self.compare.error_code(response, "-104014", f"达到最大等级后进行突破: {response}")

        with allure.step(f"升级至最大等级后进行升级, level={level}, state={state}"):
            res = self.api.level_up(hid)
            response = decrypt(res.text)
            self.compare.error_code(response, "-104014", f"达到最大等级后进行升级: {response}")

    def test_less_star(self):
        """
        传入消耗不足的情况下升星
        :return:
        """
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, grade, star = hero.get("heroid"), hero.get("hid"), hero.get("grade"), hero.get("star")
        # 获取到同时消耗两个材料的配置并将英雄改为对应的属性
        cur_setting = self.hero_manager.get_star_cost_two_setting(hero_id)
        self.api.update_hero(hid, {
            "grade": cur_setting.state,
            "star": cur_setting.star
        })
        cost_hero = self.get_star_cost(cur_setting)

        with allure.step(f"需要消耗两个材料时仅传1个升星"):
            re = self.api.star_up(hid, cost_hero[:-1])
            response = decrypt(re.text)
            self.compare.error_code(response, msg=f"需要消耗两个时仅传1个升星: {response}")

        with allure.step(f"不传被消耗的英雄进行升星"):
            re = self.api.star_up(hid, [])
            response = decrypt(re.text)
            self.compare.error_code(response, msg=f"不传被消耗的英雄进行升星: {response}")

        with allure.step(f"验证英雄星级未变化,被消耗英雄未消耗"):
            db_hero = self.api.get_hero_db_data(hid)
            self.compare.equal(grade, db_hero.get("grade"), f"升星英雄星级未变化")
            self.compare.equal(star, db_hero.get("star"), f"升星英雄星级未变化")
            for h in cost_hero:
                db_cost_hero = self.api.get_hero_db_data(h)
                self.compare.equal_bool(db_cost_hero, True, f"成功获取到未被消耗的英雄")

    def test_err_star(self):
        """
        传入错误的消耗进行升星
        :return:
        """
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, grade, star = hero.get("heroid"), hero.get("hid"), hero.get("grade"), hero.get("star")
        # 获取配置
        settings = self.hero_manager.get_star_by_hero(hero_id)
        for cost_type in self.data.star_cost_type:
            cur_setting = None
            for s in settings:
                if s.costtype1 == cost_type or s.costtype2 == cost_type:
                    cur_setting = s
                    break
            if not cur_setting:
                raise AssertionError(f"英雄: {hero_id} 没有配置消耗类型为{cost_type}的升星配置")
            self.api.update_hero(hid, {
                "grade": cur_setting.state,
                "star": cur_setting.star
            })
            with allure.step(f"获取与当前配置不一致的配置: 当前消耗1:{cur_setting.cost1} 消耗2: {cur_setting.cost2}"):
                err_setting = random.choice(
                    [s for s in self.hero_manager.get_all_star_setting() if
                     s.cost1 != cur_setting.cost1 or s.cost2 != cur_setting.cost2]
                )
                # 获取消耗品
                err_cost = self.get_star_cost(err_setting)

            with allure.step(f"使用错误的数据进行升星"):
                res = self.api.star_up(hid, err_cost)
                response = decrypt(res.text)
                self.compare.error_code(response, msg=f"使用错误的数据进行升星: \n"
                                                      f"request: hid: {hid}, cost: {err_cost}\n"
                                                      f"response: {response}")

    def test_star(self):
        """
        正常对一个英雄进行升星
        :return:
        """
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, grade, star = hero.get("heroid"), hero.get("hid"), hero.get("grade"), hero.get("star")
        max_setting = self.hero_manager.get_star_by_hero(hero_id)[-1]
        while True:
            cur_setting = self.hero_manager.get_hero_star_setting(hero_id, grade, star)
            next_setting = self.hero_manager.get_next_star_setting(hero_id, grade, star)

            cost_hero = self.get_star_cost(cur_setting)
            with allure.step(f"传入正确的数据升星: heroid={hero_id}, grade={grade}, star={star}"):
                re = self.api.star_up(hid, cost_hero)
                response = decrypt(re.text)
                self.compare.success_code(response, f"传入正确的数据升星: {response}")

            with allure.step(f"升星后对星级及消耗英雄验证"):
                db_hero = self.api.get_hero_db_data(hid)
                self.compare.equal(db_hero.get("grade"), next_setting.state, f"升星后对grade进行验证")
                self.compare.equal(db_hero.get("star"), next_setting.star, f"升星后对star进行验证")
                for h in cost_hero:
                    db_cost_hero = self.api.get_hero_db_data(h)
                    self.compare.equal_bool(db_cost_hero, False, f"升星后被消耗的英雄还存在: {db_cost_hero}")
            if next_setting == max_setting:
                break
            grade, star = next_setting.state, next_setting.star
            # 验证属性
            self.attr.assert_hero_attr(hid)
        with allure.step(f"到达最大星级后进行升星"):
            cost_hero = self.get_star_cost(next_setting)

            re = self.api.star_up(hid, cost_hero)
            response = decrypt(re.text)
            self.compare.error_code(response, msg=f"达到最大星级后进行升星: {response}")

        with allure.step(f"最大星级升星后对星级及消耗英雄验证"):
            db_hero = self.api.get_hero_db_data(hid)
            self.compare.equal(db_hero.get("grade"), next_setting.state, f"最大星级后升星, grade应该不变")
            self.compare.equal(db_hero.get("star"), next_setting.star, f"最大星级后升星, star应该不变")
            for h in cost_hero:
                db_cost_hero = self.api.get_hero_db_data(h)
                self.compare.equal_bool(db_cost_hero, True, f"最大星级后升星,升星后被消耗的英雄需要还存在: {db_cost_hero}")
        self.attr.assert_hero_attr(hid)

    def test_star_return(self):
        """
        测试升星返还
        :return:
        """
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, grade, star = hero.get("heroid"), hero.get("hid"), hero.get("grade"), hero.get("star")
        with allure.step(f"获取升星的配置"):
            # 获取升星需要消耗两个物品的配置
            cur_setting = self.hero_manager.get_star_cost_two_setting(hero_id)
            self.api.update_hero(hid, {
                "grade": cur_setting.state,
                "star": cur_setting.star
            })
            cost_hero = self.get_star_cost(cur_setting)

        with allure.step(f"获取升级消耗并更改消耗的英雄的等级"):
            first_cost_hero = self.api.get_hero_db_data(cost_hero[0])
            sec_cost_hero = self.api.get_hero_db_data(cost_hero[-1])
            cost1 = self.hero_manager.get_hero_level_cost(
                first_cost_hero.get("heroid"), first_cost_hero.get("level"), first_cost_hero.get("state"), 20, 1
            )
            cost2 = self.hero_manager.get_hero_level_cost(
                sec_cost_hero.get("heroid"), sec_cost_hero.get("level"), sec_cost_hero.get("state"), 60, 6
            )
            self.api.update_hero(cost_hero[0], {
                "level": 20,
                "state": 1,
            })
            self.api.update_hero(cost_hero[-1], {
                "level": 60,
                "state": 6,
            })
            level_cost = merge_dict(cost1, cost2)

        with allure.step(f"获取升级专属消耗并更改专属等级"):
            exclusive1 = self.api.get_exclusive_db_data(hid=cost_hero[0])
            exclusive2 = self.api.get_exclusive_db_data(hid=cost_hero[-1])
            max_setting1 = self.hero_manager.get_exclusive_by_hero(first_cost_hero.get("heroid"))[-1]
            max_setting2 = self.hero_manager.get_exclusive_by_hero(sec_cost_hero.get("heroid"))[-1]

            self.api.update_exclusive({"level": max_setting1.level}, hid=first_cost_hero.get("hid"))
            self.api.update_exclusive({"level": max_setting2.level}, hid=sec_cost_hero.get("hid"))

            cost1 = self.hero_manager.get_exclusive_level_cost(first_cost_hero.get("heroid"), exclusive1.get("level"),
                                                               max_setting1.level)
            cost2 = self.hero_manager.get_exclusive_level_cost(sec_cost_hero.get("heroid"), exclusive2.get("level"),
                                                               max_setting2.level)

            exclusive_cost = merge_dict(cost1, cost2)

        with allure.step(f"进行升星"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            re = self.api.star_up(hid, cost_hero)
            response = decrypt(re.text)
            self.compare.success_code(response, f"升星返回code")
            changed_prop = self.api.db_utils.get_user_props(self.role_id)

        with allure.step(f"验证返还的道具是否正确"):
            setting_drop = AwardEntity(reward=[])
            for pid, num in merge_dict(level_cost, exclusive_cost).items():
                setting_drop.reward.append({"pid": pid, "num": num})

            self.compare.changing(response, before_prop, changed_prop, setting_drop)

    def test_concurrent_star(self):
        """
        仅一次升星就达到最大星级时并发升星
        :return:
        """
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, grade, star = hero.get("heroid"), hero.get("hid"), hero.get("grade"), hero.get("star")
        # 获取倒数第二级的星级并修改至该品级
        cur_setting = self.hero_manager.get_star_by_hero(hero_id)[-2]
        next_setting = self.hero_manager.get_next_star_setting(hero_id, cur_setting.state, cur_setting.star)

        self.api.update_hero(hid, {
            "grade": cur_setting.state,
            "star": cur_setting.star
        })

        with allure.step(f"获取升星消耗"):
            cost_hero = self.get_star_cost(cur_setting)

        with allure.step(f"进行并发升星, 当前属性: hero={hero_id}, grade={cur_setting.state}, star={cur_setting.star}"):
            params = {"hid": hid, "uhids": cost_hero}
            results = self.api.concurrent_func(self.api.star_up, **params)

            success = False
            for result in results:
                response = decrypt(result.text)
                if parse_json("code", response) == "1":
                    self.compare.equal_bool(success, False, "首次成功")
                    success = True

        with allure.step(f"并发升星完毕后对星级及英雄进行验证"):
            db_hero = self.api.get_hero_db_data(hid)
            self.compare.equal(db_hero.get("grade"), next_setting.state, f"并发升星后grade验证")
            self.compare.equal(db_hero.get("star"), next_setting.star, f"并发升星后star验证")
            for h in cost_hero:
                db_cost_hero = self.api.get_hero_db_data(h)
                self.compare.equal_bool(db_cost_hero, False, f"升星后被消耗的英雄还存在: {db_cost_hero}")
        with allure.step(f"验证并发升星后属性是否正确"):
            self.attr.assert_hero_attr(hid)

    def test_exclusive_level_less(self):
        """
        材料不足的情况下给专属升级
        :return:
        """
        # 清除玩家背包数据
        self.api.db_utils.clear_user_package(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, exclusive = hero.get("heroid"), hero.get("hid"), hero.get("exclusiveEntity")
        with allure.step(f"进行专属升级"):
            re = self.api.exclusive_level(exclusive.get("upid"))
            response = decrypt(re.text)
            # 材料不足时返回了code 1 但changing为空
            changing = parse_json("changing", response)
            self.compare.equal_bool(changing, False, "材料不足时进行专属升级")

        with allure.step(f"对数据库专属信息进行验证"):
            db_exclusive = self.api.get_exclusive_db_data(exclusive.get("upid"))
            self.compare.equal(db_exclusive.get("level"), exclusive.get("level"), f"材料不足时升级等级未变动")

    def test_exclusive_level(self):
        """
        专属正常升级 皮肤解锁 技能解锁等功能
        :return:
        """
        self.api.clear_exclusive_skin()

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, exclusive = hero.get("heroid"), hero.get("hid"), hero.get("exclusiveEntity")
        upid = exclusive.get("upid")
        exclusive_skill = exclusive.get("skillid")
        exclusive_skin = exclusive.get("skinid")
        level = exclusive.get("level")
        unlock_skin = [exclusive_skin]

        with allure.step(f"未升级前获取专属皮肤"):
            skin_re = self.api.get_exclusive_allSkin()
            skin_response = decrypt(skin_re.text)
            data = skin_response.get("data")
            now_skin = data.get(str(exclusive.get("pid")))
            self.compare.equal(now_skin, [exclusive_skin], f"验证是否仅存在初始皮肤")

        with allure.step(f"未操作前拉取专属信息验证"):
            info_re = self.api.get_exclusive(hid)
            info_response = decrypt(info_re.text)
            info = parse_json("info", info_response)
            self.compare.equal(info.get("upid"), upid, f"拉取信息接口返回upid")
            self.compare.equal(info.get("level"), level, f"拉取信息接口返回level")
            self.compare.equal(info.get("skillid"), exclusive_skill, f"拉取信息接口返回skill")
            self.compare.equal(info.get("skinid"), exclusive_skin, f"拉取信息接口返回skin")

        with allure.step(f"未解锁更高级的专属皮肤时进行切换"):
            # exclusive_skin+1 切换更高一级的皮肤
            switch_re = self.api.exclusive_switch_skin(upid, exclusive_skin + 1)
            switch_response = decrypt(switch_re.text)
            self.compare.error_code(switch_response, msg="切换未解锁的皮肤code验证")

        max_setting = self.hero_manager.get_exclusive_by_hero(hero_id)[-1]
        while True:
            cur_setting = self.hero_manager.get_exclusive_level_setting(hero_id, level)
            next_setting = self.hero_manager.get_next_exclusive_setting(hero_id, level)
            setting_drop = AwardEntity(reward=[])
            with allure.step(f"获取升级所需消耗物品: hero={hero_id}, level={cur_setting.level}"):
                for pid, num in parse_setting(cur_setting.cost).items():
                    self.chat_api.chat_talk(f"gm prop {pid} {num}")
                    setting_drop.reward.append({"pid": pid, "num": -num})

            with allure.step(f"进行专属升级, hero={hero_id}, level={cur_setting.level}"):
                before_prop = self.api.db_utils.get_user_props(self.role_id)
                re = self.api.exclusive_level(upid)
                response = decrypt(re.text)
                self.compare.success_code(response, msg=f"升级专属: hero={hero_id} upid={upid} level={cur_setting.level}")
                changed_prop = self.api.db_utils.get_user_props(self.role_id)

            with allure.step(f"断言升级消耗: hero={hero_id} upid={upid} level={cur_setting.level}"):
                self.compare.changing(response, before_prop, changed_prop, setting_drop)

            with allure.step(f"断言等级, 是否解锁技能, 是否解锁皮肤"):
                db_exclusive = self.api.get_exclusive_db_data(upid)
                self.compare.equal(db_exclusive.get("level"), next_setting.level, f"升级专属后断言等级是否升到下一级")
                # 技能和皮肤
                exclusive_skill = exclusive_skill if next_setting.skill == -1 else next_setting.skill
                if next_setting.unlockskin != -1:
                    exclusive_skin = next_setting.unlockskin
                    # 获取数据库专属皮肤记录是否有数据
                    unlock_skin.append(next_setting.unlockskin)

                self.compare.equal(db_exclusive.get("skillid"), exclusive_skill,
                                   f"{hero_id}专属当前等级: {next_setting.level} 未解锁技能")
                self.compare.equal(db_exclusive.get("skinid"), exclusive_skin,
                                   f"{hero_id}专属当前等级: {next_setting.level} 未解锁并使用皮肤")

            with allure.step(f"断言升级专属后英雄属性是否正常"):
                self.attr.assert_hero_attr(hid)

            level = next_setting.level
            if next_setting == max_setting:
                break
        with allure.step(f"操作完成后拉取专属信息接口验证"):
            info_re = self.api.get_exclusive(hid)
            info_response = decrypt(info_re.text)
            info = parse_json("info", info_response)
            self.compare.equal(info.get("upid"), upid, f"拉取信息接口返回upid")
            self.compare.equal(info.get("level"), level, f"拉取信息接口返回level")
            self.compare.equal(info.get("skillid"), exclusive_skill, f"拉取信息接口返回skill")
            self.compare.equal(info.get("skinid"), exclusive_skin, f"拉取信息接口返回skin")

        with allure.step(f"验证是否皮肤均已经解锁"):
            db_skin = self.api.get_unlock_skin(exclusive.get("pid"))
            self.compare.equal(db_skin, unlock_skin, f"已解锁的皮肤与配置表内需要解锁的皮肤不一致")

        with allure.step(f"升到最大等级后继续进行升级"):
            for pid, num in parse_setting(cur_setting.cost).items():
                self.chat_api.chat_talk(f"gm prop {pid} {num}")
                setting_drop.reward.append({"pid": pid, "num": num})

            re = self.api.exclusive_level(upid)
            response = decrypt(re.text)
            self.compare.error_code(response, msg=f"升级专属: hero={hero_id} upid={upid} level={cur_setting.level}")

        with allure.step(f"获取当前拥有的所有专属皮肤验证是否正确"):
            skin_re = self.api.get_exclusive_allSkin()
            skin_response = decrypt(skin_re.text)
            data = skin_response.get("data")
            now_skin = data.get(str(exclusive.get("pid")))
            self.compare.equal(now_skin, unlock_skin, f"验证升级完毕后是否解锁所有皮肤")

        with allure.step(f"切换当前已装备的皮肤"):
            switch_skin = db_exclusive.get("skinid")
            switch_re = self.api.exclusive_switch_skin(upid, switch_skin)
            switch_response = decrypt(switch_re.text)
            self.compare.error_code(switch_response, msg=f"切换至当前正在装备的皮肤code验证")

        with allure.step(f"切换已解锁的皮肤"):
            # 移除掉当前的已装备的皮肤
            db_skin.remove(switch_skin)
            switch_skin = random.choice(db_skin)
            switch_re = self.api.exclusive_switch_skin(upid, switch_skin)
            switch_response = decrypt(switch_re.text)
            self.compare.success_code(switch_response, msg=f"切换已解锁的专属皮肤code验证")

            db_exclusive = self.api.get_exclusive_db_data(upid)
            use_skin = db_exclusive.get("skinid")
            self.compare.equal(use_skin, switch_skin, f"切换专属皮肤后数据库内存储数据验证")

    def test_exclusive_quick_level_less(self):
        """
        专属装备材料不足的情况下快速升级
        :return:
        """
        self.api.db_utils.unlock_quick_function(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 英雄相关属性
        hero_id, hid, exclusive = hero.get("heroid"), hero.get("hid"), hero.get("exclusiveEntity")
        upid = exclusive.get("upid")
        level = exclusive.get("level")

        exclusive_setting = self.hero_manager.get_exclusive_by_hero(hero_id)
        cur_setting = self.hero_manager.get_exclusive_level_setting(hero_id, level)
        max_setting = exclusive_setting[-1]
        half_setting = exclusive_setting[int(len(exclusive_setting) / 2)]

        cur_cost = self.hero_manager.get_exclusive_level_cost(hero_id, cur_setting.level, half_setting.level)
        with allure.step(f"获取一部分专属升级材料"):
            for pid, num in cur_cost.items():
                self.chat_api.chat_talk(f"gm prop {pid} {num}")

        with allure.step(f"在材料不足的情况下快速升级至满级"):
            res = self.api.exclusive_quick_level(upid, max_setting.level)
            response = decrypt(res.text)
            self.compare.error_code(response, msg="材料不足时快速升级专属返回code")

        with allure.step(f"错误的升级后验证等级是否变化"):
            db_exclusive = self.api.get_exclusive_db_data(upid)
            self.compare.equal(db_exclusive.get("level"), level, f"材料不足时快速升级等级应无变化")

    def test_exclusive_quick_level(self):
        """
        正常进行专属快速升级
        :return:
        """
        self.api.db_utils.unlock_quick_function(self.role_id)
        self.api.clear_exclusive_skin()

        with allure.step(f"获取专属升级配置"):
            exclusive_setting = self.hero_manager.get_exclusive_by_hero(self.data.test_hero)
            max_setting = exclusive_setting[-1]
            half_setting = exclusive_setting[int(len(exclusive_setting) / 2)]
            test_setting = [half_setting, max_setting]

        for t_setting in test_setting:
            with allure.step(f"获取对应的英雄及专属"):
                hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
                # 英雄相关属性
                hero_id, hid, exclusive = hero.get("heroid"), hero.get("hid"), hero.get("exclusiveEntity")
                upid = exclusive.get("upid")
                level = exclusive.get("level")
                exclusive_skin = exclusive.get("skinid")
                cur_setting = self.hero_manager.get_exclusive_level_setting(hero_id, level)

                cur_cost = self.hero_manager.get_exclusive_level_cost(hero_id, cur_setting.level, t_setting.level)
                unlock_skill, unlock_skin = self.hero_manager.get_exclusive_level_unlock(hero_id, cur_setting.level,
                                                                                         t_setting.level)
                # 没有初始皮肤时加个初始皮肤
                if exclusive_skin not in unlock_skin:
                    unlock_skin.insert(0, exclusive_skin)

            cost = AwardEntity(reward=[])
            with allure.step(f"获取升至中途等级所需消耗的材料"):
                for pid, num in cur_cost.items():
                    self.chat_api.chat_talk(f"gm prop {pid} {num}")
                    cost.reward.append({"pid": pid, "num": -num})

            with allure.step(f"快速升级至{t_setting.level}"):
                before_prop = self.api.db_utils.get_user_props(self.role_id)
                res = self.api.exclusive_quick_level(upid, t_setting.level)
                response = decrypt(res.text)
                self.compare.success_code(response, msg="快速升级专属返回code")
                changed_prop = self.api.db_utils.get_user_props(self.role_id)

            with allure.step(f"升级后材料消耗,等级属性等验证"):
                db_exclusive = self.api.get_exclusive_db_data(upid)
                self.compare.equal(db_exclusive.get("level"), t_setting.level, f"快速升级后等级验证")
                self.compare.equal(db_exclusive.get("skillid"), unlock_skill, f"快速升级后解锁技能验证")
                self.compare.equal(db_exclusive.get("skinid"), unlock_skin[-1], f"快速升级后是否自动装备最后一个解锁的皮肤")

                with allure.step(f"验证皮肤解锁是否正确"):
                    db_skin = self.api.get_unlock_skin(exclusive.get("pid"))
                    self.compare.equal(db_skin, unlock_skin, f"已解锁的皮肤与配置表内需要解锁的皮肤不一致")

                self.compare.changing(response, before_prop, changed_prop, cost)
                self.attr.assert_hero_attr(hid)

            with allure.step(f"删除已解锁的专属装备皮肤的数据"):
                self.api.db_utils.sql_conn.delete(
                    f"delete from {self.api.setting.DB}.user_exclusive_skin where roleid ={self.role_id} and exclusiveid = {exclusive.get('pid')} and RIGHT(skinid, 1) != 1"
                )

    def test_exclusive_quick_level_error(self):
        """
        传入错误的参数进行专属快速升级
        :return:
        """
        self.api.db_utils.unlock_quick_function(self.role_id)

        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 专属相关属性
        hero_id, hid, exclusive = hero.get("heroid"), hero.get("hid"), hero.get("exclusiveEntity")
        upid = exclusive.get("upid")
        test_level = 10
        with allure.step(f"修改专属的等级至测试等级"):
            self.api.update_exclusive({"level": test_level}, hid=hid)

        with allure.step(f"快速升级至更低等级"):
            re = self.api.exclusive_quick_level(upid=upid, tolevel=5)
            response = decrypt(re.text)
            changing = parse_json("changing", response)
            self.compare.equal_bool(changing.get("prop"), False, f"返回接口信息中应无物品变化")
            self.compare.error_code(response, msg=f"专属快速升级至更低等级")

        with allure.step(f"验证专属等级是否有变化"):
            db_exclusive = self.api.get_exclusive_db_data(upid)
            self.compare.equal(db_exclusive.get("level"), test_level, f"快速升级至更低等级验证等级是否无变化")

    def test_concurrent_exclusive_quick_level(self):
        """
        并发进行专属快速升级
        :return:
        """
        self.api.db_utils.unlock_quick_function(self.role_id)
        self.api.db_utils.clear_user_package(self.role_id)

        with allure.step(f"专属升级配置"):
            exclusive_setting = self.hero_manager.get_exclusive_by_hero(self.data.test_hero)
            half_setting = exclusive_setting[int(len(exclusive_setting) / 2)]

        with allure.step(f"获取英雄"):
            hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
            hero_id, hid, exclusive = hero.get("heroid"), hero.get("hid"), hero.get("exclusiveEntity")
            upid = exclusive.get("upid")
            level = exclusive.get("level")
            cur_setting = self.hero_manager.get_exclusive_level_setting(hero_id, level)
            unlock_skill, unlock_skin = self.hero_manager.get_exclusive_level_unlock(hero_id, cur_setting.level,
                                                                                     half_setting.level)

        with allure.step(f"获取专属升级材料"):
            cost = self.hero_manager.get_exclusive_level_cost(hero_id, cur_setting.level, half_setting.level)
            setting_prop = AwardEntity(reward=[])
            for pid, num in cost.items():
                # 获取到超出的升级材料
                self.chat_api.chat_talk(f"gm prop {pid} {num}")
                setting_prop.reward.append({"pid": pid, "num": -num})

        with allure.step(f"并发进行一键升级"):
            before_prop = self.api.db_utils.get_user_props(self.role_id)
            params = {"upid": upid, "tolevel": half_setting.level}
            results = self.api.concurrent_func(self.api.exclusive_quick_level, **params)
            changed_prop = self.api.db_utils.get_user_props(self.role_id)

            success = False
            for result in results:
                response = decrypt(result.text)
                changing = parse_json("changing", response)
                if changing:
                    self.compare.equal_bool(success, False, "首次成功")
                    self.compare.changing(response, before_prop, changed_prop, setting_prop)
                    success = True
            self.compare.equal_bool(success, True, f"两次均未成功")

        with allure.step(f"并发升级后对属性及等级进行验证"):
            db_exclusive = self.api.get_exclusive_db_data(upid)
            self.compare.equal(db_exclusive.get("level"), half_setting.level, f"并发升级仅一次成功")
            with allure.step(f"验证皮肤解锁是否正确"):
                db_skin = self.api.get_unlock_skin(exclusive.get("pid"))
                self.compare.equal(db_skin, unlock_skin, f"已解锁的皮肤与配置表内需要解锁的皮肤不一致")
            self.compare.equal(db_exclusive.get("skillid"), unlock_skill, f"快速升级后解锁技能验证")
            self.compare.equal(db_exclusive.get("skinid"), unlock_skin[-1], f"快速升级后是否自动装备最后一个解锁的皮肤")

    # 不可被添加为主共鸣的英雄
    def test_add_resonance_less(self):
        # 获取英雄
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality)[0]
        # 获取该英雄共鸣的第一条配置
        resonance = self.hero_manager.get_hero_resonance(self.data.test_hero)[0]
        # 获取英雄信息
        if resonance.resstar >= 1:
            self.api.update_hero(hero.get("hid"), {
                "star": resonance.resstar - 1
            })
        elif resonance.resquality >= 1:
            self.api.update_hero(hero.get("hid"), {
                "grade": resonance.resquality - 1
            })
        else:
            raise AssertionError("最低品质英雄也可以共鸣")

        re = self.api.add_resonance(hero.get("hid"))
        response = decrypt(re.text)
        self.compare.error_code(response, msg=f"品级不够时也添加了共鸣: {response}")

    # 添加已经被加入共鸣的英雄
    def test_add_resonance_repeat(self):
        # 获取英雄
        hero = self.api.get_hero(self.data.test_hero, self.data.test_quality, 2)
        resonance_hero = hero[0]
        be_resonance_hero = hero[-1]
        # 获取配置
        resonance_setting = self.hero_manager.get_hero_resonance(self.data.test_hero)[-1]

        self.api.update_hero(resonance_hero.get("hid"), {
            "star": resonance_setting.resstar,
            "grade": resonance_setting.resquality
        })

        # 将该英雄添加为主共鸣者
        add_master_re = self.api.add_resonance(resonance_hero.get("hid"))
        add_master_response = decrypt(add_master_re.text)
        self.compare.error_code(add_master_response, msg=f"添加主共鸣: {add_master_response}")

        # 添加被共鸣者两次
        with allure.step(f"使用同一个英雄添加两次"):
            add_other_re1 = self.api.add_resonance(resonance_hero.get("hid"), fhid=be_resonance_hero.get("hid"),
                                                   trench=1)
            add_other_response1 = decrypt(add_other_re1.text)
            self.compare.success_code(add_other_response1, msg=f"第一次添加被共鸣: {add_other_response1}")

            add_other_re2 = self.api.add_resonance(resonance_hero.get("hid"), fhid=be_resonance_hero.get("hid"),
                                                   trench=2)
            add_other_response2 = decrypt(add_other_re2.text)
            self.compare.success_code(add_other_response2, msg=f"第二次添加被共鸣: {add_other_response2}")

        with allure.step(f"对数据库中的结果进行验证"):
            result = self.api.get_db_resonance(resonance_hero.get("hid"))
            self.compare.equal(result.get("position1"), be_resonance_hero.get("hid"), f"第一个位置英雄成功添加: {result}")
            self.compare.equal_bool(result.get("position2"), False, f"第二个位置不应该被添加: {result}")

    def get_star_cost(self, config: HeroStarEntity):
        """
        获取当前星级升一星的消耗
        :param config: 当前星级的配置
        :return:
        """

        def get_cost(cost_type, cost):
            if cost_type == self.data.star_cost_type[0]:
                race, grade, star = cost.split(";")
                while True:
                    race_hero = random.choice(self.hero_manager.get_hero_by_faction(int(race)))
                    try:
                        self.item_manager.get_hero_by_quality(race_hero.heroid, self.data.test_quality)
                    except AssertionError:
                        pass
                    else:
                        break
                return race_hero.heroid, int(grade), int(star)
            elif cost_type == self.data.star_cost_type[1]:
                hero, grade, star = cost.split(";")
                return int(hero), int(grade), int(star)

        result = []
        if config.costtype1 != -1:
            hero_id, gra, sta = get_cost(config.costtype1, config.cost1)
            with allure.step(f"获取升星消耗1： {config.cost1}, hero={hero_id}, grade={gra}, star={sta}"):
                get_hero = self.api.get_hero(hero_id, self.data.test_quality)[0]
                self.api.update_hero(get_hero.get("hid"), {
                    "grade": gra,
                    "star": sta
                })
                result.append(get_hero.get("hid"))
        if config.costtype2 != -1:
            hero_id, gra, sta = get_cost(config.costtype2, config.cost2)
            with allure.step(f"获取升星消耗2： {config.cost2}, hero={hero_id}, grade={gra}, star={sta}"):
                get_hero = self.api.get_hero(hero_id, self.data.test_quality)[0]
                self.api.update_hero(get_hero.get("hid"), {
                    "grade": gra,
                    "star": sta
                })
                result.append(get_hero.get("hid"))
        return result
